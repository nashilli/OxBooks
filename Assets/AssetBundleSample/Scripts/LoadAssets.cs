﻿using System;
using UnityEngine;
using System.Collections;
using System.IO;
using AssetBundles;
using Assets.Scripts.App;
using Assets.Scripts._Book;
using Assets.Scripts._Book.Page;
using Application = UnityEngine.Application;


public class LoadAssets : MonoBehaviour
{
    private static LoadAssets _loadAssets;
    public const string AssetBundlesOutputPath = "/AssetBundles/";
    private string _assetBundleName;    //public string assetName;
    private Action<float> _onUpdateFunc;

    void Awake()
    {
        if (_loadAssets == null) _loadAssets = this;
        else if (_loadAssets != this) Destroy(gameObject);
    }

    



	// Use this for initialization
	IEnumerator Start ()
	{
	    //Caching.CleanCache();
	 
        yield return StartCoroutine(Initialize() );
			
	}

	// Initialize the downloading url and AssetBundleManifest object.
	protected IEnumerator Initialize()
	{
		// Don't destroy this gameObject as we depend on it to run the loading script.
		DontDestroyOnLoad(gameObject);

        // With this code, when in-editor or using a development builds: Always use the AssetBundle Server
        // (This is very dependent on the production workflow of the project. 
        // 	Another approach would be to make this configurable in the standalone player.)
        //#if DEVELOPMENT_BUILD || UNITY_EDITOR
        //AssetBundleManager.SetDevelopmentAssetBundleServer ();
        //#else
        // Use the following code if AssetBundles are embedded in the project for example via StreamingAssets folder etc:
        //AssetBundleManager.SetSourceAssetBundleURL(Application.dataPath + "/");
        // Or customize the URL based on your deployment or configuration
        //AssetBundleManager.SetSourceAssetBundleURL("http://www.MyWebsite/MyAssetBundles");
        //#endif

        
        
        // se cargan los assets en streaming assets path

        AssetBundleManager.SetSourceAssetBundleDirectory("BooksResources/");
	   
        // Initialize AssetBundleManifest which loads the AssetBundleManifest object.
	    var request1 = AssetBundleManager.Initialize();
  
	    if (request1 != null)
	        yield return StartCoroutine(request1);


        foreach (Book book in AppController.GetController().GetAllBooks())
        {
#if UNITY_EDITOR
            string path = System.IO.Path.Combine(AssetBundleManager.BaseDownloadingURL.Remove(0, 7), book.AssetBundle.ToLower() + ".meta") ;
#else
            string path = System.IO.Path.Combine(AssetBundleManager.BaseDownloadingURL.Remove(0, 7), book.AssetBundle.ToLower() + ".meta");
#endif
            if (File.Exists(path))
            {
                string[] strings = ((CoverPage)book.Pages[0]).ImagePath.Split('/');
                string imagePath = strings[strings.Length - 1];
                LoadAssets.GetLoadAssets()
                    .LoadAnAsset<UnityEngine.Object>(book.AssetBundle.ToLower(), imagePath, operation => { }, f => {});

            }
        }      

        AssetBundleManager.SetSourceAssetBundleURL("http://www.oxed.com.ar/projects/oxbooks/assets/");
        // Initialize AssetBundleManifest which loads the AssetBundleManifest object.
        var request = AssetBundleManager.Initialize();

	  

	     if (request != null)
			yield return StartCoroutine(request);
	}

    void UpdateProgress()
    {
        if(!AssetBundleManager.m_DownloadingWWWs.ContainsKey(_assetBundleName)) return;
        if (_onUpdateFunc != null && AssetBundleManager.m_DownloadingWWWs[_assetBundleName].progress < 1)
        {
            _onUpdateFunc(AssetBundleManager.m_DownloadingWWWs[_assetBundleName].progress);
            Invoke("UpdateProgress", 0.001f);
        }
        else
        {
            CancelInvoke("UpdateProgress");
            _assetBundleName = "";
        }
        //Debug.Log("progress " + AssetBundleManager.m_DownloadingWWWs[_assetBundleName].progress);
    }

    public void LoadAnAsset<T>(string assetBundleName, string assetName, Action<AssetBundleLoadAssetOperation> onLoadedFunc, Action<float> onUpdateFunc)
    {
        // Load asset.
        StartCoroutine(InstantiateGameObjectAsync<T>(assetBundleName, assetName, onLoadedFunc, onUpdateFunc));
    }

    protected IEnumerator InstantiateGameObjectAsync<T> (string assetBundleName, string assetName, Action<AssetBundleLoadAssetOperation> onLoadedFunc, Action<float> onUpdateFunc)
	{
		// This is simply to get the elapsed time for this phase of AssetLoading.
		float startTime = Time.realtimeSinceStartup;
        _assetBundleName = assetBundleName;
        // Load asset from assetBundle.
        AssetBundleLoadAssetOperation request = AssetBundleManager.LoadAssetAsync(assetBundleName, assetName, typeof(T));
        _onUpdateFunc = onUpdateFunc;
        Invoke("UpdateProgress", 0.001f);
        if (request == null)
			yield break;
		yield return StartCoroutine(request);

        // Get the asset.

        //  T asset = request.GetAsset<T>();

        onLoadedFunc(request);


        // Calculate and display the elapsed time.
        // float elapsedTime = Time.realtimeSinceStartup - startTime;
        //Debug.Log(assetName + (prefab == null ? " was not" : " was")+ " loaded successfully in " + elapsedTime + " seconds" );
    }

    public static LoadAssets GetLoadAssets()
    {
        return _loadAssets;
    }

    
}
