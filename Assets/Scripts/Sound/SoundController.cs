﻿using UnityEngine;
using Assets.Scripts.Settings;

namespace Assets.Scripts.Sound{

    public class SoundController : MonoBehaviour{
	    private static SoundController soundController;

	    public AudioClip wrongAnswerSound;
	    public AudioClip rightAnswerSound;
	    public AudioClip levelCompleteSound;
	    public AudioClip clickSound;
        public AudioClip music;
        public AudioClip KeyPressedSound;

        public AudioSource soundSource;
	    public AudioSource musicSource;
	    public AudioSource extraSource;

        void Awake(){
            if (soundController == null){
                soundController = this;
            }
            else if (soundController != this){
                Destroy(gameObject);
            }
            DontDestroyOnLoad(this);

        }

        void Start()
        {
            PlayMusic();

        }

        public void PlayClip(AudioClip customClip){
            soundSource.clip = customClip;
	        soundSource.Play();
        }

        public void PlayFailureSound(){
	        if (SettingsController.GetController().SfxOn()) {
                soundSource.clip = wrongAnswerSound;
		        soundSource.Play();
	        }
        }

        public void PlayClickSound(){
	        if (SettingsController.GetController().SfxOn()) {
                soundSource.clip = clickSound;
		        soundSource.Play();
	        }
        }

        public void PlayRightAnswerSound(){ 
	        if (SettingsController.GetController().SfxOn()) {
                soundSource.volume = 1;
                soundSource.clip = rightAnswerSound;
		        soundSource.Play();
	        }
        }

        public void PlayLevelCompleteSound(){
	        if (SettingsController.GetController().SfxOn()) {
                soundSource.clip = levelCompleteSound;
		        soundSource.Play();
	        }
        }    

        public void PlayMusic(){
			if (SettingsController.GetController().MusicOn() && !musicSource.isPlaying) {
				musicSource.clip = music;
		    	musicSource.Play();         	
	        }
        }

        public void StopMusic(){
	        musicSource.Stop();
        }

        public void StopSound(){
            soundSource.Stop();
        }

        public static SoundController GetController(){
            return soundController;
        }

        public void PlayKeyPressedSound()
        {
            if (SettingsController.GetController().SfxOn())
            {
                soundSource.clip = KeyPressedSound;
                soundSource.Play();
            }
            
        }


        public void StopExtraClip()
        {
            extraSource.Stop();
        }

        public void ContinueExtraClip(AudioClip clip)
        {
            if (extraSource.time > 0)
            {
                extraSource.UnPause();
            }
            else
            {
                extraSource.clip = clip;
                extraSource.Play();
            }
        }

        public void PauseExtraClip()
        {
            extraSource.Pause();
        }

        public bool IsExtraClipPlaying()
        {
            return extraSource.isPlaying;
        }

       
    }
}