﻿using System.Collections.Generic;
using Assets.Scripts.App;
using Assets.Scripts.Buttons;
using Assets.Scripts.Sound;
using Assets.Scripts._Book;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Profile
{
    public class ProfileView : MonoBehaviour
    {

        // User info
        public Image Avatar;
        public Image LevelImage;
        public Text Name;
        public List<Image> TrophiesImages;

        public StartedBookButton[] StartedBookButtons;
        public Text AnyBookInProgressText;
        public Button SettingsButton;


        // Labels
        public Text LevelLabel;
        public Text TitleLabel;
        public Text LastReadBookLabel;
        public Text LastStartedBookLabel;
        public Text TrophiesLabel;

        
    }
}
