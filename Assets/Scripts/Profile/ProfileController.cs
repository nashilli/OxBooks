﻿using Assets.Scripts.App;
using Assets.Scripts.Buttons;
using Assets.Scripts.Sound;
using Assets.Scripts._Book;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Profile
{
    public class ProfileController : MonoBehaviour
    {

        public ProfileView ProfileView;

        void Start()
        {
            //SetLabels();
            //ProfileView.TitleLabel.text = "Últimos libros empezados";
            LoadUserData();
            ProfileView.SettingsButton.onClick.AddListener(delegate
            {
                SoundController.GetController().PlayClickSound();
                ViewController.GetController().LoadSettings();
            });
        }

        private void SetLabels()
        {
            SetStringToText(ProfileView.LevelLabel, "Nivel");
            SetStringToText(ProfileView.TitleLabel, "Mis lecturas");
            SetStringToText(ProfileView.LastReadBookLabel, "Último libro leído");
            SetStringToText(ProfileView.LastStartedBookLabel, "Último libro empezado");
            SetStringToText(ProfileView.TrophiesLabel, "Trofeos");

        }

        private void SetStringToText(Text text, string aString)
        {
            text.text = aString;
        }

        private void LoadUserData()
        {
            User currentUser = AppController.GetController().GetCurrentUser();
            ProfileView.Name.text = currentUser.Name;
            ProfileView.Avatar.material = currentUser.Avatar.LoadProfilePicture();
          /*  int j = 0;
            for (var i = currentUser.StartedBooks.Count - 1; i >= 0 && j < ProfileView.StartedBookButtons.Length; i--)
            {
                if (currentUser.StartedBooks[i].State == StartedBookState.InProgress)
                {

                    ProfileView.StartedBookButtons[j].gameObject.SetActive(true);
                    ProfileView.StartedBookButtons[j].SetStartedBook(currentUser.StartedBooks[i]);
                    ProfileView.AnyBookInProgressText.transform.parent.gameObject.SetActive(false);
                    j++;
                }
            }

            ProfileView.AnyBookInProgressText.transform.parent.gameObject.SetActive(j == 0);
            ProfileView.AnyBookInProgressText.text = "No tienes en progreso ningún libro.";
            for (; j < ProfileView.StartedBookButtons.Length; j++)
            {
                ProfileView.StartedBookButtons[j].gameObject.SetActive(false);
            }*/


            //_levelImage.sprite = currentUser.Level.LoadSprite();
            //_levelLabel.text += " " + currentUser.Level.GetNumericLevel();
            //int trophy = 0;
            /* for (; trophy < currentUser.Trophies.Count; trophy++)
         {
             _trophiesImages[trophy].sprite = currentUser.Trophies[trophy].LoadSprite();
         }

         for (; trophy < _trophiesImages.Count; trophy++)
         {
             _trophiesImages[trophy].sprite = Trophy.RANDOM.LoadSprite();
         }*/

        }
    }
}
