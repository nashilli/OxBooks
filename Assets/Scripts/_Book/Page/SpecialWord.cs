﻿namespace Assets.Scripts._Book.Page
{
    public class SpecialWord
    {
        public string WordInText { get; set; }
        public string GenericWord { get; set; }
        public string Description { get; set; }
    }
}