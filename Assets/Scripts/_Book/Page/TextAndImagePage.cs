﻿
namespace Assets.Scripts._Book.Page
{
    public class TextAndImagePage : TextPage
    {
        public string ImagePath { get; set; } 


        public TextAndImagePage(int id) : base(id)
        {
        }

        public override PageType GetPageType()
        {
            return PageType.TextAndImage;
        }

        public override bool Update(Page page)
        {
            throw new System.NotImplementedException();
        }
    }
}