﻿namespace Assets.Scripts._Book.Page
{
    public class ObjectiveWord
    {
        public string Question { get; set; }
        public string Word { get; set; }
        public WordOrientation Orientation { get; set; }
        public Point InitialPoint { get; set; }


        public ObjectiveWord(string question, string word, WordOrientation orientation, Point initialPoint)
        {
            Question = question;
            Word = word;
            Orientation = orientation;
            InitialPoint = initialPoint;
        }
    }

    public class Point
    {
 
        public Point(int v1, int v2)
        {
            X = v1;
            Y = v2;
        }

        public int X { get; set; }
        public int Y { get; set; }
    }

    public enum WordOrientation
    {
        Vertical, Horizontal
    }
}