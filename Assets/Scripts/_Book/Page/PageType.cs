﻿using System;
using Assets.Scripts.Utils;

namespace Assets.Scripts._Book.Page
{
   
    public class PageTypeInfo : Attribute
    {
        internal PageTypeInfo(string prefabName)
        {
            PrefabName = prefabName;
        }
        public string PrefabName { get; private set; }
    }

    [Serializable]
    public enum PageType
    {
        [PageTypeInfo("CoverPageView")]
        Cover,
        [PageTypeInfo("TextPageView")]
        Text,
        [PageTypeInfo("TextAndImagePageView")]
        TextAndImage,
        [PageTypeInfo("ImagePageView")]
        Image,
        [PageTypeInfo("LetterSoupView")]
        LetterSoup,
        [PageTypeInfo("MultipleChoiceView")]
        MultipleChoice,
        [PageTypeInfo("FluencyReadView")]
        ReadingFluency,
        [PageTypeInfo("")]
        Etpa,
        [PageTypeInfo("")]
        EtpaTextAndImage

    }

    public static class PageTypeExtensions
    {
        public static string GetPrefabName(this PageType p)
        {
            PageTypeInfo attr = p.GetAttribute<PageTypeInfo>();
            return attr.PrefabName;
        }

    }
}