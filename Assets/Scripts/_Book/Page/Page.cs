﻿using Assets.Scripts.Common;
using Assets.Scripts.Common.Action;

namespace Assets.Scripts._Book.Page
{
    public abstract class Page
    {
        protected Page(int id)
        {
            Id = id;
        }

        public int Id { get; set; }
        public string AudioPath { get; set; }
        public int NextPageId { get; set; }

        /*Return the @PageType of this class */
        public abstract PageType GetPageType();

        /*Return the id of the next page */
        public abstract int GetNextPageId();

        /* Return true if page was successfully updated, otherwise return false */
        public abstract bool Update(Page page);

        protected bool Equals(Page other)
        {
            return Id == other.Id;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Page) obj);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public abstract bool HasText();

    }
}
