﻿using Assets.Scripts.Common.Action;

namespace Assets.Scripts._Book.Page
{
    public class ReadingFluencyPage : MultipleChoicePage
    {
        public float FluencySpeed { get; set; }


        public ReadingFluencyPage(int id) : base(id, ChoiceType.Text)
        {
                       
            FluencySpeed = 1;
            HintAction = new FluencyReadingAction();
        }

        public override PageType GetPageType()
        {
            return PageType.ReadingFluency;
        }

        public override bool HasText()
        {
            return true;
        }
    }
}