﻿namespace Assets.Scripts._Book.Page
{
    public class Option
    {
        public bool IsTrue { get; set; }
        public string Text { get; set; }
        public string ImagePath { get; set; }
        public string SoundPath { get; set; }
        public int NextPageId { get; set; }

        public OptionType GetOptionType()
        {
            return !string.IsNullOrEmpty(SoundPath)
                ? OptionType.Sound
                : !string.IsNullOrEmpty(ImagePath) ? OptionType.Image : OptionType.Text;
        }
    }
}