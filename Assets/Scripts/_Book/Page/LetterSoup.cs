﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Common.Action;
using UnityEngine;
using Action = Assets.Scripts.Common.Action.Action;

namespace Assets.Scripts._Book.Page
{
    public class LetterSoup : Page, IExercise
    {
        public string[][] Letters { get; set; }
        public List<ObjectiveWord> ObjectiveWords { get; set; }
        private bool[][] _resolutionMatrix;

        public Action TicAction { get; set; }
        public Action HintAction { get; set; }


        public LetterSoup(int id) : base(id)
        {
            Letters = new string[11][];
            for (int i = Letters.Length - 1; i >= 0; i--)
            {
                Letters[i] = new string[6];
            }

            ObjectiveWords = new List<ObjectiveWord>();
            TicAction = new CheckAlphabetSoupAction();
         
        }

        public override PageType GetPageType()
        {
            return PageType.LetterSoup;
        }

        public override int GetNextPageId()
        {
            return NextPageId;
        }

        public void AddObjectiveWord(ObjectiveWord objectiveWord)
        {
            ObjectiveWords.Add(objectiveWord);
        }

        public void RemoveObjectiveWord(ObjectiveWord objectiveWord)
        {
            ObjectiveWords.Remove(objectiveWord);
        }

        public override bool Update(Page page)
        {
            throw new System.NotImplementedException();
        }

        public override bool HasText()
        {
            return false;
        }

        public static char GetLetter()
        {
            string chars = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
            return chars[UnityEngine.Random.Range(0, chars.Length)];  
        }


        public bool IsResolved()
        {
            return false;
        }

        public void GenereteRandom()
        {
            GenerateObjectiveWords();
            LocateObjectiveWords();
            FillBlanks();
            
        }

        public void LocateObjectiveWords()
        {
            foreach (ObjectiveWord objectiveWord in ObjectiveWords)
            {
                switch (objectiveWord.Orientation)
                {
                    case WordOrientation.Horizontal:
                        for (int i = objectiveWord.InitialPoint.X; i - objectiveWord.InitialPoint.X < objectiveWord.Word.Length; i++)
                        {
                            // the next method is only for test purposes
                            if (i >= Letters.Length)
                            {
                                Debug.Log("Error");
                            }
                            string[] strings = Letters[i];
                            char c = objectiveWord.Word[i - objectiveWord.InitialPoint.X];
                            CheckIfIsCorrected(strings[objectiveWord.InitialPoint.Y], "" + c, i , objectiveWord.InitialPoint.Y);

                            strings[objectiveWord.InitialPoint.Y] = "" +
                                c;
                        }
                        break;
                    case WordOrientation.Vertical:
                        for (int j = objectiveWord.InitialPoint.Y; j - objectiveWord.InitialPoint.Y < objectiveWord.Word.Length; j++)
                        {
                            string[] strings = Letters[objectiveWord.InitialPoint.X];
                            if (j >= strings.Length)
                            {
                                Debug.Log("Error en palabra " + objectiveWord.Word );
                            }
                            string prev = strings[j];
                            char c = objectiveWord.Word[j - objectiveWord.InitialPoint.Y];
                            CheckIfIsCorrected(prev, "" + c, objectiveWord.InitialPoint.X, j);
                            strings[j] = "" + c;
                        }
                        break;
                    default:
                        for (int i = objectiveWord.InitialPoint.X; i < objectiveWord.Word.Length; i++)
                        {
                            Letters[i][objectiveWord.InitialPoint.Y] = "" +
                                objectiveWord.Word[i - objectiveWord.InitialPoint.X];
                        }
                        break;
                }
            }
        }

        private void CheckIfIsCorrected(string prev, string actual, int x, int y)
        {
            if (!string.IsNullOrEmpty(prev))
            {
                if (prev != actual)
                {
                    Debug.Log("Error settig alphabet soup. On x = " + x + " y = " + y);
                    Debug.Log("Quiera entrar " + prev + " pero está " + actual);
                    throw new Exception("Error settig alphabet soup. On x = " + x + " y = " + y);

                }
            }
        }

        private void GenerateObjectiveWords()
        {
            ObjectiveWord objectiveWord1 = new ObjectiveWord("Q1", "WORD1", WordOrientation.Horizontal, new Point(0, 0));
            ObjectiveWord objectiveWord2 = new ObjectiveWord("Q2", "MARY", WordOrientation.Vertical, new Point(5, 0));
            ObjectiveWord objectiveWord3 = new ObjectiveWord("Q3", "NONO", WordOrientation.Horizontal, new Point(0, 4));
            ObjectiveWord objectiveWord4 = new ObjectiveWord("Q4", "INE", WordOrientation.Vertical, new Point(10, 2));

            ObjectiveWords.Add(objectiveWord1);
            ObjectiveWords.Add(objectiveWord2);
            ObjectiveWords.Add(objectiveWord3);
            ObjectiveWords.Add(objectiveWord4);
        }

        public void FillBlanks()
        {
            for (int i = Letters.Length - 1; i >= 0; i--)
            {
                for (int j = Letters[i].Length - 1; j >= 0; j--)
                {
                    if (Letters[i][j] == null || Letters[i][j] == "") { Letters[i][j] = "" + GetLetter(); }
                }
            }
        }

        public bool[][] GetResolutionMatrix()
        {
            if (_resolutionMatrix == null) CalculateResolutionMatrix();
            return _resolutionMatrix;
        }

        private void CalculateResolutionMatrix()
        {
            _resolutionMatrix = new bool[Letters.Length][];
            for (int i = _resolutionMatrix.Length - 1; i >= 0; i--)
            {
                _resolutionMatrix[i] = new bool[Letters[i].Length];
            }

            foreach (ObjectiveWord objectiveWord in ObjectiveWords)
            {

                switch (objectiveWord.Orientation)
                {
                    case WordOrientation.Horizontal:
                        for (int i = objectiveWord.InitialPoint.X;
                            i < objectiveWord.InitialPoint.X + objectiveWord.Word.Length;
                            i++)
                        {
                            _resolutionMatrix[i][objectiveWord.InitialPoint.Y] = true;
                        }

                        break;
                    case WordOrientation.Vertical:
                        for (int j = objectiveWord.InitialPoint.Y;
                            j < objectiveWord.InitialPoint.Y + objectiveWord.Word.Length;
                            j++)
                        {
                            _resolutionMatrix[objectiveWord.InitialPoint.X][j] = true;
                        }
                        break;
                }
            }
        }
    }
}