﻿using Assets.Scripts.Common;
using Assets.Scripts.Common.Action;

namespace Assets.Scripts._Book.Page
{
    public interface IExercise
    {
        Action TicAction { get; set; }
        Action HintAction { get; set; }
    }
}