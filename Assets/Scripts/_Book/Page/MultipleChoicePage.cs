﻿using Assets.Scripts.Common.Action;

namespace Assets.Scripts._Book.Page
{
    public class MultipleChoicePage : TextAndImagePage, IExercise
    {
        public string Question { get; set; }
        public Option[] Options { get; set; }

        public Action TicAction { get; set; }
        public Action HintAction { get; set; }

        public ChoiceType ChoiceType;


        public MultipleChoicePage(int id, ChoiceType choice) : base(id)
        {
            ChoiceType = choice;
            if(ChoiceType == ChoiceType.Text) HintAction = new ColorTextHintAction();
            TicAction = new CheckMultipleChoiceAction();
        }

        public override PageType GetPageType()
        {
            return PageType.MultipleChoice;
        }

        public override bool Update(Page page)
        {
            throw new System.NotImplementedException();
        }

        public override bool HasText()
        {
            return Text != null && Text.Trim() != "";
        }


        public bool IsResolved()
        {
            return false;
        }

        public int GetCorrectAnswers()
        {
            int counter = 0;
            foreach (Option option in Options)
            {
                if (option.IsTrue) counter++;
            }
            return counter;
        }        
    }

    public enum ChoiceType
    {
        Text, Image
    }
}