﻿namespace Assets.Scripts._Book.Page
{
    public class CoverPage : TextAndImagePage
    {

        public CoverPage(int id) : base(id)
        {
        }

        public override PageType GetPageType()
        {
            return PageType.Cover;
        }
        
    }
}