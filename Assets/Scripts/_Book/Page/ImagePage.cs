﻿namespace Assets.Scripts._Book.Page
{
    public class ImagePage : Page
    {
        public string ImagePath { get; set; }


        public ImagePage(int id) : base(id)
        {
        }

        public override PageType GetPageType()
        {
            return PageType.Image;
        }

        public override int GetNextPageId()
        {
            return NextPageId;
        }

        public override bool Update(Page page)
        {
            throw new System.NotImplementedException();
        }

        public override bool HasText()
        {
            return false;
        }
    }
}