﻿using System;
using Assets.Scripts._Book.PageView;

namespace Assets.Scripts._Book.Page
{
    public class EtpaPage : MultipleChoicePage
    {

        public EtpaPage(int id, ChoiceType choice) : base(id, choice)
        {
            TicAction = null;
            HintAction = null;
        }

        public override int GetNextPageId()
        {
            EtpaView multplieChoiceView = (EtpaView) BookViewController.GetController().GetCurrentPageView();
            string[] selectedAnswers = multplieChoiceView.GetSelectedAnswers();
            string selected = selectedAnswers[0];
            foreach (Option option in Options)
            {
                if (selected.Equals(option.GetOptionType() == OptionType.Text ? option.Text : option.GetOptionType() == OptionType.Image ? option.ImagePath : option.SoundPath))
                {
                    return option.NextPageId;

                }
            }
            throw new Exception("Any option match with the selected");
        }

        public override PageType GetPageType()
        {
            return PageType.Etpa;
        }


        public OptionType GetTypeOfOptions()
        {
            return Options[0].GetOptionType();
            
        }
    }
}