﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Common;

namespace Assets.Scripts._Book.Page
{
    public class TextPage : Page
    {
        public string Text { get; set; }
        public string[] HintTexts { get; set; }
        public SpecialWord[] SpecialWords { get; set; }

        public TextPage(int id) : base(id)
        {
        }

        public TextPage(int id, string text) : base(id)
        {
            Text = text;
        }

        public override PageType GetPageType()
        {
            return PageType.Text;
        }

        public override int GetNextPageId()
        {
            return NextPageId;
        }

        public override bool Update(Page page)
        {
            // todo
            return false;
        }

        public override bool HasText()
        {
            return true;
        }

        public List<Tuple<List<int>, Tuple<string, string>>> GetIndexesOfSpecialWords()
        {
            List<Tuple<List<int>, Tuple<string, string>>> toReturn = new List<Tuple<List<int>, Tuple<string, string>>>();

            if (SpecialWords == null || SpecialWords.Length == 0) return toReturn;
            List<string> splitted = Text.Split(' ').ToList();

            
            foreach (SpecialWord specialWord in SpecialWords)
            {
                List<int> indexes = new List<int>();

                for (int i = splitted.Count - 1; i >= 0; i--)
                {
                    if (splitted[i].Replace("\"","").Equals(specialWord.WordInText, StringComparison.InvariantCultureIgnoreCase))
                    {
                        indexes.Add(i);
                    }
                }

                Tuple<List<int>, Tuple<string, string>> tuple = new Tuple<List<int>, Tuple<string, string>>(indexes, new Tuple<string, string>(specialWord.GenericWord, specialWord.Description));
                toReturn.Add(tuple);
            }
            return toReturn;
        }
    }

}