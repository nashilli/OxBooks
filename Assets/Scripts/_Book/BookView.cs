﻿using Assets.Scripts.App;
using Assets.Scripts.Sound;
using UnityEngine;

namespace Assets.Scripts._Book
{
    // His childs have to implement a method called NextChallenge that
    // recieve whatever it needs
    public abstract class BookView : MonoBehaviour
    {
        // This method have to restart the view of the game to the initial state
        public abstract void RestartGame();

        public abstract void OnCorrectAnswerAnimationEnd();
        public abstract void OnWrongAnswerAnimationEnd();

        // This method have to be called when the user clicks menuButton
        public void OnClickMenuBtn(){
            PlaySoundClick();
            AppController.GetController().ShowInGameMenu();
        }
        // This method have to be called when the user clicks a button
        internal void PlaySoundClick()
        {
            SoundController.GetController().PlayClickSound();
        }
        // This method have to be called when the answers is correct
        internal void PlayRightSound()
        {
            SoundController.GetController().PlayRightAnswerSound();
        }
        // This method have to be called when the answers is incorrect
        internal void PlayWrongSound()
        {
            SoundController.GetController().PlayFailureSound();
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape)) { OnClickMenuBtn(); }
        }
    }
}
