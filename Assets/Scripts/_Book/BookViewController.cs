﻿using System.Collections.Generic;
using Assets.Scripts.App;
using Assets.Scripts.Common;
using Assets.Scripts.Common.Action;
using UnityEngine;
using Assets.Scripts._Book.Page;
using Assets.Scripts._Book.PageView;
using UnityEngine.UI;
using System.Linq;
using Assets.Scripts.FormatableText;
using Assets.Scripts.Sound;

namespace Assets.Scripts._Book
{

    public class BookViewController : MonoBehaviour
    {
        private static BookViewController _instance;

        public PageView.PageView[] PageViews;

        public Book CurrentBook { get; set; }
       // public int CurrentPageId { get; set; }

        public Text PageIndicatorText;

        public ActionButton HintButton;
        public Button MenuButton;
        // only visibile on cover screen
        public Button GoToExploreButton;
        public ActionButton TicButton;
        public ActionButton PrevPageActionButton;
        public ActionButton NexPageActionButton;

        public ActionButton BackwardArrowActionButton;
        public ActionButton ForwardArrowActionButton;

        public ActionToggle TextToSpeechToggle;
        public ActionToggle PlayPauseToggle;

        public ActionToggle ReadTogetherToggle;
        /*        public ActionToggle FormatTextToggle;*/
        /*      public ActionToggle FormatWordsToggle;*/
        public ActionToggle FormatHighlightToggle;
        public Toggle EraseToggle;

        /*        public GameObject FormatTextPanel;
                public GameObject FormatWordsPanel;*/
        public GameObject FormatHighlightPanel;


        void Awake()
        {
            if (_instance == null) _instance = this;
            else if (_instance != this) Destroy(gameObject);
            MenuButton.onClick.AddListener(delegate
            {
                SoundController.GetController().PlayClickSound();
                ViewController.GetController().ShowInGameMenu();
                if (GetPlayTextAction().IsPlaying) PlayPauseToggle.isOn = false;
            });

            GoToExploreButton.onClick.AddListener(SoundController.GetController().PlayClickSound);

            NexPageActionButton.MyAction = new NextPageAction();
            NexPageActionButton.onClick.AddListener(delegate
            {
                Invoke("EnableNextPageActionButton", 0.7f);
            });
            NexPageActionButton.onClick.AddListener(delegate {
                SoundController.GetController().PlayClickSound();
            });
            PrevPageActionButton.MyAction = new PrevPageAction();
            PrevPageActionButton.onClick.AddListener(delegate
            {
                Invoke("EnablePrevPageActionButton", 0.7f);
            });
            PrevPageActionButton.onClick.AddListener(delegate {
                SoundController.GetController().PlayClickSound();
            });

            PlayPauseToggle.ToggleAction = new PlayTextAction();
            PlayPauseToggle.onValueChanged.AddListener(delegate {
                SoundController.GetController().PlayClickSound();
            });
            BackwardArrowActionButton.MyAction = new FinishPlayTextAction();
            BackwardArrowActionButton.onClick.AddListener(delegate {
                SoundController.GetController().PlayClickSound();
            });
            ForwardArrowActionButton.MyAction = new FinishPlayTextAction();
            ForwardArrowActionButton.onClick.AddListener(delegate {
                SoundController.GetController().PlayClickSound();
            });
            TextToSpeechToggle.ToggleAction = new EnablePlayerControlAction(PlayPauseToggle.transform.parent.gameObject);
            TextToSpeechToggle.onValueChanged.AddListener(delegate {
                SoundController.GetController().PlayClickSound();

                //only for stanalone purposes
                BackwardArrowActionButton.MyAction.Execute();
                PlayPauseToggle.isOn = true;
                PlayPauseToggle.ToggleAction.Execute();
               

            });
            ReadTogetherToggle.ToggleAction = new EnablePlayerControlAction(PlayPauseToggle.transform.parent.gameObject);
            ReadTogetherToggle.onValueChanged.AddListener(delegate {
                SoundController.GetController().PlayClickSound();
                //only for standalone purposes
                BackwardArrowActionButton.MyAction.Execute();
                PlayPauseToggle.isOn = true;
                PlayPauseToggle.ToggleAction.Execute();
            });
            FormatHighlightToggle.ToggleAction = new EnableGameObjectAction(FormatHighlightPanel.gameObject);
            FormatHighlightToggle.onValueChanged.AddListener(delegate {
                SoundController.GetController().PlayClickSound();
            });

            TicButton.onClick.AddListener(delegate {
                SoundController.GetController().PlayClickSound();
            });
            HintButton.onClick.AddListener(delegate {
                SoundController.GetController().PlayClickSound();
                HintButton.interactable = false;
                //CreateStartedPage(AppController.GetController().GetCurrentUser(), GetCurrentPage());
                AppController.GetController().GetCurrentUser().CurrentBook.Metrics.AddHint();
                AppController.GetController().GetCurrentUser().CurrentBook.GetCurrentPage().IsHintActivated = true;
            });
            PlayPauseToggle.gameObject.transform.parent.gameObject.SetActive(false);

        }

        void Start()
        {
            ViewController.GetController().OnBookViewControolerLoaded();

        }

        private void EnableNextPageActionButton()
        {
            NexPageActionButton.enabled = true;
        }
        private void EnablePrevPageActionButton()
        {
            PrevPageActionButton.enabled = true;
        }

        void Update()
        {

            if (Input.GetKeyDown(KeyCode.RightArrow) && NexPageActionButton.gameObject.activeSelf && NexPageActionButton.interactable)
            {
                SoundController.GetController().PlayClickSound();
                NexPageActionButton.MyAction.Execute();
            }
            else if (Input.GetKeyDown(KeyCode.LeftArrow) && PrevPageActionButton.gameObject.activeSelf)
            {
                SoundController.GetController().PlayClickSound();
                PrevPageActionButton.MyAction.Execute();
            }
            else if (Input.GetKeyDown(KeyCode.Escape))
            {
                GameObject descriptionPopUp = ViewController.GetController().GetDescriptionPopUp();
                SoundController.GetController().PlayClickSound();
                if (descriptionPopUp == null)
                {
                    ViewController.GetController().ShowInGameMenu();
                }
                else
                {
                    Destroy(descriptionPopUp.gameObject);
                }

            }
        }

        public void ShowPage(int idPage, StartedPage startedPage)
        {            
            Page.Page currentPage = CurrentBook.Pages[idPage];
            StartedBook currentBook = AppController.GetController().GetCurrentUser().CurrentBook;
            Mode mode = currentBook.Mode;
            PageView.PageView pageView = RetrieveAdequatePageView(currentPage, mode);
            pageView.gameObject.SetActive(true);

            // es feo este if, pero necesito que funcione ahora. COnvierto el choice con img a img, xq choice no herda de img
            if (mode == Mode.View && currentPage.GetPageType() == PageType.MultipleChoice && !currentPage.HasText())
            {
                string imagePath = ((MultipleChoicePage)currentPage).ImagePath;
                currentPage = new ImagePage(0);
                ((ImagePage) currentPage).ImagePath = imagePath;
            }
            pageView.LoadPage(currentPage, startedPage, CurrentBook.Level == 0 ? 34 : 28, GetCurrentPage() is CoverPage ? '_' : ' ');
            TreeNode<StartedPage> currentTreeNode = currentBook.CurrentTreeNode;
            PageIndicatorText.text = "" + (currentTreeNode != null ?currentTreeNode.PreviousNodes().Count() + 1 : 1);

            if (idPage == 0)
            {
                HintButton.gameObject.SetActive(false);
                MenuButton.gameObject.SetActive(false);
                GoToExploreButton.gameObject.SetActive(true);
                ReadTogetherToggle.transform.gameObject.SetActive(false);
                FormatHighlightToggle.transform.parent.gameObject.SetActive(false);
                TextAndImagePageView textAndImagePageView = (TextAndImagePageView) GetCurrentPageView();
                foreach (FormatableWord formatableWord in textAndImagePageView.GetComponentsInChildren<FormatableWord>())
                {
                    formatableWord.Underline(textAndImagePageView.Formatable.DefaulFontColor);
                    formatableWord.SetFontColor(textAndImagePageView.Formatable.DefaulFontColor);
                } 

            }
            else
            {
                GoToExploreButton.gameObject.SetActive(false);
                FormatHighlightToggle.transform.parent.gameObject.SetActive(true);
                HintButton.gameObject.SetActive(true);
                MenuButton.gameObject.SetActive(true);


            }
            UpdateArrows();

           // PrintBookInfo();


        }

        private void PrintBookInfo()
        {
            StartedBook currentBook = AppController.GetController().GetCurrentUser().CurrentBook;
            StartedPage startedPage = currentBook.GetCurrentPage();
            if(startedPage == null) return;
            Debug.Log("Current page id: " + startedPage.PageId);
            //Debug.Log("Current page index: " + AppController.GetController().GetCurrentUser().CurrentBook.CurrentPage);
            string history = "";
            TreeNode<StartedPage> currentPage = currentBook.GetRootNode();
            while (currentPage.Children.Count > 0)
            {
                history += currentPage.Value.PageId + ", ";

                currentPage = currentPage.Children[0];

            }
           
            Debug.Log("History ids: " + history);
        }


        private PageView.PageView RetrieveAdequatePageView(Page.Page currentPage, Mode mode)
        {
            if (mode == Mode.View)
            {
                if (currentPage.GetPageType() == PageType.MultipleChoice)
                {
                    return currentPage.HasText() ? PageViews[(int) PageType.Text] : PageViews[(int) PageType.Image];
                } if (currentPage.GetPageType() == PageType.ReadingFluency)
                {
                    return PageViews[(int) PageType.Text];
                }
                // todo --> case alphabetSoutp
            }
            return PageViews[(int)currentPage.GetPageType()];

        }


        public void SetAudioButton(string audioPath)
        {
            GetPlayTextAction().LoadClip(audioPath);
            bool hasAudioClip = GetPlayTextAction().HasAudioClip();
            TextToSpeechToggle.gameObject.SetActive(hasAudioClip);
            //PlayPauseToggle.gameObject.transform.parent.gameObject.SetActive(hasAudioClip && TextToSpeechToggle.isOn);
        }


        public void UpdateArrows()
        {
            TreeNode<StartedPage> currentTreeNode = AppController.GetController().GetCurrentUser().CurrentBook.CurrentTreeNode;
            PrevPageActionButton.gameObject.SetActive(currentTreeNode.Parent != null);
            Page.Page currentPage = GetCurrentPage();
            bool isLastPage = !(currentPage is EtpaPage) && !CurrentBook.Pages.ContainsKey(currentPage.GetNextPageId());
            Image[] images = NexPageActionButton.GetComponentsInChildren<Image>(true);
            images[0].color = isLastPage ? (Color)new Color32(22, 39, 67, 255) : Color.white;
            images[1].gameObject.SetActive(!isLastPage);
            NexPageActionButton.GetComponentInChildren<Text>(true).gameObject.SetActive(isLastPage);
        }

        public void InitBook(Book book, int currentIdPage = 0)
        {
            CurrentBook = book;
            ShowPage(currentIdPage, GetStartedPage(currentIdPage));

        }

        public void EndGame(int minSeconds, int pointsPerSecond, int pointsPerError)
        {
            //MetricsController.GetController().GameFinished(minSeconds, pointsPerSecond, pointsPerError);
            ViewController.GetController().LoadLevelCompleted();
        }

        public static BookViewController GetController()
        {
            return _instance;
        }

        public string GetCurrentText()
        {
            TextPage textPage = CurrentBook.Pages[AppController.GetController().GetCurrentUser().CurrentBook.GetCurrentPage().PageId] as TextPage;
            if (textPage != null)
                return textPage.Text;
            return "ERROR";
        }

        public void UnpaintWordFromIndex(List<string> splittedText, int wordIndex)
        {
            /*  splittedText[wordIndex] = splittedText[wordIndex].Substring(splittedText[wordIndex].IndexOf(">", StringComparison.Ordinal) + 1);
              splittedText[wordIndex] = splittedText[wordIndex].Substring(0, splittedText[wordIndex].IndexOf("<", StringComparison.Ordinal));*/
            TextPageView currentPageView =  GetCurrentPageView() as TextPageView;
            if(currentPageView != null) currentPageView.UpdateWord(wordIndex, splittedText[wordIndex]);
            //((TextPageView) GetCurrentPageView()).AText.text = string.Join(" ",splittedText.ToArray());
        }

        public PageView.PageView GetCurrentPageView()
        {
            return ViewController.GetController().GetCurrentPageView();
        }

        /*        public void PaintWordFromIndex(List<string> splittedText, int wordIndex)
                {
                    splittedText[wordIndex] = "<color=green>" + splittedText[wordIndex]+"</color>";
                    TextPageView textPageView = GetCurrentPageView() as TextPageView;
                    if (textPageView != null)
                        textPageView.SetText(string.Join(" ", splittedText.ToArray()));
                }*/

        public bool IsReadTogetherOn()
        {
            return ReadTogetherToggle.isOn;
        }

        public void PaintWordFromIndex(List<string> splittedText, int wordIndex, int currentLetter)
        {
            string[] mySplittedText = new string[splittedText.Count];
            splittedText.CopyTo(mySplittedText);
            mySplittedText[wordIndex] = "<color=#939FD1>" + mySplittedText[wordIndex].Substring(0, currentLetter + 1) + "</color>" + mySplittedText[wordIndex].Substring(currentLetter + 1, mySplittedText[wordIndex].Length - currentLetter - 1);
            TextPageView textPageView = GetCurrentPageView() as TextPageView;
            if (textPageView != null)
                textPageView.UpdateWord(wordIndex, mySplittedText[wordIndex]);
        }

        public bool IsTtsOn()
        {
            return TextToSpeechToggle.isOn;
        }

        public PlayTextAction GetPlayTextAction()
        {
            return (PlayTextAction)PlayPauseToggle.ToggleAction;
        }

        internal void EnablePlayerControl(bool enable)
        {
            PlayPauseToggle.transform.parent.gameObject.SetActive(enable);
            PlayPauseToggle.isOn = false;
            if (enable) RestartPlayTextAction();
        }

        public void RestartPlayTextAction()
        {
            GetPlayTextAction().Restart();
        }

        public bool IsPlaying()
        {
            return PlayPauseToggle.isOn;
        }

        public void PauseText()
        {
            PlayPauseToggle.isOn = false;
        }

        public Page.Page GetCurrentPage()
        {
            return CurrentBook.Pages[AppController.GetController().GetCurrentUser().CurrentBook.CurrentTreeNode.Value.PageId];
        }

        public void ShowDescriptionPopUp(string word, string description)
        {
            DescriptionPopUp descriptionPopUp = ViewController.GetController().LoadDescriptionPopUp().GetComponent<DescriptionPopUp>();
            descriptionPopUp.SetText(description);
            descriptionPopUp.SetWord(word);
        }

       /* public int GetCurrentPageId()
        {
            return CurrentPageId;
        }*/

        public void SaveStateOfCurrentPage()
        {
            User currentUser = AppController.GetController().GetCurrentUser();
            //StartedPage startedPage = currentUser.CurrentBook.StartedPages.Find(e => e.PageId == CurrentPageId);            
            GetCurrentPageView().SaveState(currentUser.CurrentBook.CurrentTreeNode.Value);
        }

        public void CreateStartedPage(User currentUser, Page.Page currentPage)
        {
            StartedPage startedPage = new StartedPage(currentPage.Id, currentPage.HasText() ? ((TextPage)currentPage).Text.Split(currentPage is CoverPage ? '_' : ' ').Length : 0,
                (currentPage is MultipleChoicePage ? (currentPage as MultipleChoicePage).Options.Length : 0), currentPage is MultipleChoicePage ? (currentPage as MultipleChoicePage).Options[0].GetOptionType() : OptionType.Text);

            currentUser.CurrentBook.CurrentTreeNode.AddChild(startedPage);
        }

        public void RestartBook()
        {
            GetCurrentPageView().gameObject.SetActive(false);
            User currentUser = AppController.GetController().GetCurrentUser();
            if (currentUser.CurrentBook.State == StartedBookState.Finished)
            {
                Book book = AppController.GetController().GetBook(currentUser.CurrentBook.IdBook);
                currentUser.AddNewStartedBook(book.Id, book.Pages.Count);

                Page.Page currentPage = book.GetCover();
                StartedPage startedPage = new StartedPage(currentPage.Id, currentPage.HasText() ? ((TextPage)currentPage).Text.Split(currentPage is CoverPage ? '_' : ' ').Length : 0,
                    (currentPage is MultipleChoicePage ? (currentPage as MultipleChoicePage).Options.Length : 0), currentPage is MultipleChoicePage ? (currentPage as MultipleChoicePage).Options[0].GetOptionType() : OptionType.Text);
                AppController.GetController().GetCurrentUser().CurrentBook.CurrentTreeNode = new TreeNode<StartedPage>(startedPage);

                ShowPage(startedPage.PageId, startedPage);
            }
            else
            {
                currentUser.CurrentBook.Restart();

                Page.Page currentPage = CurrentBook.GetCover();
                StartedPage startedPage = new StartedPage(currentPage.Id, currentPage.HasText() ? ((TextPage)currentPage).Text.Split(currentPage is CoverPage ? '_' : ' ').Length : 0,
                                (currentPage is MultipleChoicePage ? (currentPage as MultipleChoicePage).Options.Length : 0), currentPage is MultipleChoicePage ? (currentPage as MultipleChoicePage).Options[0].GetOptionType() : OptionType.Text);
                AppController.GetController().GetCurrentUser().CurrentBook.CurrentTreeNode = new TreeNode<StartedPage>(startedPage);

                ShowPage(startedPage.PageId, startedPage);
            }
        }

        public void UnpaintCurrentWord()
        {
            PlayTextAction playTextAction = PlayPauseToggle.ToggleAction as PlayTextAction;
            if (playTextAction != null)
                playTextAction.UnpaintCurrentWord();
            else Debug.Log("Error, no hay PlayTextAction");
        }

        public StartedPage GetStartedPage(int currentPageId)
        {
            StartedBook currentBook = AppController.GetController().GetCurrentUser().CurrentBook;
           /* List<StartedPage> startedPages = currentBook.StartedPages;
            
            return startedPages.Find(e => e.PageId == currentPageId);*/
            return currentBook.GetCurrentPage();
        }

        public void ShowAudioButton(bool show)
        {
            TextToSpeechToggle.gameObject.SetActive(show);
        }

        public void ShowReadTogetherButton(bool show)
        {
            ReadTogetherToggle.gameObject.SetActive(false);
        }

        public void ShowFormatTextButtons(bool show)
        {
            EraseToggle.gameObject.SetActive(show);
            FormatHighlightToggle.gameObject.SetActive(show);
        }

        public void CheckExecutePlayTextAction()
        {
            if (PlayPauseToggle.transform.parent.gameObject.activeSelf)
            {
                PlayPauseToggle.isOn = true;
                PlayPauseToggle.ToggleAction.Execute();
            }
            else
            {
                PlayPauseToggle.isOn = false;
            }
        }

        public string GetCurrentAssetBundle()
        {
            return CurrentBook.AssetBundle.ToLower();
        }
    }
}
