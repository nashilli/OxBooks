﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Assets.Scripts.Common;
using Assets.Scripts.FormatableText;
using UnityEngine;

namespace Assets.Scripts._Book
{
    [Serializable]
    public class StartedPage
    {
        public StartedPage(int pageId, int textLenght, int optionsLenght, OptionType optionType)
        {
            PageId = pageId;
            WordStatuses = new List<WordStatus>(textLenght);
            for (int i = textLenght - 1; i >= 0; i--)
            {
                WordStatuses.Add(new WordStatus(Color.black, new Color(0,0,0,0), new Tuple<bool, Colour>(false, new Colour(Color.white))));
            }
            OptionStatuses = new List<OptionStatus>(optionsLenght);
            for (int i = OptionStatuses.Capacity - 1; i >= 0; i--)
            {
                OptionStatuses.Add(new OptionStatus("", false, Color.white, optionType));
            }
            AttempsToSolve = 0;
        }

        public List<WordStatus> WordStatuses { get; private set; }
        public List<OptionStatus> OptionStatuses { get; private set; }
        public bool[][] AlphabetSoupBools { get; set; }
        public bool IsResolved { get; set; }
        public bool IsHintActivated { get; set; }
        public int AttempsToSolve { get; private set; }
        public int CurrentAlphabetSoupQuestion { get; set; }
        public int PageId;

        public void AddWordStatus(FormatableWord formatableWord)
        {
            WordStatuses.Add(new WordStatus(formatableWord.GetFontColor(), formatableWord.GetHighlightColor(), new Tuple<bool, Colour>(formatableWord.IsUnderlined(), new Colour(formatableWord.UnderlineColor()))));
        }

        public void AddOptionTuple(string option, bool isSelected, Color checkmarkColor, OptionType optionType)
        {
            OptionStatuses.Add(new OptionStatus(option, isSelected, checkmarkColor, optionType));
        }

        public void Restart()
        {
            foreach (WordStatus wordStatus in WordStatuses)
            {
                wordStatus.Restart();
            }
        }
    }
    [Serializable]
    public class WordStatus
    {
        public WordStatus(Color fontColor, Color highlightColor, Tuple<bool, Colour> underlineTuple)
        {
            FontColor = new Colour(fontColor);
            HighlightColor = new Colour(highlightColor);
            UnderlineTuple = underlineTuple;
        }

        public Colour FontColor { get; set; }
        public Colour HighlightColor { get; set; }
        public Tuple<bool, Colour> UnderlineTuple { get; set; }

        public void Restart()
        {
            FontColor = new Colour(Color.black);
            HighlightColor = new Colour(new Color(0,0,0,0));
            UnderlineTuple = new Tuple<bool, Colour>(false, HighlightColor);
        }
    }

    [Serializable]
    public class OptionStatus
    {
        public string Text { get; set; }
        public bool IsSelected { get; set; }
        public Colour CheckmarkColor { get; set; }

        [OptionalField(VersionAdded = 2)] public OptionType? Type;

        public OptionStatus(string text, bool isSelected, Color checkmarkColor, OptionType optionType)
        {
            Text = text;
            IsSelected = isSelected;
            CheckmarkColor = new Colour(checkmarkColor);
            //Type = optionType;
        }

        [OnDeserialized]
        private void AddType(StreamingContext sc)
        {
            if (Type == null) Type = OptionType.Text;
        }
    }

    [Serializable]
    public enum OptionType
    {
        Text, Image, Sound
    }


}