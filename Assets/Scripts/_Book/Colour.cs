﻿using System;
using UnityEngine;

namespace Assets.Scripts._Book
{
    [Serializable]
    public class Colour
    {
        public float A;
        public float R;
        public float G;
        public float B;

        public Colour(Color color)
        {
            A = color.a;
            R = color.r;
            G = color.g;
            B = color.b;
        }

        public Color GetColor()
        {
            return new Color(R, G, B, A);
        }
    }
}