﻿
using System;
using System.Collections.Generic;
using Assets.Scripts.App;
using Assets.Scripts._Book.Page;
using SimpleJSON;

namespace Assets.Scripts._Book
{
    public class Book
    {
        public long Id { get; private set; }
        public Dictionary<int, Page.Page> Pages { get; set; }

        public string Title { get; private set; }
        public int Level;
        public string Author { get; private set; }
        public string[] Genres { get; private set; }
        public string Synopsis { get; private set; }
        public string AssetBundle { get; private set; }
        public DateTime CreatedDateTime { get; private set; }


        public Language Language;


        // These three instance variables are used to the metrics and have to be
        // initialized in each child class
        protected int minSeconds;
        protected int pointsPerSecond;
        private int pointsPerError;

        protected int PointsPerError
        {
            get { return pointsPerError; } 
            set { pointsPerError = value; }
        }

        public Illustrator Illustrator { get; set; }
        public int EstimatedReadTimeInMinutes { get; set; }

        public Book(long id, string author, string synopsis, string title, string[] genres, int level, Language language, string assetBundle)
        {
            Id = id;
            Pages = new Dictionary<int, Page.Page>();
            CreatedDateTime = DateTime.Today;
            Author = author;
            Synopsis = synopsis;
            Genres = genres;  
            Title = title;
            Level = level;
            Language = language;
            AssetBundle = assetBundle.ToLower();
        }

        public void AddPage(Page.Page page)
        {
            Pages.Add(page.Id, page);
        }

        public bool UpdatePage(Page.Page page)
        {
            var found = Pages[page.Id];
            return found != null && found.Update(page);
        }

        public void RemovePage(Page.Page page)
        {
            Pages.Remove(page.Id);
        }

        public bool HasNextPage(int currentPage)
        {
            return currentPage + 1 < Pages.Count;
        }

        public int GetExercisesCount()
        {
            int exercises = 0;
            foreach (KeyValuePair<int, Page.Page> keyValuePair in Pages)
            {
                if (keyValuePair.Value is IExercise && !(keyValuePair.Value is EtpaPage)) exercises++;
            }
         
            return exercises;
        }

        public Page.Page GetCover()
        {
            return Pages[0];
        }
    }
}
