﻿using Assets.Scripts._Book;
using UnityEngine;

namespace Assets.Scripts._Levels
{

    public class TryButton : MonoBehaviour {

        public BookView BookView;

        void OnEnable()
        {
            gameObject.GetComponent<Animator>().SetBool("playCorrect", false);
            gameObject.GetComponent<Animator>().SetBool("playWrong", false);
        }

        internal void CorrectAnswer()
        {
            gameObject.GetComponent<Animator>().SetBool("playCorrect", true);
        }
        internal void WrongAnswer()
        {
            gameObject.GetComponent<Animator>().SetBool("playWrong", true);
        }
        public void OnCorrectAnswerEnd()
        {
            gameObject.GetComponent<Animator>().SetBool("playCorrect", false);
            BookView.OnCorrectAnswerAnimationEnd();
        }
        public void OnWrongAnswerEnd()
        {
            gameObject.GetComponent<Animator>().SetBool("playWrong", false);
            BookView.OnWrongAnswerAnimationEnd();
        }

        public void PlayWrongSound() {
            BookView.PlayWrongSound();
        }

        public void PlayRightSound()
        {
            BookView.PlayRightSound();
        }
    }
}