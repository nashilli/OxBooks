﻿using Assets.Scripts.FormatableText;
using Assets.Scripts.Sound;
using Assets.Scripts._Book.Page;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

namespace Assets.Scripts._Book.PageView
{
    public class EtpaView : MultplieChoiceView
    {
        public GameObject[] ImageOptions;
        public GameObject[] TextOptions;
        public GameObject[] SoundOptions;


        public override void SetEnvirorment(Page.Page aPage, StartedPage startedPage)
        {
            BookViewController.GetController().HintButton.interactable = false;
            BookViewController.GetController().SetAudioButton(aPage.AudioPath);
            BookViewController.GetController().ReadTogetherToggle.gameObject.SetActive(aPage.HasText());
            BookViewController.GetController().EraseToggle.gameObject.SetActive(aPage.HasText());
            BookViewController.GetController().FormatHighlightToggle.gameObject.SetActive(aPage.HasText());
            BookViewController.GetController().TicButton.gameObject.SetActive(false);
            BookViewController.GetController().NexPageActionButton.interactable = Utils.Utils.AnyToggleOnIn(CurrentAnswerPanel);

            //BookViewController.GetController().PlayPauseToggle.isOn = false;
            BookViewController.GetController().RestartPlayTextAction();
            BookViewController.GetController().PlayPauseToggle.transform.parent.gameObject.SetActive(BookViewController.GetController().ReadTogetherToggle.isOn || BookViewController.GetController().TextToSpeechToggle.isOn);
            if (!aPage.HasText()) return;
            BookViewController.GetController().GetPlayTextAction().ReInitialize(aPage is CoverPage ? '_' : ' ');
        }

        public override void Show(Page.Page aPage, StartedPage startedPage, int chardWidth, char charToSplit)
        {
            Show((EtpaPage) aPage, startedPage, chardWidth, charToSplit);
        }

        private void Show(EtpaPage aEtpaPage, StartedPage startedPage, int chardWidth, char charToSplit)
        {
            OptionType optionType = aEtpaPage.GetTypeOfOptions();
            switch (optionType)
            {
                case OptionType.Text:
                    foreach (GameObject imagePanel in ImageOptions) imagePanel.SetActive(false);
                    foreach (GameObject soundPanel in SoundOptions) soundPanel.SetActive(false);
                    ShowAnswerPanel(TextOptions, aEtpaPage.Options);
                    break;
                case OptionType.Image:
                    foreach (GameObject panel in TextOptions) panel.SetActive(false);
                    foreach (GameObject soundPanel in SoundOptions) soundPanel.SetActive(false);
                    ShowAnswerPanel(ImageOptions, aEtpaPage.Options);
                    break;
                case OptionType.Sound:
                    // todo
                    break;
                    
               
            }

            if (startedPage != null) ShowOptions(startedPage.OptionStatuses, optionType);
            else ShowOptions(aEtpaPage.Options);
            
            if (aEtpaPage.HasText())
            {
                AImage.gameObject.SetActive(false);
                Formatable.gameObject.SetActive(true);
                SetText(aEtpaPage.Text, chardWidth, charToSplit);
                SetSpecialWords(aEtpaPage, Formatable.GetComponentsInChildren<FormatableWord>());
                SetPrevFormatWords(startedPage);

            }
            else
            {
                // has image
                Formatable.gameObject.SetActive(false);
                AImage.gameObject.SetActive(true);
                //AImage.material = Resources.Load<Material>(aEtpaPage.ImagePath);


                string[] strings = aEtpaPage.ImagePath.Split('/');
                LoadAssets.GetLoadAssets()
                    .LoadAnAsset<Material>(BookViewController.GetController().GetCurrentAssetBundle(),
                        strings[strings.Length - 1],
                        o =>
                        {
                            AImage.material = o.GetAsset<Material>();

                        }, null);
            }
         
        }

        private void ShowOptions(List<OptionStatus> optionStatuses, OptionType optionType)
        {
            Toggle[] toggles = CurrentAnswerPanel.GetComponentsInChildren<Toggle>();
            for (int i = toggles.Length - 1; i >= 0; i--)
            {
                toggles[i].isOn = optionStatuses[i].IsSelected;
                if (optionType == OptionType.Text)
                {
                    toggles[i].GetComponentsInChildren<Text>()[1].text = optionStatuses[i].Text;

                }
                else
                {
                    Image[] images = toggles[i].GetComponentsInChildren<Image>();
                    //Material material = Resources.Load<Material>(optionStatuses[i].Text);
                    //images[0].material = material;
                    //images[1].sprite = sprite;


                    string[] strings = optionStatuses[i].Text.Split('/');
                    LoadAssets.GetLoadAssets()
                        .LoadAnAsset<Material>(BookViewController.GetController().GetCurrentAssetBundle(),
                            strings[strings.Length - 1],
                            o =>
                            {
                                images[0].material = o.GetAsset<Material>();

                            }, null);

                    toggles[i].name = optionStatuses[i].Text;
                }
                toggles[i].graphic.color = optionStatuses[i].CheckmarkColor.GetColor();
                SetOnValueChangeFunction(i, toggles[i]);
            }
        }

        private void ShowOptions(Option[] options)
        {
            OptionType optionType = options[0].GetOptionType();
            Toggle[] toggles = CurrentAnswerPanel.GetComponentsInChildren<Toggle>();
            for (int i = toggles.Length - 1; i >= 0; i--)
            {
                SetOnValueChangeFunction(i, toggles[i]);
                toggles[i].isOn = false;
                switch (optionType)
                {
                    case OptionType.Image:
                        Image[] images = toggles[i].GetComponentsInChildren<Image>();
                        //Material material = Resources.Load<Material>(options[i].ImagePath);
                        //images[0].material = material;

                        string[] strings = options[i].ImagePath.Split('/');
                        LoadAssets.GetLoadAssets()
                            .LoadAnAsset<Material>(BookViewController.GetController().GetCurrentAssetBundle(),
                                strings[strings.Length - 1],
                                o =>
                                {
                                    images[0].material = o.GetAsset<Material>();

                                }, null);

                        //images[1].sprite = sprite;
                        toggles[i].name = options[i].ImagePath;
                        break;
                    case OptionType.Text:
                        toggles[i].GetComponentsInChildren<Text>()[1].text = options[i].Text;
                        break;

                }
            }

        }

        private void ShowAnswerPanel(GameObject[] panels, Option[] options)
        {
            for (int i = panels.Length - 1; i >= 0; i--)
            {
                if (options.Length - 2 == i)
                {
                    panels[i].gameObject.SetActive(true);
                    CurrentAnswerPanel = panels[i];
                }
                else
                {
                    panels[i].gameObject.SetActive(false);
                }
            }
        }

        
    }
}