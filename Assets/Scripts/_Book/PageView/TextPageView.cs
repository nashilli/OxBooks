﻿using System.Collections.Generic;
using Assets.Scripts.Common;
using Assets.Scripts.FormatableText;
using Assets.Scripts._Book.Page;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts._Book.PageView
{
    public class TextPageView : PageView
    {

        public FormatableText.FormatableText Formatable;

        private void Show(TextPage aTextPage, StartedPage startedPage, int chardWidth, char charToSplit)
        {
            Formatable.ShowText(aTextPage.Text, chardWidth, charToSplit);
            SetSpecialWords(aTextPage, Formatable.GetComponentsInChildren<FormatableWord>());

            SetPrevFormatWords(startedPage);
        }

        protected void SetSpecialWords(TextPage aTextPage, FormatableWord[] words)
        {
            List<Tuple<List<int>, Tuple<string, string>>> tuples = aTextPage.GetIndexesOfSpecialWords();
            foreach (Tuple<List<int>, Tuple<string, string>> tuple in tuples)
            {
                foreach (int index in tuple.First)
                {
                    words[index].SetAsSpecialWord(tuple.Second.First, tuple.Second.Second);
                }
            }
        }

        protected void SetPrevFormatWords(StartedPage startedPage)
        {
            FormatableWord[] words = Formatable.GetComponentsInChildren<FormatableWord>();
            if (startedPage != null)
            {
                for (int i = words.Length - 1; i >= 0; i--)
                {
                    words[i].SetFontColor(startedPage.WordStatuses[i].FontColor.GetColor());
                    words[i].SetHighlightColor(startedPage.WordStatuses[i].HighlightColor.GetColor());
                    /* if (startedPage.WordStatuses[i].UnderlineTuple.First)
                         words[i].Underline(startedPage.WordStatuses[i].UnderlineTuple.Second.GetColor());*/
                }
            }
        }

        public override void RestartView()
        {
            foreach (FormatableLine line in Formatable.GetComponentsInChildren<FormatableLine>(true))
            {
                line.Restart();
            }
        }

        public override void Show(Page.Page aPage, StartedPage startedPage, int chardWidth, char charToSplit)
        {
            Show(aPage as TextPage, startedPage, chardWidth, charToSplit);
            //Invoke("PrintHeights", 2);
        }


        /*   private void PrintHeights()
           {
               FormatableWord[] words = Formatable.GetComponentsInChildren<FormatableWord>();
               Vector2 sizeDelta = words[0].GetComponent<RectTransform>().sizeDelta;
               for (int i = words.Length - 1; i > 0; i--)
               {
                   words[i].GetComponent<RectTransform>().sizeDelta = sizeDelta;
               }
           }*/

        public override void SetEnvirorment(Page.Page aPage, StartedPage startedPage)
        {
            BookViewController.GetController().HintButton.interactable = false;
            BookViewController.GetController().SetAudioButton(((TextPage)aPage).AudioPath);
            BookViewController.GetController().ReadTogetherToggle.gameObject.SetActive(true);
            BookViewController.GetController().EraseToggle.gameObject.SetActive(true);
            BookViewController.GetController().FormatHighlightToggle.gameObject.SetActive(true);
            BookViewController.GetController().TicButton.gameObject.SetActive(false);
            BookViewController.GetController().NexPageActionButton.interactable = true;

            //BookViewController.GetController().PlayPauseToggle.isOn = false;
            BookViewController.GetController().RestartPlayTextAction();
            BookViewController.GetController().GetPlayTextAction().ReInitialize(aPage is CoverPage ? '_' : ' ');
            bool activeSelf = BookViewController.GetController().ReadTogetherToggle.gameObject.activeSelf;
            bool isOn = BookViewController.GetController().ReadTogetherToggle.isOn;
            bool self = BookViewController.GetController().TextToSpeechToggle.gameObject.activeSelf;
            bool b = BookViewController.GetController().TextToSpeechToggle.isOn;
            bool value = (activeSelf && isOn) || (self && b);
            BookViewController.GetController().PlayPauseToggle.transform.parent.gameObject.SetActive(value);
        }

        public override void SaveState(StartedPage startedPage)
        {

            TextPageView textPageView = (TextPageView)BookViewController.GetController().GetCurrentPageView();
            FormatableWord[] formatableWords = textPageView.Formatable.GetComponentsInChildren<FormatableWord>();
            for (int i = startedPage.WordStatuses.Count - 1; i >= 0 && i < formatableWords.Length; i--)
            {
                startedPage.WordStatuses[i].FontColor = new Colour(formatableWords[i].GetFontColor());
                startedPage.WordStatuses[i].HighlightColor = new Colour(formatableWords[i].GetHighlightColor());
                //startedPage.WordStatuses[i].UnderlineTuple = new Tuple<bool, Colour>(formatableWords[i].IsUnderlined(), new Colour(formatableWords[i].UnderlineColor()));
            }
        }

        public void SetText(string text, int charWidth, char charToSplit)
        {
            
            Formatable.ShowText(text, charWidth, charToSplit);
        }

        public void UpdateWord(int wordIndex, string word)
        {
            Formatable.UpdateWord(wordIndex, word);
        }
    }
}