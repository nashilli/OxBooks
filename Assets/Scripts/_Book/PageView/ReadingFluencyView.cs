﻿using Assets.Scripts.Common.Action;
using Assets.Scripts.Sound;
using Assets.Scripts._Book.Page;
using UnityEngine.UI;

namespace Assets.Scripts._Book.PageView
{
    public class ReadingFluencyView : MultplieChoiceView
    {
        public Button PlayButton;

        
        public override void Show(Page.Page aPage, StartedPage startedPage, int chardWidth, char charToSplit)
        {
            base.Show(aPage, startedPage, chardWidth, charToSplit);
            Formatable.gameObject.SetActive(false);
            if (startedPage!= null && startedPage.IsResolved)
            {
                PlayButton.interactable = false;
            }
            else
            {
                PlayButton.onClick.AddListener(delegate
                {
                    SoundController.GetController().PlayClickSound();
                    Formatable.gameObject.SetActive(true);
                    FluencyReadingController.Execute(((TextPage) aPage).Text);
                    PlayButton.gameObject.SetActive(false);
                });
                DisableAllOptions();
            }


        }

        public override void SetEnvirorment(Page.Page aPage, StartedPage startedPage)
        {
            BookViewController.GetController().HintButton.interactable = aPage.HasText() && (startedPage == null || (!startedPage.IsHintActivated && (!startedPage.IsResolved)));
            BookViewController.GetController().HintButton.MyAction = ((IExercise)aPage).HintAction;
            BookViewController.GetController().SetAudioButton("");
            BookViewController.GetController().ReadTogetherToggle.gameObject.SetActive(false);
            BookViewController.GetController().PlayPauseToggle.transform.parent.gameObject.SetActive(false);
            BookViewController.GetController().EraseToggle.gameObject.SetActive(false);
            BookViewController.GetController().FormatHighlightToggle.gameObject.SetActive(false);
            BookViewController.GetController().TicButton.gameObject.SetActive(true);
            BookViewController.GetController().TicButton.MyAction = ((IExercise)aPage).TicAction;
            BookViewController.GetController().NexPageActionButton.interactable = (startedPage != null && startedPage.IsResolved);
        }
    }
}