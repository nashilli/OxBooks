﻿using AssetBundles;
using Assets.Scripts.FormatableText;
using Assets.Scripts._Book.Page;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts._Book.PageView
{
    public class TextAndImagePageView : TextPageView
    {

        public Image AImage;

        public void Show(TextAndImagePage aTextAndImagePage, StartedPage startedPage, int chardWidth, char charToSplit)
        {
            base.Show(aTextAndImagePage, startedPage, chardWidth, charToSplit);

            string[] strings = aTextAndImagePage.ImagePath.Split('/');
            LoadAssets.GetLoadAssets()
                .LoadAnAsset<Material>(BookViewController.GetController().GetCurrentAssetBundle(),
                    strings[strings.Length - 1],
                    o =>
                    {
                        AImage.material = o.GetAsset<Material>();

                    }, null);
           /* AssetBundleManager.LoadAssetAsync(BookViewController.GetController().GetCurrentAssetBundle(),
                aTextAndImagePage.ImagePath, typeof(Material));*/
        }

        public override void Show(Page.Page aPage, StartedPage startedPage, int chardWidth, char charToSplit)
        {
            Show(aPage as TextAndImagePage, startedPage, chardWidth, charToSplit);
        }

    }
}