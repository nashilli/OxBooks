﻿using System;
using Assets.Scripts.Sound;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.Scripts._Book.PageView
{
    public class AlphabetSoupTile : Toggle
    {

        private static float _lastTimeChecked;

        void Start()
        {
            onValueChanged.AddListener(delegate {
                SoundController.GetController().PlayClickSound();
                                                    BookViewController.GetController().TicButton.interactable =
                                                        Utils.Utils.AnyToggleOnIn(
                                                            ((LetterSoupView) BookViewController.GetController().GetCurrentPageView()).Grid.gameObject);
            });    
        }

       
        public override void OnPointerDown(PointerEventData eventData)
        {

            if (isActiveAndEnabled && Math.Abs(_lastTimeChecked - Time.time) > 0.001)
            {

                interactable = false;
                isOn = !isOn;
            }
        }

        public override void OnPointerEnter(PointerEventData eventData)
        {
            _lastTimeChecked = Time.time;
            if (Input.GetMouseButton(0) && isActiveAndEnabled) { isOn = !isOn; }
        }
    }
}