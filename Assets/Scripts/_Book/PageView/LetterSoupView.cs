﻿using System;
using Assets.Scripts._Book.Page;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts._Book.PageView
{
    public class LetterSoupView : PageView
    {
        public LetterSoup LetterSoup{ get; private set; }
        public int CurrentQuestion { get; private set; }
        public Text QuestionText;
        public Button PrevArrowButton;
        public Button NextArrowButton;
        public GridLayoutGroup Grid;

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.RightArrow) && NextArrowButton.interactable) NextQuestion();
            else if (Input.GetKeyDown(KeyCode.LeftArrow) && PrevArrowButton.interactable) PrevQuestion();
        }

        private void Show(LetterSoup letterSoup, StartedPage startedPage)
        {
            LetterSoup = letterSoup;
            FillToggles();
            CurrentQuestion = 0;
            ShowCurrentQuestion();
            UpdateArrows();
            if (startedPage != null)
            {
                LoadPrevStatus(startedPage);
            }
            //BookViewController.GetController().HintButton.interactable = false;
        }

        private void LoadPrevStatus(StartedPage startedPage)
        {
            Toggle[] toggles = Grid.GetComponentsInChildren<Toggle>();
            // this case is when mode is view
            if (startedPage.AlphabetSoupBools == null)
            {
                DisableAllTiles();
                return;
            }
            for (int i = startedPage.AlphabetSoupBools.Length - 1; i >= 0; i--)
            {
                for (int j = startedPage.AlphabetSoupBools[i].Length - 1; j >= 0; j--)
                {
                    toggles[i + j * startedPage.AlphabetSoupBools.Length].isOn = startedPage.AlphabetSoupBools[i][j];
                }
            }

            if(startedPage.IsResolved) DisableAllTiles();
            CurrentQuestion = startedPage.CurrentAlphabetSoupQuestion;
            ShowCurrentQuestion();
            UpdateArrows();
        }

        public override void SetEnvirorment(Page.Page aPage, StartedPage startedPage)
        {
            BookViewController.GetController().HintButton.interactable = false;
            BookViewController.GetController().TextToSpeechToggle.gameObject.SetActive(false);
            BookViewController.GetController().ReadTogetherToggle.gameObject.SetActive(false);
            BookViewController.GetController().PlayPauseToggle.transform.parent.gameObject.SetActive(false);
            BookViewController.GetController().EraseToggle.gameObject.SetActive(false);
            BookViewController.GetController().FormatHighlightToggle.gameObject.SetActive(false);
            BookViewController.GetController().TicButton.gameObject.SetActive(true);
            BookViewController.GetController().TicButton.interactable = (startedPage == null || !startedPage.IsResolved) && Utils.Utils.AnyToggleOnIn(Grid.gameObject);
            BookViewController.GetController().TicButton.MyAction = ((IExercise) aPage).TicAction;
            BookViewController.GetController().NexPageActionButton.interactable = startedPage != null && startedPage.IsResolved;
     

        }

        public override void SaveState(StartedPage startedPage)
        {
            if (startedPage.AlphabetSoupBools == null)
            {
                startedPage.AlphabetSoupBools = new bool[11][];
                for (int i = startedPage.AlphabetSoupBools.Length - 1; i >= 0; i--)
                {
                    startedPage.AlphabetSoupBools[i] = new bool[6];
                }
            }
            Toggle[] toggles = Grid.GetComponentsInChildren<Toggle>();
            for (int i = startedPage.AlphabetSoupBools.Length - 1; i >= 0; i--)
            {
                for (int j = startedPage.AlphabetSoupBools[i].Length - 1; j >= 0; j--)
                {
                    startedPage.AlphabetSoupBools[i][j] = toggles[i + j*startedPage.AlphabetSoupBools.Length].isOn;
                }
            }

            startedPage.CurrentAlphabetSoupQuestion = CurrentQuestion;

        }


        private void ShowCurrentQuestion()
        {
            QuestionText.text = (CurrentQuestion + 1) + ". " + LetterSoup.ObjectiveWords[CurrentQuestion].Question;
        }


        private void FillToggles()
        {
            Toggle[] toggles = Grid.GetComponentsInChildren<Toggle>();
            for (int i = 0; i < LetterSoup.Letters.Length; i++)
            {
                for (int j = 0; j < LetterSoup.Letters[i].Length; j++)
                {
                    int index0 = i + j * LetterSoup.Letters.Length;
                    toggles[index0].isOn = false;
                    toggles[index0].GetComponentInChildren<Text>().text = LetterSoup.Letters[i][j];
                }
            }
        }


        public void NextQuestion()
        {
            CurrentQuestion++;
            ShowCurrentQuestion();
            UpdateArrows();
        }

        private void UpdateArrows()
        {
            PrevArrowButton.interactable = CurrentQuestion > 0;
            NextArrowButton.interactable = CurrentQuestion + 1 < LetterSoup.ObjectiveWords.Count;
        }

        public void PrevQuestion()
        {
            CurrentQuestion--;
            ShowCurrentQuestion();
            UpdateArrows();
        }


        public override void RestartView()
        {
            RestartAllTiles();
            CurrentQuestion = 0;
        }

        private void RestartAllTiles()
        {
            foreach (AlphabetSoupTile child in Grid.GetComponentsInChildren<AlphabetSoupTile>())
            {
                child.enabled = true;
                child.isOn = false;
                child.GetComponentInChildren<Text>().text = "";
                child.interactable = true;
            }
        }

        public override void Show(Page.Page aPage, StartedPage startedPage, int chardWidth, char charToSplit)
        {
            Show(aPage as LetterSoup, startedPage);
        }

        public bool[][] ObtainBoolMatrix()
        {
            Toggle[] toggles = Grid.GetComponentsInChildren<Toggle>();
            bool[][] toReturn = new bool[LetterSoup.Letters.Length][];
            for (int i = toReturn.Length - 1; i >= 0; i--)
            {
                toReturn[i] = new bool[LetterSoup.Letters[i].Length];
                for (int j = toReturn[i].Length - 1; j >= 0; j--)
                {
                    toReturn[i][j] = toggles[i + j * LetterSoup.Letters.Length].isOn;
                }
            }

            return toReturn;
        }

      /*  public void OnPointerEnter(int toggle)
        {
            _lastTimeChecked = Time.time;
            Toggle currentToggle = Grid.GetComponentsInChildren<Toggle>()[toggle];
            if (Input.GetMouseButton(0) && currentToggle.isActiveAndEnabled) { currentToggle.isOn = !currentToggle.isOn; }
        }*/

     /*   public void OnClickToggle()
        {
            bool atLeastOneIsOn = false;
            Toggle[] toggles = Grid.GetComponentsInChildren<Toggle>();
            for (int i = 0; i < toggles.Length; i++)
            {
                if (toggles[i].isOn)
                {
                    atLeastOneIsOn = true;
                    break;
                }
            }

           // tryBtn.enabled = oneOn;
        }

        public void OnPointerDown(int toggle)
        {
            Toggle currentToggle = Grid.GetComponentsInChildren<Toggle>()[toggle];

            if (currentToggle.isActiveAndEnabled && Math.Abs(_lastTimeChecked - Time.time) > 0.001)
            {
                currentToggle.interactable = false;
                currentToggle.isOn = !currentToggle.isOn;
            }


        }*/

        public void DisableAllTiles()
        {
            foreach (AlphabetSoupTile soupTile in Grid.GetComponentsInChildren<AlphabetSoupTile>())
            {
                soupTile.interactable = false;
                soupTile.enabled = false;
            }
        }
    }
}