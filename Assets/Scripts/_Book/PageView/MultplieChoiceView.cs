﻿using System.Collections.Generic;
using Assets.Scripts.App;
using Assets.Scripts.Common;
using Assets.Scripts.FormatableText;
using Assets.Scripts.Sound;
using Assets.Scripts._Book.Page;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts._Book.PageView
{
    public class MultplieChoiceView : TextAndImagePageView
    {
        [SerializeField] private GameObject[] OneCorrectOptions;
        [SerializeField] private GameObject[] MultipleCorrectOptions;
        [SerializeField] private Text Question;

        void Update()
        {
            if ((Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetKeyDown(KeyCode.Return)) &&
                BookViewController.GetController().TicButton.interactable)
            {
                SoundController.GetController().PlayClickSound();
                BookViewController.GetController().TicButton.MyAction.Execute();;
            }
        }

        public override void SaveState(StartedPage startedPage)
        {
            base.SaveState(startedPage);

            Toggle[] toggles = ((MultplieChoiceView)BookViewController.GetController().GetCurrentPageView()).GetComponentsInChildren<Toggle>();
            Option[] options = ((MultipleChoicePage)BookViewController.GetController().GetCurrentPage()).Options;

            for (int i = startedPage.OptionStatuses.Count - 1; i >= 0; i--)
            {
                OptionType optionType = options[i].GetOptionType();
                startedPage.OptionStatuses[i].Text = optionType == OptionType.Text ? toggles[i].GetComponentsInChildren<Text>()[1].text : optionType == OptionType.Image ? options[i].ImagePath : options[i].SoundPath;
                startedPage.OptionStatuses[i].IsSelected = toggles[i].isOn;
                startedPage.OptionStatuses[i].CheckmarkColor = new Colour(toggles[i].graphic.color);
            }

        }

        public override void RestartView()
        {
            // the page was never showed
            if (CurrentAnswerPanel == null) return;
            base.RestartView();

            foreach (Toggle child in CurrentAnswerPanel.GetComponentsInChildren<Toggle>())
            {
                child.graphic.color = Color.white;
                child.interactable = true;
                child.isOn = false;
            }
        }

        internal GameObject CurrentAnswerPanel;

        public override void Show(Page.Page aPage, StartedPage startedPage, int chardWidth, char charToSplit)
        {
            Show(aPage as MultipleChoicePage, startedPage, chardWidth, charToSplit);
        }

        private void Show(MultipleChoicePage aChoicePage, StartedPage startedPage, int chardWidth, char charToSplit)
        {
            switch (aChoicePage.GetCorrectAnswers())
            {
                case 1: 
                    // Only one correct, so multiple must be invisble
                    foreach (GameObject multipleCorrectOption in MultipleCorrectOptions) multipleCorrectOption.SetActive(false);
                    ShowAnswerPanel(OneCorrectOptions, aChoicePage.Options);
                    break;
                default:
                    // Multiple correct, so one correct must be invisble
                    foreach (GameObject oneCorrectOptions in OneCorrectOptions) oneCorrectOptions.SetActive(false);
                    ShowAnswerPanel(MultipleCorrectOptions, aChoicePage.Options);
                    break;
            }
            ShowQuestion(aChoicePage.Question);
            
            if (startedPage != null) ShowOptions(startedPage.OptionStatuses);
            else ShowOptions(aChoicePage.Options);

            if (aChoicePage.HasText())
            {
                AImage.gameObject.SetActive(false);
                Formatable.gameObject.SetActive(true);
                SetText(aChoicePage.Text, chardWidth, charToSplit);
                SetSpecialWords(aChoicePage, Formatable.GetComponentsInChildren<FormatableWord>());
                SetPrevFormatWords(startedPage);
         
            }
            else
            {
                // has image
                Formatable.gameObject.SetActive(false);
                AImage.gameObject.SetActive(true);
                string[] strings = aChoicePage.ImagePath.Split('/');
                LoadAssets.GetLoadAssets()
                    .LoadAnAsset<Material>(BookViewController.GetController().GetCurrentAssetBundle(),
                        strings[strings.Length - 1],
                        o =>
                        {
                            AImage.material = o.GetAsset<Material>();

                        }, null);
                //AImage.sprite = Resources.Load<Sprite>(aChoicePage.ImagePath);
            }

            if (startedPage != null && startedPage.IsResolved)
            {
                BookViewController.GetController().HintButton.interactable = false;
                DisableAllOptions();
                BookViewController.GetController().TicButton.interactable = false;

            }
        }

        private void ShowOptions(List<OptionStatus> optionStatuses)
        {
            Toggle[] toggles = CurrentAnswerPanel.GetComponentsInChildren<Toggle>();
            for (int i = toggles.Length - 1; i >= 0; i--)
            {
                toggles[i].isOn = optionStatuses[i].IsSelected;
                toggles[i].GetComponentsInChildren<Text>()[1].text = optionStatuses[i].Text;
                toggles[i].graphic.color = optionStatuses[i].CheckmarkColor.GetColor();
                SetOnValueChangeFunction(i, toggles[i]);
            }
        }

        public void SetOnValueChangeFunction(int i, Toggle toggle)
        {
            toggle.onValueChanged.AddListener(
                delegate
                {
                    SoundController.GetController().PlayClickSound();
                    UnPaintAllToggles();
                    if (this is EtpaView)
                    {
                        BookViewController.GetController().NexPageActionButton.interactable = Utils.Utils.AnyToggleOnIn(toggle.transform.parent.gameObject);
                        
                    }
                    else
                    {
                        BookViewController.GetController().TicButton.interactable = Utils.Utils.AnyToggleOnIn(toggle.transform.parent.gameObject);

                    }
                });
        }

        

        protected void UnPaintAllToggles()
        {
            foreach (Toggle toggle in CurrentAnswerPanel.GetComponentsInChildren<Toggle>())
            {
                toggle.graphic.color = new Color32(55, 164, 175, 255);
            }
        }

        private void ShowQuestion(string text)
        {
            Question.text = text;
        }

        private void ShowOptions(Option[] options)
        {
            Toggle[] toggles = Utils.Utils.ShuffleArray(CurrentAnswerPanel.GetComponentsInChildren<Toggle>());
            for (int i = toggles.Length - 1; i >= 0; i--)
            {
                SetOnValueChangeFunction(i, toggles[i]);
                toggles[i].isOn = false;
                toggles[i].GetComponentsInChildren<Text>()[1].text = options[i].Text;
            }

        }

        private void ShowAnswerPanel(GameObject[] optionsGameObjects, Option[] options)
        {
            if (options.Length == 3)
            {
                optionsGameObjects[0].SetActive(true);
                CurrentAnswerPanel = optionsGameObjects[0];
                optionsGameObjects[1].SetActive(false);
            }
            else
            {
                // options.Length is 4
                optionsGameObjects[0].SetActive(false);
                optionsGameObjects[1].SetActive(true);
                CurrentAnswerPanel = optionsGameObjects[1];
            }


        }

        public string[] GetSelectedAnswers()
        {
            List<string> selectedAnswers = new List<string>();
            Toggle[] selectedToggles = GetSelectedToggles();
            for (int i = selectedToggles.Length - 1; i >= 0; i--)
            {
                OptionType optionType = (BookViewController.GetController().GetCurrentPage() as MultipleChoicePage).Options[0].GetOptionType();
                selectedAnswers.Add(optionType == OptionType.Text ? selectedToggles[i].GetComponentsInChildren<Text>()[1].text : selectedToggles[i].name);
            }

            return selectedAnswers.ToArray();
        }

        private Toggle[] GetSelectedToggles()
        {
            List<Toggle> selectedToggles = new List<Toggle>();
            Toggle[] allToggles = CurrentAnswerPanel.GetComponentsInChildren<Toggle>();
            for (int i = allToggles.Length - 1; i >= 0; i--)
            {
                if (allToggles[i].isOn)
                {
                    selectedToggles.Add(allToggles[i]);
                }
            }
            
            return selectedToggles.ToArray();
        }

        public void PaintAnswers(bool[] answers, string[] selectedAnswers)
        {
            Toggle[] toggles = GetSelectedToggles();

            for (int i = toggles.Length - 1; i >= 0; i--)
            {
                for (int j = selectedAnswers.Length - 1; j >= 0; j--)
                {
                    if (!selectedAnswers[j].Equals(toggles[i].GetComponentsInChildren<Text>()[1].text)) continue;
                    toggles[i].graphic.color = answers[j]
                        ? new Color32(22, 183, 103, 255)
                        : new Color32(234, 91, 55, 255);
                    break;
                }
   
                
            }
        }

        public void DisableAllOptions()
        {
            foreach (Toggle toggle in CurrentAnswerPanel.GetComponentsInChildren<Toggle>())
            {
                toggle.interactable = false;
                //toggle.enabled = false;
            }
        }

        public void EnableAllOptions()
        {
            foreach (Toggle toggle in CurrentAnswerPanel.GetComponentsInChildren<Toggle>())
            {
                toggle.interactable = true;
                toggle.enabled = true;
            }

        }    

        public override void SetEnvirorment(Page.Page aPage, StartedPage startedPage)
        {
            BookViewController.GetController().HintButton.interactable = aPage.HasText() && (startedPage == null || (!startedPage.IsHintActivated && (!startedPage.IsResolved)));
            BookViewController.GetController().HintButton.MyAction = ((IExercise) aPage).HintAction;
            BookViewController.GetController().SetAudioButton(((TextPage)aPage).AudioPath);
            ActionToggle readTogetherToggle = BookViewController.GetController().ReadTogetherToggle;
            readTogetherToggle.gameObject.SetActive(aPage.HasText());
            BookViewController.GetController().EraseToggle.gameObject.SetActive(aPage.HasText());
            BookViewController.GetController().FormatHighlightToggle.gameObject.SetActive(aPage.HasText());
            BookViewController.GetController().TicButton.gameObject.SetActive(true);
            BookViewController.GetController().TicButton.MyAction = ((IExercise) aPage).TicAction;
            BookViewController.GetController().TicButton.interactable = (startedPage == null || !startedPage.IsResolved) && Utils.Utils.AnyToggleOnIn(CurrentAnswerPanel);
            BookViewController.GetController().NexPageActionButton.interactable = (startedPage != null && startedPage.IsResolved);
            ActionToggle textToSpeechToggle = BookViewController.GetController().TextToSpeechToggle;
            BookViewController.GetController().PlayPauseToggle.transform.parent.gameObject.SetActive((readTogetherToggle.isActiveAndEnabled && readTogetherToggle.isOn) || (textToSpeechToggle.isActiveAndEnabled && textToSpeechToggle.isOn));
            if (!aPage.HasText()) return;
            //BookViewController.GetController().PlayPauseToggle.isOn = false;
            BookViewController.GetController().RestartPlayTextAction();
            BookViewController.GetController().GetPlayTextAction().ReInitialize(aPage is CoverPage ? '_' : ' ');

        }
    }
}