﻿using UnityEngine;

namespace Assets.Scripts._Book.PageView
{
    public abstract class PageView : MonoBehaviour
    {

        public void LoadPage(Page.Page aPage, StartedPage startedPage, int chardWidth, char charToSplit)
        {
            RestartView();
            Show(aPage, startedPage, chardWidth, charToSplit);
            SetEnvirorment(aPage, startedPage);
        }

        public abstract void RestartView();

        public abstract void Show(Page.Page aPage, StartedPage startedPage, int chardWidth, char charToSplit);

        public abstract void SetEnvirorment(Page.Page aPage, StartedPage startedPage);

        public abstract void SaveState(StartedPage startedPage);
    }
}