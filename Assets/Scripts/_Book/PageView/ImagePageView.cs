﻿using Assets.Scripts._Book.Page;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts._Book.PageView
{
    public class ImagePageView : PageView
    {
        public Image AImage;

        private void Show(ImagePage aImagePage)
        {
            string[] strings = aImagePage.ImagePath.Split('/');
            LoadAssets.GetLoadAssets()
                .LoadAnAsset<Material>(BookViewController.GetController().GetCurrentAssetBundle(),
                    strings[strings.Length - 1],
                    o =>
                    {
                        AImage.material = o.GetAsset<Material>();

                    }, null);
            //AImage.material = Resources.Load<Material>(aImagePage.ImagePath);
            //AImage.sprite = Resources.Load<Sprite>(aImagePage.ImagePath);
        }


        public override void RestartView()
        {
            
        }

        public override void Show(Page.Page aPage, StartedPage startedPage, int chardWidth, char charToSplit)
        {
            Show(aPage as ImagePage);
        }

        public override void SetEnvirorment(Page.Page aPage, StartedPage startedPage)
        {

            BookViewController.GetController().HintButton.interactable = false;
            BookViewController.GetController().SetAudioButton(aPage.AudioPath);
            BookViewController.GetController().ReadTogetherToggle.gameObject.SetActive(false);
            BookViewController.GetController().EraseToggle.gameObject.SetActive(false);
            BookViewController.GetController().FormatHighlightToggle.gameObject.SetActive(false);
            BookViewController.GetController().TicButton.gameObject.SetActive(false);
            BookViewController.GetController().NexPageActionButton.interactable = true;


            //BookViewController.GetController().PlayPauseToggle.isOn = false;
            BookViewController.GetController().RestartPlayTextAction();
            BookViewController.GetController().GetPlayTextAction().ReInitialize(aPage is CoverPage ? '_' : ' ');
            bool activeSelf = BookViewController.GetController().ReadTogetherToggle.isActiveAndEnabled;
            bool isOn = BookViewController.GetController().ReadTogetherToggle.isOn;
            bool self = BookViewController.GetController().TextToSpeechToggle.isActiveAndEnabled;
            bool b = BookViewController.GetController().TextToSpeechToggle.isOn;
            bool value = (activeSelf && isOn) || (self && b);
            BookViewController.GetController().PlayPauseToggle.transform.parent.gameObject.SetActive(value);
        }

        public override void SaveState(StartedPage startedPage)
        {
            return;
        }
    }
}