﻿
using System.Collections.Generic;

namespace Assets.Scripts._Book
{
    public class Illustrator
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public List<string> ContactForms { get; set; }
    }
}
