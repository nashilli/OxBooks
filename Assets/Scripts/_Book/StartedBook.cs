﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Assets.Scripts.Metrics;
using UnityEngine;

namespace Assets.Scripts._Book
{
    [Serializable]
    public class StartedBook
    {
        public long IdBook { get; private set; }
        // Deprecated
        public List<StartedPage> StartedPages { get; private set; }
        public int BookVersion { get; private set; }
        // the index that reference to startedPages - Deprecated
        public int CurrentPage { get; set; }
        public StartedBookState State { get; set; }

        public BookMetrics Metrics;
        public Mode Mode;

        [OptionalField(VersionAdded = 2)] public TreeNode<StartedPage> CurrentTreeNode;


        public StartedBook(long id, int pagesLenght, Mode mode)
        {
            Metrics = new BookMetrics();
            //StartedPages = new List<StartedPage>(pagesLenght);
            IdBook = id;
            State = StartedBookState.InProgress;
            Mode = mode;
        }

        public int GetResolvedExercises()
        {
            int resolvedExercises = 0;           
            foreach (StartedPage page in GetRootNode().Flatten())
            {
                if (page.IsResolved) resolvedExercises++;
            }
            return resolvedExercises;
        }

        public void Restart()
        {
            Metrics.Restart();
            if(Mode == Mode.Play) CurrentTreeNode = null;
            else { 
                foreach (StartedPage startedPage in CurrentTreeNode.Flatten())
                {
                    startedPage.Restart();
                }
            }
            State = StartedBookState.InProgress;
        }

        public StartedPage GetCurrentPage()
        {
            if (CurrentTreeNode == null) return null;
            return CurrentTreeNode.Value;
        }

        public TreeNode<StartedPage> GetRootNode()
        {
            TreeNode<StartedPage> rootNode = CurrentTreeNode;
            while (rootNode.Parent != null)
            {
                rootNode = rootNode.Parent;
            }
            return rootNode;
        }

        [OnDeserialized]
        private void UpdateToLastVersion(StreamingContext sc)
        {
            if (StartedPages != null && StartedPages.Count != 0)
            {
                ConvertToTreeStructure();
                return;
            }

            if (CurrentTreeNode != null && StartedPages != null)
            {
                throw new Exception("OnDesrializing");
            }

        }

        private void ConvertToTreeStructure()
        {
            CurrentTreeNode = new TreeNode<StartedPage>(StartedPages[0]);
            for (int i = 1; i < StartedPages.Count; i++)
            {
                CurrentTreeNode.AddChild(StartedPages[i]);
                CurrentTreeNode = CurrentTreeNode.Children[0];
            }
            StartedPages.Clear();
            StartedPages = null;
        }
    }

    public enum StartedBookState
    {
        InProgress, Finished
    }

    public enum Mode
    {
        Play,
        View
    }
}