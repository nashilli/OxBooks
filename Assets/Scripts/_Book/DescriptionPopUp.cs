﻿using System;
using Assets.Scripts.Common;
using Assets.Scripts.Sound;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts._Book
{
    public class DescriptionPopUp : MonoBehaviour
    {
        public Text Word;
        public Text Description;

        void Start()
        {
            GetComponentInChildren<Button>().onClick.AddListener(delegate
            {
                SoundController.GetController().PlayClickSound();
                Destroy(gameObject);
            });
        }

        public void SetText(string text)
        {
            Description.text = text;
        }

        public void SetWord(string word)
        {
            Word.text = word.Substring(0, 1).ToUpper() + word.Substring(1);
        }  
    }
}