﻿using Assets.Scripts.Common.Action;
using UnityEngine.UI;
using UnityEngine;

namespace Assets.Scripts.Common
{
    public class ActionToggle : Toggle
    {
        public ToggleAction ToggleAction { get; set; }

        void Start()
        {
            onValueChanged.AddListener(delegate { OnValueChange(); });
        }

        private void OnValueChange()
        {
            if(ToggleAction == null) return;
            if(isOn) ToggleAction.Execute();
            else ToggleAction.Pause();
        }
    }
}