﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Sound;

public class ExitButton : MonoBehaviour {

    public void Exit()
    {
        SoundController.GetController().PlayClickSound();
        Application.Quit();
    }

}
