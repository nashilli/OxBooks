﻿using System;
using UnityEngine.UI;
using System.Globalization;
using I18N;

namespace Assets.Scripts.Common {
	public class Views {
		private Views() { }

		/** If using toggle group, make sure that 'Allow switch off' in toggle group is on. */
		public static void TogglesOff(Toggle[] toggles) {
			for(int i = 0; i < toggles.Length; i++) toggles[i].isOn = false;
		}

		public static void TogglesEnabled(Toggle[] toggles, bool enabled) {
			for (int i = 0; i < toggles.Length; i++) toggles[i].enabled = enabled;
		}

		public static void ButtonsEnabled(Button[] buttons, bool enabled) {
			for (int i = 0; i < buttons.Length; i++) buttons[i].enabled = enabled;
		}

		public static void SetActiveToggle(Toggle toggle, bool active){
			toggle.gameObject.SetActive(active);
		}

		public static void SetActiveButton(Button button, bool active){
			button.gameObject.SetActive(active);
		}

		public static void SetActiveImage(Image image, bool active) {
			image.gameObject.SetActive(active);
		}

		public static bool IsAnyActive(Toggle[] pizzas) {
			foreach(Toggle pizza in pizzas) {
				if(pizza.IsActive()) return true;
			}
			return false;
		}

		public static decimal NumberFromString(string number){
			return decimal.Parse(number, CultureInfo.CreateSpecificCulture(I18n.GetLocale()));
		}

		public static string GetNumberText(decimal number) {
			var decimalSplit = number.ToString(CultureInfo.CreateSpecificCulture("en-US")).Split('.');

			var decimals = decimalSplit.Length > 1 ? TrimLastZeroes(decimalSplit[1]).Length : 0;
			return number.ToString("N" + decimals, CultureInfo.CreateSpecificCulture(I18n.GetLocale()));
		}

		private static string TrimLastZeroes(string decimalPart){
			if(decimalPart.EndsWith("0")) return TrimLastZeroes(decimalPart.Substring(0, decimalPart.Length - 1));
			return decimalPart;
		}
	}
}