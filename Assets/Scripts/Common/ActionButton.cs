﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Common
{
    public class ActionButton : Button
    {
        public Action.Action MyAction { get; set; }

        protected override void Start()
        {
            base.Start();
            onClick.AddListener(OnClick);
        }

        private void OnClick()
        {
            MyAction.Execute();
        }
    }
}