﻿using System;
using Assets.Scripts.App;
using Assets.Scripts.Explore;
using Assets.Scripts.MyThings;
using Assets.Scripts.Profile;
using Assets.Scripts.Sound;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Common
{
    public class Topbar : MonoBehaviour
    {

        public Toggle ExploreButton;
        public Toggle MyProfileButton;
        public Toggle MytThingsButton;

        void Start()
        {
            GameObject currentGameObject = ViewController.GetController().GetCurrentGameObject();

            if (currentGameObject.GetComponent<ExploreView>() != null) ExploreButton.isOn = true;
            else if (currentGameObject.GetComponent<MyThingsView>() != null) MytThingsButton.isOn = true;
            else MyProfileButton.isOn = true;
        }


        public void OnClickMyThings()
        {
            SoundController.GetController().PlayClickSound();
            if(ViewController.GetController().GetCurrentGameObject().GetComponentInChildren<MyThingsView>() == null) ViewController.GetController().LoadMyThings();
        }

        public void OnClickExplore()
        {
            SoundController.GetController().PlayClickSound();
            if (ViewController.GetController().GetCurrentGameObject().GetComponentInChildren<ExploreView>() == null) ViewController.GetController().LoadExplore();

        }

        public void OnClickProfile()
        {
            SoundController.GetController().PlayClickSound();
            if (ViewController.GetController().GetCurrentGameObject().GetComponentInChildren<ProfileView>() == null) ViewController.GetController().LoadProfile();

        }
    }
}
