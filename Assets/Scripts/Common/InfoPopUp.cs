﻿using Assets.Scripts.Sound;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Common
{
    public class InfoPopUp : MonoBehaviour {

        public Button CloseButton;
        public Text Messagge;    
        // Use this for initialization
        void Start () {
            CloseButton.onClick.AddListener(delegate
            {
                SoundController.GetController().PlayClickSound();
                Destroy(this.gameObject.transform.parent.gameObject);
            });
        }
	
        public void SetMessage(string messagge)
        {
            Messagge.text = messagge;
        }
    }
}
