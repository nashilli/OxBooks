﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

	public static GameObject itemBeingDragged;
	Vector3 startPosition;

	public void OnBeginDrag(PointerEventData eventData) {
		transform.SetAsLastSibling();
		itemBeingDragged = gameObject;
		startPosition = transform.position;
	}

	public void OnDrag(PointerEventData eventData) {
		transform.position = Input.mousePosition;
	}

	public void OnEndDrag(PointerEventData eventData) {
		itemBeingDragged = null;
	}
}