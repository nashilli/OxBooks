﻿using Assets.Scripts.App;
using Assets.Scripts.Sound;
using Assets.Scripts._Book;
using Assets.Scripts._Book.Page;

namespace Assets.Scripts.Common.Action
{
    public class ViewBookAction : Action
    {
        public ViewBookAction(Book book)
        {
            Book = book;
        }

        public Book Book { get; private set; }


        public override void Execute()
        {
            ViewController.GetController().ShowLoading();
            SoundController.GetController().StopMusic();
            ViewController.GetController().LoadBookView(OnBookViewControllerLoaded);
        }

        private void OnBookViewControllerLoaded()
        {
            User currentUser = AppController.GetController().GetCurrentUser();
            if (currentUser.CurrentBook == null)
                currentUser.CurrentBook = new StartedBook(Book.Id, Book.Pages.Count, Mode.View);
            if (currentUser.CurrentBook.CurrentTreeNode == null)
            {
                /*  for (var i = 0; i < Book.Pages.Count; i++)
                {
                    // todo --> ver modo ver con ETPA. En duda el todo
                  /*  currentUser.CurrentBook.StartedPages.Add(
                        new StartedPage(Book.Pages[i].Id, Book.Pages[i].HasText() ? ((TextPage) Book.Pages[i]).Text.Split(' ').Length : 0,
                            (Book.Pages[i] is MultipleChoicePage
                                ? (Book.Pages[i] as MultipleChoicePage).Options.Length
                                : 0)));
                    currentUser.CurrentBook.StartedPages[i].IsResolved = true;#1#
                }*/

                BookViewController.GetController().CurrentBook = Book;

                Page currentPage = Book.GetCover();
                StartedPage startedPage = new StartedPage(currentPage.Id,
                    currentPage.HasText() ? ((TextPage) currentPage).Text.Split(currentPage is CoverPage ? '_' : ' ').Length : 0,
                    (currentPage is MultipleChoicePage ? (currentPage as MultipleChoicePage).Options.Length : 0),
                    currentPage is MultipleChoicePage
                        ? (currentPage as MultipleChoicePage).Options[0].GetOptionType()
                        : OptionType.Text);

                AppController.GetController().GetCurrentUser().CurrentBook.CurrentTreeNode =
                    new TreeNode<StartedPage>(startedPage);


                BookViewController.GetController().ShowPage(startedPage.PageId, startedPage);
            }
            else
            {
                BookViewController.GetController().InitBook(Book, currentUser.CurrentBook.GetCurrentPage().PageId);
            }
            ViewController.GetController().HideLoading();

        }
    }
}