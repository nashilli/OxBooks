﻿using Assets.Scripts._Book;
using UnityEngine;

namespace Assets.Scripts.Common.Action
{
    public class EnablePlayerControlAction : EnableGameObjectAction
    {

        public EnablePlayerControlAction(GameObject gameObject) : base(gameObject)
        {
        }

        public override void Execute()
        {
            if (BookViewController.GetController().ReadTogetherToggle.isOn && BookViewController.GetController().TextToSpeechToggle.isOn) return;
            base.Execute();
        }

        public override void Pause()
        {
            if(BookViewController.GetController().ReadTogetherToggle.isOn || BookViewController.GetController().TextToSpeechToggle.isOn) return;
            base.Pause();
        }

        
    }
}