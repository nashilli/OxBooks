﻿namespace Assets.Scripts.Common.Action
{
    public class StopSpeakAction : Action
    {
        public override void Execute()
        {
           TTSManager.Stop();
        }
    }
}