﻿using Assets.Scripts.Sound;
using Assets.Scripts._Book;

namespace Assets.Scripts.Common.Action
{
    public class FinishPlayTextAction : Action
    {
        public override void Execute()
        {
            BookViewController.GetController().PlayPauseToggle.isOn = false;
            if (BookViewController.GetController().GetCurrentPage().HasText())
            {
                BookViewController.GetController().UnpaintCurrentWord();

            }
            BookViewController.GetController().RestartPlayTextAction();
            
            
            //Speaker.GetSpeaker().Silence();

            // only for standalone purpses
            SoundController.GetController().StopExtraClip();
        }

    }
}