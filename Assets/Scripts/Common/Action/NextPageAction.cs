﻿using Assets.Scripts.App;
using Assets.Scripts.Sound;
using Assets.Scripts._Book;

namespace Assets.Scripts.Common.Action
{
    public class NextPageAction : Action
    {
        public override void Execute()
        {

            Coroutiner.GetCoroutiner().Stop();      
            SoundController.GetController().StopExtraClip();
/*
            AppController.GetController().GetCurrentUser().CurrentBook.CurrentTreeNode.Value.IsResolved = true;
*/
/*            AppController.GetController().GetCurrentUser().CurrentBook.
            StartedPages.Find(e => e.PageId == BookViewController.GetController().CurrentPageId).IsResolved = true;*/
            BookViewController.GetController().SaveStateOfCurrentPage();
            int nextIdPage = BookViewController.GetController().GetCurrentPage().GetNextPageId();
            BookViewController.GetController().GetCurrentPageView().gameObject.SetActive(false);
            ViewController.GetController().HideDescriptionPopUp();
            StartedBook currentStartedBook = AppController.GetController().GetCurrentUser().CurrentBook;
            Book currentBook = AppController.GetController().GetBook(currentStartedBook.IdBook);

            //currentStartedBook.CurrentPage++;

            // caso en el que ya fui de la página en la que estoy a la siguiente elegida
            StartedPage nextStarted = SearchStartedNext(currentStartedBook, nextIdPage);
            if (nextStarted != null)
            {
                //BookViewController.GetController().CurrentPageId = nextStarted.PageId;
                BookViewController.GetController().ShowPage(nextIdPage , nextStarted);
            }
           
            // caso en el que nunca se haya tocado el botón siguiente previamente
            else
            {
                // end
                if (nextIdPage == -1 || !currentBook.Pages.ContainsKey(nextIdPage))
                {
                    if (currentStartedBook.Mode == Mode.Play)
                    {
                        currentStartedBook.State = StartedBookState.Finished;
                        currentStartedBook.Metrics.CalculateScore();
                        currentStartedBook.Metrics.CalculateStars();
                    }
                    ViewController.GetController().LoadLevelCompleted();
                    return;
                } // show page with nextIdPage
                else
                {
                 //   BookViewController.GetController().CurrentPageId = nextIdPage;
                    BookViewController.GetController().CreateStartedPage(AppController.GetController().GetCurrentUser(), BookViewController.GetController().CurrentBook.Pages[nextIdPage]);
                    nextStarted = SearchStartedNext(currentStartedBook, nextIdPage);
                    BookViewController.GetController().ShowPage(nextIdPage, null);
                }
            }
        }

        private StartedPage SearchStartedNext(StartedBook startedBook, int nextIdPage)
        {
            foreach (TreeNode<StartedPage> node in startedBook.CurrentTreeNode.Children)
            {
                if (node.Value.PageId == nextIdPage)
                {
                    startedBook.CurrentTreeNode = node;
                    return node.Value;
                }
            }
            return null;
        }
    }
}