﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.App;
using Assets.Scripts.Sound;
using Assets.Scripts._Book;
using Assets.Scripts._Book.Page;
using UnityEngine;

namespace Assets.Scripts.Common.Action
{
    public class PlayTextAction : ToggleAction
    {
        private List<string> _words;
        private int _currentWord;
        private int _currentLetter;
        public static float SecondsPerLetter = 0.25f;

        private bool _readTogether;
        private bool _textToSpeech;
        public bool IsPlaying;

        // this variable is for standalone version
        private AudioClip _audioClip;

        public void Restart()
        {
            Coroutiner.GetCoroutiner().Stop();
            _currentWord = 0;
            _currentLetter = -1;
        }

        public void ReInitialize(char toSplit)
        {
            _words = BookViewController.GetController().GetCurrentText().Split(toSplit).ToList();
        }

        public override void Execute()
        {
            if(_words == null) ReInitialize(BookViewController.GetController().GetCurrentPage() is CoverPage ? '_' : ' ');
            IsPlaying = BookViewController.GetController().IsPlaying();
            _readTogether = BookViewController.GetController().IsReadTogetherOn();
            _textToSpeech = BookViewController.GetController().IsTtsOn();
            // This method is for android version. A robot can read word by word.
            /*PlayNextWord();*/

            // This method is for Standalone version
            PlayTextStandalone();
        }

        private void PlayTextStandalone()
        {
            if (_textToSpeech)
            {
                SoundController.GetController().ContinueExtraClip(_audioClip);
                CheckIfAudioIsEnded();
            }

            else if(_readTogether)
            {
                PlayNextWord();
            }

        }

        private void CheckIfAudioIsEnded()
        {

            bool isPlaying = SoundController.GetController().IsExtraClipPlaying();

            if (!isPlaying)
            {
                BookViewController.GetController().PlayPauseToggle.isOn = false;
                BookViewController.GetController().RestartPlayTextAction();
            }
            else
            {
                Coroutiner.GetCoroutiner().ExecuteIn(CheckIfAudioIsEnded, 0.5f);
            }
        }

        public override void Pause()
        {
            IsPlaying = false;

            // only for standalone 
            SoundController.GetController().PauseExtraClip();
        }

        private void PlayNextWord()
        {
            if (IsPlaying)
            {
                if (_currentWord == _words.Count)
                {
                    OnReachFinal();
                    return;
                }
                //Comment for standalone version
                /*if (_textToSpeech && _currentLetter == 0)
                {
                    Speak(_words[_currentWord]);
                }*/
                if (_readTogether) RemarkWord();
            }
            
        }

        private void OnReachFinal()
        {
            BookViewController.GetController().PauseText();
            Restart();
        }

        private void Speak(string s)
        {
            Speaker.GetSpeaker().Speak(s);
        }

        public void RemarkWord()
        {
            BookViewController.GetController().PaintWordFromIndex(_words, _currentWord, _currentLetter);
            _currentLetter++;
            if (_currentLetter == _words[_currentWord].Length)
            {
                Coroutiner.GetCoroutiner().ExecuteIn(ChangeWord, SecondsPerLetter);
            }
            else Coroutiner.GetCoroutiner().ExecuteIn(RemarkWord, SecondsPerLetter);
            /*
                        if (_currentLetter == 0) SpeakAction.GetSpeaker().Speak(Words[_currentWord]);
            */
        }

        private void ChangeWord()
        {
            UnpaintCurrentWord();
            _currentWord++;
            _currentLetter = 0;
            Coroutiner.GetCoroutiner().ExecuteIn(Execute, SecondsPerLetter*1.25f);
        }

        public void UnpaintCurrentWord()
        {
            if(_words != null && _currentWord >= 0) BookViewController.GetController().UnpaintWordFromIndex(_words, _currentWord);
        }

        public void NextWord()
        {
            if (!_readTogether) _currentWord++;
            if (!_readTogether && IsPlaying)
            {
                Execute();
            }
        }

        public void LoadClip(string audioPath)
        {
            BookViewController.GetController().TextToSpeechToggle.gameObject.SetActive(false);
            if (!string.IsNullOrEmpty(audioPath))
            {
                string[] strings = audioPath.Split('/');
                string assetName = strings[strings.Length - 1];
                Debug.Log("audio clip asset name: " + assetName);
                if (string.IsNullOrEmpty(assetName))
                {
                    _audioClip = null;
                    return;
                    
                }
                LoadAssets.GetLoadAssets()
                    .LoadAnAsset<AudioClip>(BookViewController.GetController().GetCurrentAssetBundle(),
                        assetName,
                        o =>
                        {
                            _audioClip = o.GetAsset<AudioClip>();
                            if (_audioClip != null)
                            {
                                BookViewController.GetController().TextToSpeechToggle.gameObject.SetActive(true);
                                BookViewController.GetController().PlayPauseToggle.transform.parent.gameObject.SetActive(BookViewController.GetController().ReadTogetherToggle.isOn || BookViewController.GetController().TextToSpeechToggle.isOn);
                                BookViewController.GetController().CheckExecutePlayTextAction();
                            }
                            
                        }, null);

            }
            else
            {
                _audioClip = null;
                BookViewController.GetController().CheckExecutePlayTextAction();

            }



            //_audioClip = !string.IsNullOrEmpty(audioPath) ? Resources.Load<AudioClip>(audioPath) : null;
        }

        public bool HasAudioClip()
        {
            return _audioClip != null;
        }
    }
}