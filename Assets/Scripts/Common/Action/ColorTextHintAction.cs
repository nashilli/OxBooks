﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Assets.Scripts.App;
using Assets.Scripts.FormatableText;
using Assets.Scripts._Book;
using Assets.Scripts._Book.Page;
using Assets.Scripts._Book.PageView;
using UnityEngine;

namespace Assets.Scripts.Common.Action
{
    public class ColorTextHintAction : Action
    
    {
        private readonly Color _hintColor = new Color32(229, 61, 28, 255);
        public override void Execute()
        {

            TextPage currentPage = (TextPage) BookViewController.GetController().GetCurrentPage();
            TextPageView pageView = ((TextPageView)BookViewController.GetController().GetCurrentPageView());
            FormatableWord[] words = pageView.Formatable.GetComponentsInChildren<FormatableWord>();

            string text = currentPage.Text;        

            for (int i = 0; i < currentPage.HintTexts.Length; i++)
            {
                string hintText = currentPage.HintTexts[i];
                int hintIndex = currentPage.Text.IndexOf(hintText, StringComparison.Ordinal);
                int hintLength = hintText.Split(' ').Length;
                int prevWordsQuantity = text.Substring(0, hintIndex).Split(' ').Length - 1;
                PaintTextFrom(prevWordsQuantity, hintLength, _hintColor, words);
            }

        }

        private void PaintTextFrom(int prevWordsQuantity, int hintLength, Color color, FormatableWord[] words)
        {
            for (int i = hintLength - 1; i >= 0; i--)
            {
                words[prevWordsQuantity + i].SetFontColor(color);
            }
        }
    }
}