﻿using System;
using Assets.Scripts.App;
using Assets.Scripts.Sound;
using Assets.Scripts._Book;
using Assets.Scripts._Book.Page;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.Common.Action
{
    public class ReadBookAction : Action
    {
        public ReadBookAction(Book book)
        {
            Book = book;
        }

        public static Book Book { get; private set; }

        public override void Execute()
        {
            ViewController.GetController().ShowLoading();
            SoundController.GetController().StopMusic();
            ViewController.GetController().LoadBookView(OnBookViewControllerLoaded);

        }

        public void OnBookViewControllerLoaded()
        {
            
            User currentUser = AppController.GetController().GetCurrentUser();

            StartedBook startedBook = currentUser.GetStartedBookNotFinished(Book.Id);
            if (startedBook != null && startedBook.State != StartedBookState.Finished)
            {
                ContinueBook(startedBook);
            }
            else
            {
                currentUser.AddNewStartedBook(Book.Id, Book.Pages.Count);
                InitNewBook();
            }

            ViewController.GetController().HideLoading();
        }

        private void ContinueBook(StartedBook startedBook)
        {
            AppController.GetController().GetCurrentUser().CurrentBook = startedBook;
            BookViewController.GetController().InitBook(Book, startedBook.GetCurrentPage().PageId);
        }

        private void InitNewBook()
        {
            BookViewController.GetController().CurrentBook = Book;

            Page currentPage = Book.GetCover();
            StartedPage startedPage = new StartedPage(currentPage.Id, currentPage.HasText() ? ((TextPage)currentPage).Text.Split(currentPage is CoverPage ? '_' : ' ').Length : 0,
                (currentPage is MultipleChoicePage ? (currentPage as MultipleChoicePage).Options.Length : 0), currentPage is MultipleChoicePage ? (currentPage as MultipleChoicePage).Options[0].GetOptionType() : OptionType.Text);

            AppController.GetController().GetCurrentUser().CurrentBook.CurrentTreeNode = new TreeNode<StartedPage>(startedPage);


            BookViewController.GetController().ShowPage(startedPage.PageId, startedPage);
        }
    }
}