﻿using System;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Common.Action
{
    public class DownloadAssetBundle : MonoBehaviour
    {
        public DownloadAssetBundle(string assetBundle)
        {
            AssetBundle = assetBundle;
        }

        public string AssetBundle { get; set; }

       
    }
}