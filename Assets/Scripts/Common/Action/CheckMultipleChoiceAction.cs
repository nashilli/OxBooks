﻿using System.Diagnostics;
using Assets.Scripts._Book;
using Assets.Scripts._Book.Page;
using Assets.Scripts._Book.PageView;
using Debug = UnityEngine.Debug;

namespace Assets.Scripts.Common.Action
{
    public class CheckMultipleChoiceAction : Action
    {
        public override void Execute()
        {
            MultplieChoiceView multplieChoiceView = (MultplieChoiceView) BookViewController.GetController().GetCurrentPageView();

            MultipleChoicePage multipleChoicePage = (MultipleChoicePage) BookViewController.GetController().GetCurrentPage();
           // todo --> hay que ver que pasa si hay imageens o sonido en lugar de texto
            string[] selectedAnswers = multplieChoiceView.GetSelectedAnswers();
            bool[] answers = CheckAnswer(selectedAnswers, multipleChoicePage.Options);

            multplieChoiceView.PaintAnswers(answers, selectedAnswers);
            bool allTrue = true;
            foreach (bool b in answers)
            {
                allTrue = allTrue && b;
            }



            if (allTrue && selectedAnswers.Length == multipleChoicePage.GetCorrectAnswers())
            {

                OnSuccess();
                ((MultplieChoiceView) BookViewController.GetController().GetCurrentPageView()).DisableAllOptions();

            }
            else
            {
                OnFailExercise();
            }



        }

        private bool[] CheckAnswer(string[] selectedAnswers, Option[] options)
        {
            bool[] correctedAnswers = new bool[selectedAnswers.Length];
            for (int i = selectedAnswers.Length - 1; i >= 0; i--)
            {
                for (int j = options.Length - 1; j >= 0; j--)
                {
                    if (!options[j].Text.Equals(selectedAnswers[i])) continue;
                    correctedAnswers[i] = options[j].IsTrue;
                    break;
                }

                
            }
           
            return correctedAnswers;
        }


    }
}