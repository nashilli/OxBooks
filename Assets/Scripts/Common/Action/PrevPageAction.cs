﻿
using Assets.Scripts.App;
using Assets.Scripts.Sound;
using Assets.Scripts._Book;

namespace Assets.Scripts.Common.Action
{
    public class PrevPageAction : Action
    {
        public override void Execute()
        {
            Coroutiner.GetCoroutiner().Stop();
            SoundController.GetController().StopExtraClip();

            BookViewController.GetController().SaveStateOfCurrentPage();
            BookViewController.GetController().GetCurrentPageView().gameObject.SetActive(false);
            ViewController.GetController().HideDescriptionPopUp();
            /*
                        AppController.GetController().GetCurrentUser().CurrentBook.CurrentPage--;
            */
            //BookViewController.GetController().CurrentPageId = AppController.GetController().GetCurrentUser().CurrentBook.GetCurrentPage().PageId;
            //int currentPage = BookViewController.GetController().CurrentPageId;

            StartedBook currentBook = AppController.GetController().GetCurrentUser().CurrentBook;
            currentBook.CurrentTreeNode = currentBook.CurrentTreeNode.Parent;
            BookViewController.GetController().ShowPage(currentBook.CurrentTreeNode.Value.PageId, currentBook.GetCurrentPage());
        }
    }
}