﻿using UnityEngine;

namespace Assets.Scripts.Common.Action
{
    public class EnableGameObjectAction : ToggleAction
    {
        private readonly GameObject _gameObject;

        public EnableGameObjectAction(GameObject gameObject)
        {
            _gameObject = gameObject;
        }

        public override void Execute()
        {
            _gameObject.SetActive(true);
        }

        public override void Pause()
        {
            _gameObject.SetActive(false);
        }
    }
}