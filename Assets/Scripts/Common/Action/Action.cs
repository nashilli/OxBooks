﻿using Assets.Scripts.App;
using Assets.Scripts.Sound;
using Assets.Scripts._Book;

namespace Assets.Scripts.Common.Action
{
    public abstract class Action
    {
        

        public abstract void Execute();

        public void OnSuccess()
        {
            BookViewController.GetController().TicButton.interactable = false;
            BookViewController.GetController().HintButton.interactable = false;
            BookViewController.GetController().NexPageActionButton.interactable = true;
            //BookViewController.GetController().CreateStartedPage(AppController.GetController().GetCurrentUser(), BookViewController.GetController().GetCurrentPage());
            /*        AppController.GetController().GetCurrentUser().CurrentBook.
                    StartedPages.Find(e => e.PageId == BookViewController.GetController().CurrentPageId).IsResolved = true;*/
            AppController.GetController().GetCurrentUser().CurrentBook.CurrentTreeNode.Value.IsResolved = true;

            AppController.GetController().GetCurrentUser().CurrentBook.Metrics.AddCorrectAnswer();
            SoundController.GetController().PlayRightAnswerSound();

        }

        public void OnFailExercise()
        {
            SoundController.GetController().PlayFailureSound();
            AppController.GetController().GetCurrentUser().CurrentBook.Metrics.AddWrongAnswer(BookViewController.GetController().GetCurrentPage().GetPageType());
        }
    }

    
}