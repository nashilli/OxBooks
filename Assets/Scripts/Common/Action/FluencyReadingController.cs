﻿using Assets.Scripts.App;
using Assets.Scripts.FormatableText;
using Assets.Scripts._Book;
using Assets.Scripts._Book.Page;
using Assets.Scripts._Book.PageView;
using UnityEngine;

namespace Assets.Scripts.Common.Action
{
    public class FluencyReadingController
    {

        private static int _indexWord;
        private static string[] _text;
        private static FormatableWord[] _formatableWords;


        public static void Execute(string text)
        {
            _text = text.Split(' ');
            _indexWord = 0;
            _formatableWords = (BookViewController.GetController().GetCurrentPageView() as TextPageView).Formatable.GetComponentsInChildren<FormatableWord>();
            DisableAllAnswerButtons();
            MakeInvisibleAllWords();
            PaintCurrentWord();
            Coroutiner.GetCoroutiner().ExecuteIn(ShowCurrentWordWord, 1f);
        }

        internal static void DisableAllAnswerButtons()
        {
            ((MultplieChoiceView) BookViewController.GetController().GetCurrentPageView()).DisableAllOptions();
        }

        private static void EnableAllAnswerButtons()
        {
            ((MultplieChoiceView)BookViewController.GetController().GetCurrentPageView()).EnableAllOptions();
        }

        private static void MakeInvisibleAllWords()
        {
            foreach (FormatableWord formatableWord in _formatableWords)
            {
                formatableWord.MakeInvisible();
            }
        }
   
        private static void ShowCurrentWordWord()
        {       
            _formatableWords[_indexWord].MakeInvisible();
            if (_indexWord == _text.Length - 1)
            {
                EnableAllAnswerButtons();
                return;
            }
            _indexWord++;
            PaintCurrentWord();      
            Coroutiner.GetCoroutiner().ExecuteIn(ShowCurrentWordWord, 1f);
        }

        private static void PaintCurrentWord()
        {
            _formatableWords[_indexWord].SetFontColor(Color.black);
        }


        public static void ReExecute()
        {
            _indexWord = 0;
            Coroutiner.GetCoroutiner().Stop();
            MakeInvisibleAllWords();
            PaintCurrentWord();
            Coroutiner.GetCoroutiner().ExecuteIn(ShowCurrentWordWord, 1f);

        }
    }

    public class FluencyReadingAction : Action {
        public override void Execute()
        {
            FluencyReadingController.ReExecute();
            

        }
    }
}