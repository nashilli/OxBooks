﻿namespace Assets.Scripts.Common.Action
{
    public abstract class ToggleAction : Action
    {
        public abstract void Pause();
    }
}