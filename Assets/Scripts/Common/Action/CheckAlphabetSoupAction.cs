﻿using System;
using System.Diagnostics;
using Assets.Scripts.App;
using Assets.Scripts._Book;
using Assets.Scripts._Book.Page;
using Assets.Scripts._Book.PageView;

namespace Assets.Scripts.Common.Action
{
    public class CheckAlphabetSoupAction : Action
    {
        public override void Execute()
        {

            LetterSoupView letterSoupView = (LetterSoupView) BookViewController.GetController().GetCurrentPageView();

            bool[][] correctMatrix = letterSoupView.LetterSoup.GetResolutionMatrix();
          

            bool[][] viewBoolMatrix = letterSoupView.ObtainBoolMatrix();

            if (correctMatrix.Length != viewBoolMatrix.Length)
            {
                UnityEngine.Debug.Log("NOT SAME SIZE");
                OnFailExercise();
                return;
            }

            for (int i = correctMatrix.Length - 1; i >= 0; i--)
            {
                if (correctMatrix[i].Length != viewBoolMatrix[i].Length)
                {
                    UnityEngine.Debug.Log("NOT SAME SUB SIZE");
                    OnFailExercise();
                    return;
                }
                for (int j = correctMatrix[i].Length - 1; j >= 0; j--)
                {
                    if (correctMatrix[i][j] != viewBoolMatrix[i][j])
                    {
                        UnityEngine.Debug.Log("NOT EQUALS");
                        OnFailExercise();
                        return;
                    }
                }
            }
            OnSuccess();
            (BookViewController.GetController().GetCurrentPageView() as LetterSoupView).DisableAllTiles();


        }

        
    }
}