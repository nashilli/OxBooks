﻿using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.App;
using Assets.Scripts.Settings;
using Assets.Scripts.Sound;
using System;
using Assets.Scripts._Book;
using UnityEngine.Events;

public class InGameMenuView : MonoBehaviour
{

    public Button BackToBookButton;
    public Button InstructionsButton;
    public Button RestartBookButton;
    public Button GoToMyProfileButton;
    public Button GoToExploreButton;


    public Text BackToBookText, InstructionsText, RestarText, GoToMyProfileText, GoToExploreText;

    void OnEnable()
    {
        SetTexts();
    }

    private void SetTexts()
    {
        BackToBookText.text = "Volver al libro";
        InstructionsText.text = "Instrucciones";
        RestarText.text = "Empezar de nuevo";
        GoToMyProfileText.text = "Ir a mis cosas";
        GoToExploreText.text = "Leer otro cuento";
    }

    void Start()
    {

        GoToMyProfileButton.onClick.AddListener(OnClickGoToMyThings);
        GoToExploreButton.onClick.AddListener(OnClickGoToExplore);
        RestartBookButton.onClick.AddListener(OnClickRestartGame);
        BackToBookButton.onClick.AddListener(OnClicBackToGame);
        InstructionsButton.onClick.AddListener(OnClickInstructions);
    }

    private void OnClickInstructions()
    {
        PlayClickSound();
        ViewController.GetController().ShowInstructions();
        ViewController.GetController().HideInGameMenu();
    }


    private void OnClickGoToMyThings()
    {
        PlayClickSound();
        BookViewController.GetController().SaveStateOfCurrentPage();
        AppController.GetController().SaveAdvance();
        AppController.GetController().GetCurrentUser().CurrentBook = null;
        ViewController.GetController().LoadMyThings();
        ViewController.GetController().LoadTopbar();
        ViewController.GetController().HideInGameMenu();
        SoundController.GetController().PlayMusic();

    }

    private void OnClickGoToExplore()
    {
        PlayClickSound();
        BookViewController.GetController().SaveStateOfCurrentPage();
        AppController.GetController().SaveAdvance();
        AppController.GetController().GetCurrentUser().CurrentBook = null;
        ViewController.GetController().LoadExplore();
        ViewController.GetController().LoadTopbar();
        ViewController.GetController().HideInGameMenu();
        SoundController.GetController().PlayMusic();

    }



    public void OnInstructionsClic(){
        PlayClickSound();
        ViewController.GetController().HideInGameMenu();
        ViewController.GetController().ShowInstructions();    
    }

    private void OnClickRestartGame()
    {
        PlayClickSound();
        Coroutiner.GetCoroutiner().Stop();
        SoundController.GetController().StopExtraClip();
        BookViewController.GetController().RestartBook();
        ViewController.GetController().HideInGameMenu();

    }

    private void OnClicBackToGame()
    {
        PlayClickSound();
        ViewController.GetController().HideInGameMenu();

    }

    private void PlayClickSound()
    {
        SoundController.GetController().PlayClickSound();
    }
}
