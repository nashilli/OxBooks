﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.App;
using Assets.Scripts.Views;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

namespace Assets.Scripts.Explore
{

    public class ExploreView : MonoBehaviour
    {

        public GameObject FilterContainer;
        public GameObject ResultsPanel;
        public PreviewBook PreviewBook;
        public InputField Searcher;
        public AccordionElement LevelAccordionElement;
        public AccordionElement GenreAccordionElement;
        public AccordionElement LangagugeAccordionElement;
        public IllustratorPopUp IllustratorPopUp;
        public ProblemPopUp ProblemPopUp;

        void Start()
        {
            int levels = AppController.GetController().GetLevels();
            List<Toggle> levelToggles = GetTogglesChildren(LevelAccordionElement);
            levelToggles[0].GetComponentInChildren<Text>().text = "Ilustrados";
            
            for (int i = 1; i <= levels; i++)
            {
                levelToggles[i].GetComponentInChildren<Text>().text = "Nivel " + i;
            }


            List<Toggle> languageToggles = GetTogglesChildren(LangagugeAccordionElement);
            languageToggles[0].GetComponentInChildren<Text>().text = "Español";
            languageToggles[1].GetComponentInChildren<Text>().text = "English";

        }

        public void SetGenreOptions(string[] genreStrings)
        {
            List<Toggle> genreToggles = GetTogglesChildren(GenreAccordionElement, true);

            genreToggles[0].GetComponentInChildren<Text>().text = "Todos";

            for (int i = 1; i < genreToggles.Count; i++)
            {
                if (i > genreStrings.Length)
                {
                    genreToggles[i].gameObject.SetActive(false);
                    genreToggles[i].enabled = false;
                    genreToggles[i].isOn = false;
                    continue;
                }
                genreToggles[i].gameObject.SetActive(true);
                genreToggles[i].enabled = true;
                genreToggles[i].GetComponentInChildren<Text>().text = genreStrings[i - 1];
            }
    
        }

    /*    public int GetLevelToggleSelected()
        {
            Toggle[] toggles = FilterContainer.GetComponentsInChildren<Toggle>();
            for (int i = toggles.Length - 1; i >= 0; i--)
            {
                if (toggles[i].isOn) return i;
            }
            throw new Exception("any toggle selected");
        }*/

        public Language GetLanguageSelected()
        {
            List<Toggle> languageToggles = GetTogglesChildren(LangagugeAccordionElement);
            for (int i = languageToggles.Count - 1; i >= 0; i--)
            {
                if (languageToggles[i].isOn) return (Language) i;
            }
            return Language.Spanish;
        }

        public int GetLevelSelected()
        {
            List<Toggle> levelToggles = GetTogglesChildren(LevelAccordionElement);
            for (int i = levelToggles.Count - 1; i >= 0; i--)
            {
                if (levelToggles[i].isOn) return i;
            }
            return -1;
        }

        public string GetSelectedGenre()
        {
            return GetTogglesChildren(GenreAccordionElement)[GetSelectedGenreIndex()].GetComponentInChildren<Text>().text;
        }

        public List<Toggle> GetTogglesChildren(AccordionElement accordionElement, bool inactiveToggles = false)
        {
            List<Toggle> toggles = accordionElement.GetComponentsInChildren<Toggle>(true).ToList();
            return toggles.GetRange(1, toggles.Count - 1);
        }


        public int GetSelectedGenreIndex()
        {
            int selectedGenreIndex = GetTogglesChildren(GenreAccordionElement).FindIndex(e => e.isOn);
            return selectedGenreIndex >= 0 ? selectedGenreIndex : 0;
        }
    }


}