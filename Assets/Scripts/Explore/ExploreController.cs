﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using AssetBundles;
using Assets.Scripts.App;
using Assets.Scripts.Buttons;
using Assets.Scripts.Common;
using Assets.Scripts.Common.Action;
using Assets.Scripts.Sound;
using Assets.Scripts._Book;
using Assets.Scripts._Book.Page;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Assets.Scripts.Explore
{
    public class ExploreController : MonoBehaviour
    {
        private static ExploreController _exploreController;
        private ExploreView _exploreView;
        public GameObject BookButtonPrefab;

        void Awake()
        {
            if (_exploreController == null) _exploreController = this;
            else if (_exploreController != this) Destroy(gameObject);
        }

        void Start()
        {
            _exploreView = GetComponent<ExploreView>();
            _exploreView.PreviewBook.gameObject.SetActive(false);
            int lastSelectedLevel = AppController.GetController().LastSelectedLevel;
            Language lastLanguage = AppController.GetController().LastSelectedLanguage;
            _exploreView.GetTogglesChildren(_exploreView.LevelAccordionElement)[lastSelectedLevel].isOn = true;
            _exploreView.GetTogglesChildren(_exploreView.LangagugeAccordionElement)[(int)lastLanguage].isOn = true;
            _exploreView.GetTogglesChildren(_exploreView.GenreAccordionElement)[0].isOn = true;

            LoadBooksOfLevel(lastSelectedLevel);
            _exploreView.SetGenreOptions(AppController.GetController().GetDistinctGenresFrom(lastSelectedLevel, lastLanguage));
            ApplyFilters();
            AddListenersToFilters();
            AddListenerToSearcher();
        }

        private void ApplyFilters()
        {
            Language languageSelected = _exploreView.GetLanguageSelected();
            AppController.GetController().LastSelectedLanguage = languageSelected;
            string selectedGenre = _exploreView.GetSelectedGenre();

            foreach (BookButton bookButton in _exploreView.GetComponentsInChildren<BookButton>(true))
            {
                if(bookButton.IdBook == -1) continue;
                Book book = AppController.GetController().GetBook(bookButton.IdBook);

                bool visible = VisibilityBySearcher(bookButton) && VisibiltyByGenre(selectedGenre, bookButton) && book.Language == languageSelected;
                bookButton.gameObject.SetActive(visible);
            }
        }

        private void AddListenerToSearcher()
        {
            _exploreView.Searcher.onValueChanged.AddListener(delegate
            {
                SoundController.GetController().PlayKeyPressedSound();
                ApplyFilters();
            });
        }

        private bool VisibilityBySearcher(BookButton button)
        {
            return CultureInfo.InvariantCulture.CompareInfo.IndexOf(
                button.BookTitleText.text, _exploreView.Searcher.text,
                CompareOptions.IgnoreCase) >= 0;
        }

        private void AddListenersToFilters()
        {
            
            _exploreView.LangagugeAccordionElement.onValueChanged.AddListener(delegate
            {
                SoundController.GetController().PlayClickSound();
            });
            _exploreView.LevelAccordionElement.onValueChanged.AddListener(delegate
            {
                SoundController.GetController().PlayClickSound();
            });
            _exploreView.GenreAccordionElement.onValueChanged.AddListener(delegate
            {
                SoundController.GetController().PlayClickSound();
            });
            IEnumerable<Toggle> toggles = _exploreView.GetTogglesChildren(_exploreView.LevelAccordionElement);
            foreach (Toggle toggle in toggles)
            {
                toggle.onValueChanged.AddListener(delegate
                {
                    int selectedLevel = _exploreView.GetLevelSelected();
                    AppController.GetController().LastSelectedLevel = selectedLevel;
                    SoundController.GetController().PlayClickSound();
                    LoadBooksOfLevel(selectedLevel);
                    _exploreView.PreviewBook.gameObject.SetActive(false);
                    _exploreView.Searcher.text = "";
                    _exploreView.SetGenreOptions(AppController.GetController().GetDistinctGenresFrom(selectedLevel, Language.Spanish));
                    _exploreView.GetTogglesChildren(_exploreView.GenreAccordionElement)[0].isOn = true;
                    ApplyFilters();
                });
            }

            foreach (Toggle toggle in _exploreView.GetTogglesChildren(_exploreView.GenreAccordionElement))
            
            {
                toggle.onValueChanged.AddListener(delegate
                {
                    SoundController.GetController().PlayClickSound();
                    ApplyFilters();

                });
            }

            foreach (Toggle langugeToggle in _exploreView.GetTogglesChildren(_exploreView.LangagugeAccordionElement))
            {
                langugeToggle.onValueChanged.AddListener(delegate
                {
                    SoundController.GetController().PlayClickSound();
                    _exploreView.GetTogglesChildren(_exploreView.GenreAccordionElement)[0].isOn = true;
                    ApplyFilters();
                });
            }

        }
/*
        private void ApplyLanguageFilter()
        {
            Language language = _exploreView.GetLanguageSelected();
            _exploreView.PreviewBook.gameObject.SetActive(false);
            foreach (BookButton button in _exploreView.ResultsPanel.GetComponentsInChildren<BookButton>(true))
            {
                if (button.IdBook == -1) continue;
                _exploreView.SetGenreOptions(AppController.GetController()
                    .GetDistinctGenresFrom(_exploreView.GetLevelSelected(), language));
                _exploreView.Searcher.text = "";
                button.gameObject.SetActive(AppController.GetController().GetBook(button.IdBook).Language == language);
            }
            Debug.Log("Language filter applied");

        }*/

        private static bool VisibiltyByGenre(string selectedGenre, BookButton button)
        {
            return selectedGenre.ToLower() == "todos" || AppController.GetController().GetBook(button.IdBook).Genres.Contains(selectedGenre);
        }


        private void LoadBooksOfLevel(int level)
        {
            Book[] books = AppController.GetController().GetBooks(level);
            BookButton[] bookButtons = _exploreView.ResultsPanel.GetComponentsInChildren<BookButton>(true);
            for (int i = books.Length - 1; i >= 0; i--)
            {
                bookButtons[i].IdBook = books[i].Id;
                string imagePath = ((CoverPage) books[i].Pages[0]).ImagePath;
                bookButtons[i].BookImage.sprite = Resources.Load<Sprite>(imagePath.Substring(0, imagePath.Length - 2) + "Icon");
                bookButtons[i].BookTitleText.text = books[i].Title;
                bookButtons[i].gameObject.SetActive(true);
                bookButtons[i].SetLevel(level);
                var index = i;
                bookButtons[i].ShowPreviewBookButton.onClick.RemoveAllListeners();
                bookButtons[i].ShowPreviewBookButton.onClick.AddListener(delegate
                {
                    ShowPreviewBook(books[index]);
                });
               
            }
            for (int j = bookButtons.Length - 1; j >= books.Length; j--)
            {
                bookButtons[j].gameObject.SetActive(false);
                bookButtons[j].IdBook = -1;
            }
        }

        private void ShowPreviewBook(Book book)
        {
            SoundController.GetController().PlayClickSound();
            _exploreView.PreviewBook.SetData(book, null, false);
                                     
            _exploreView.PreviewBook.gameObject.SetActive(true);
        }


        public static ExploreController GetController()
        {
            return _exploreController;
        }

        public void ShowIllustratorPopUp()
        {
            _exploreView.IllustratorPopUp.gameObject.SetActive(true);
            ViewController.GetController().GetTopbar().SetActive(false);
        }

        public void ShowProblemPopUp()
        {
            _exploreView.ProblemPopUp.gameObject.SetActive(true);
            ViewController.GetController().GetTopbar().SetActive(false);

        }
    }
}