﻿using System;
using System.IO;
using System.Net;
using AssetBundles;
using Assets.Scripts.App;
using Assets.Scripts.Common;
using Assets.Scripts.Common.Action;
using Assets.Scripts.Metrics;
using Assets.Scripts.Sound;
using Assets.Scripts._Book;
using Assets.Scripts._Book.Page;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Explore
{
    public class PreviewBook : MonoBehaviour
    {

        public Text TitleText;
        public Text AuthorText;
        public Text IllustratorText;
        public Text EstimatedReadTimeText;
        public Text GenresText;
        public Text SynopsisText;
        public Text PagesText;
        public Text PercentageOfDownloadText;

        public GameObject MetricsPanel;
        public Image[] AverageStars;
        public Text AverageScore;

        public ActionButton PlayBookActionButton;
        public ActionButton ViewBookActionButton;

        public Button DownloadBookActionButton;
        public Button IllustratorButton;
        public Button RemoveBookButton;

        public Button CloseButton;
        public Button PrintButton;


        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape)) ClosePreview();
        }

        void Start()
        {
            CloseButton.onClick.AddListener(ClosePreview);
            PlayBookActionButton.onClick.AddListener(SoundController.GetController().PlayClickSound);
            ViewBookActionButton.onClick.AddListener(SoundController.GetController().PlayClickSound);
            PrintButton.onClick.AddListener(SoundController.GetController().PlayClickSound);
            IllustratorButton.onClick.AddListener(SoundController.GetController().PlayClickSound);
            IllustratorButton.onClick.AddListener(() => ExploreController.GetController().ShowIllustratorPopUp());
        }



        public void SetData(Book book, BookMetrics averageMetrics, bool inProgress)
        {
            SetBookInfo(book);
            SetMetricsPanelVisualization(averageMetrics);
            SetListenersAndActions(book, inProgress);
            SetButtonsToDownaloadOrRead(book);
            SetToContinueOrToStart(book);
        }

        private void SetToContinueOrToStart(Book book)
        {
            StartedBook startedBook = AppController.GetController().GetCurrentUser().GetStartedBookNotFinished(book.Id);
            PlayBookActionButton.GetComponentInChildren<Text>().text = startedBook != null && startedBook.State == StartedBookState.InProgress
                ? "CONTINUAR LECTURA"
                : "COMENZAR LECTURA";
        }

        private void SetButtonsToDownaloadOrRead(Book book)
        {
            Hash128 assetBundleHash = AssetBundleManager.m_AssetBundleManifest.GetAssetBundleHash(book.AssetBundle);
            if (Caching.IsVersionCached(book.AssetBundle, assetBundleHash))
            {
                DownloadBookActionButton.gameObject.SetActive(false);
                PlayBookActionButton.gameObject.SetActive(true);
                ViewBookActionButton.gameObject.SetActive(true);
                RemoveBookButton.gameObject.SetActive(true);

            }
            else
            {
                RemoveBookButton.gameObject.SetActive(false);
                DownloadBookActionButton.gameObject.SetActive(true);
                PercentageOfDownloadText.text = "";

                PlayBookActionButton.gameObject.SetActive(false);
                ViewBookActionButton.gameObject.SetActive(false);

            }
        }

        private void SetListenersAndActions(Book book, bool inProgress)
        {
            PlayBookActionButton.MyAction = new ReadBookAction(book);
            PlayBookActionButton.onClick.AddListener(SoundController.GetController().PlayClickSound);
            PlayBookActionButton.GetComponentInChildren<Text>().text = inProgress ? "CONTINUAR LECTURA" : "LEER OTRA VEZ";

            ViewBookActionButton.MyAction = new ViewBookAction(book);
            ViewBookActionButton.onClick.AddListener(SoundController.GetController().PlayClickSound);


            PrintButton.onClick.RemoveAllListeners();         
            PrintButton.onClick.AddListener(delegate
            {
                SoundController.GetController().PlayClickSound();
                PrintBook(book);
            });



            DownloadBookActionButton.onClick.RemoveAllListeners();


            RemoveBookButton.onClick.RemoveAllListeners();


            DownloadBookActionButton.onClick.AddListener(delegate { DownloadBook(book); });

            RemoveBookButton.onClick.AddListener(delegate
            {
                RemoveBookButton.enabled = false;
                AssetBundleManager.RemoveAsset(book.AssetBundle.ToLower());
                OnRemoved();
            });

            
        }

        private void DownloadBook(Book book)
        {
            if (!HasInternetConnectionAndManifestExists())
            {
                ShowPopUpProblemExists();
            }
            else
            {
                DownloadBookActionButton.enabled = false;
                string[] strings = ((CoverPage)book.Pages[0]).ImagePath.Split('/');
                string imagePath = strings[strings.Length - 1];
                ViewController.GetController().BlockScreen();
                LoadAssets.GetLoadAssets()
                    .LoadAnAsset<UnityEngine.Object>(book.AssetBundle.ToLower(), imagePath,
                        o =>
                        {
                            OnDownloaded();
                            ViewController.GetController().UnBlockScreen();

                        },
                        f => { PercentageOfDownloadText.text = (int)(f * 100) + "%"; }
                    );
            }
        }

        private void ShowPopUpProblemExists()
        {
            ExploreController.GetController().ShowProblemPopUp();
        }

        private bool HasInternetConnectionAndManifestExists()
        {
            return CheckInternetAvailability() && AssetBundleManager.m_AssetBundleManifest != null;
            // todo --> case when the device hasnt got internet
            /*if (AssetBundleManager.m_AssetBundleManifest == null)
            {
                DownloadBookActionButton.interactable = false;

                if (Caching.IsVersionCached(books[i1].AssetBundle, AssetBundleManager.m_AssetBundleManifest.GetAssetBundleHash(books[i1].AssetBundle)))
                {
                    DownloadBookActionButton.gameObject.SetActive(false);
                    PlayBookActionButton.gameObject.SetActive(true);
                    ViewBookActionButton.gameObject.SetActive(true);
                    RemoveBookButton.gameObject.SetActive(true);

                }
                else
                {
                    RemoveBookButton.gameObject.SetActive(false);
                    DownloadBookActionButton.gameObject.SetActive(true);
                    PercentageOfDownloadText.text = "";

                    PlayBookActionButton.gameObject.SetActive(false);
                    ViewBookActionButton.gameObject.SetActive(false);

                }
            }*/
        }

        private bool CheckInternetAvailability()
        {
            string htmlText = GetHtmlFromUri("http://google.com");
            if (htmlText == "")
            {
                //No connection
                return false;
            }
            else if (!htmlText.Contains("schema.org/WebPage"))
            {
                //Redirecting since the beginning of googles html contains that 
                //phrase and it was not found
                return false;
            }
            else
            {
                //success
                return true;
            }

        }

        private void SetBookInfo(Book book)
        {
            GenresText.text = string.Join(" - ", book.Genres);
            TitleText.text = book.Title;
            AuthorText.text = book.Author;
            IllustratorText.text = book.Illustrator.FirstName + " " + book.Illustrator.LastName;
            EstimatedReadTimeText.text = book.EstimatedReadTimeInMinutes + " min.";
            PagesText.text = book.Pages.Count.ToString();
            SynopsisText.text = book.Synopsis;
        }

        private void SetMetricsPanelVisualization(BookMetrics averageMetrics)
        {
            if (averageMetrics != null)
            {
                MetricsPanel.SetActive(true);
                for (int i = AverageStars.Length - 1; i >= 0; i--)
                {
                    AverageStars[i].enabled = i < averageMetrics.Stars;
                }

                AverageScore.text = "" + averageMetrics.Score;
            }
            else
            {
                MetricsPanel.SetActive(false);
            }
        }


        private void ClosePreview()
        {
            SoundController.GetController().PlayClickSound();
            gameObject.SetActive(false);
        }

        public void OnDownloaded()
        {
            DownloadBookActionButton.gameObject.SetActive(false);
            PlayBookActionButton.gameObject.SetActive(true);
            ViewBookActionButton.gameObject.SetActive(true);
            RemoveBookButton.gameObject.SetActive(true);
            DownloadBookActionButton.enabled = true;

        }

        private void OnRemoved()
        {
            DownloadBookActionButton.gameObject.SetActive(true);
            PlayBookActionButton.gameObject.SetActive(false);
            ViewBookActionButton.gameObject.SetActive(false);
            RemoveBookButton.gameObject.SetActive(false);
            RemoveBookButton.enabled = true;
            PercentageOfDownloadText.text = "";

        }

        private static void PrintBook(Book book)
        {
            string pathFilename = (Application.platform == RuntimePlatform.Android
                                      ? Application.persistentDataPath
                                      : Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "/OXBOOKS") + "/" + book.Title +
                                  "/ ";
            Directory.CreateDirectory(pathFilename);
            Printer.ConvertBookToDoc(book, pathFilename);
            ViewController.GetController()
                .ShowInfoPopUp("La versión imprimible del cuento se ha guardado con éxito en " + pathFilename);
        }

        public string GetHtmlFromUri(string resource)
        {
            string html = string.Empty;
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(resource);
            try
            {
                using (HttpWebResponse resp = (HttpWebResponse)req.GetResponse())
                {
                    bool isSuccess = (int)resp.StatusCode < 299 && (int)resp.StatusCode >= 200;
                    if (isSuccess)
                    {
                        using (StreamReader reader = new StreamReader(resp.GetResponseStream()))
                        {
                            //We are limiting the array to 80 so we don't have
                            //to parse the entire html document feel free to 
                            //adjust (probably stay under 300)
                            char[] cs = new char[80];
                            reader.Read(cs, 0, cs.Length);
                            foreach (char ch in cs)
                            {
                                html += ch;
                            }
                        }
                    }
                }
            }
            catch
            {
                return "";
            }
            return html;
        }
    }
}