﻿using Assets.Scripts.App;
using Assets.Scripts.Sound;
using Assets.Scripts._Book;
using UnityEngine;

namespace Assets.Scripts.Explore
{
    public class ExploreAction : MonoBehaviour {

        public void GoToExplore()
        {
            SoundController.GetController().PlayClickSound();
            BookViewController.GetController().SaveStateOfCurrentPage();
            AppController.GetController().SaveAdvance();
            AppController.GetController().GetCurrentUser().CurrentBook = null;
            ViewController.GetController().LoadExplore();
            ViewController.GetController().LoadTopbar();
            SoundController.GetController().PlayMusic();
        }
    }
}
