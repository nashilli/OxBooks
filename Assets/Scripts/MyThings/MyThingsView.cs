﻿using System;
using Assets.Scripts.App;
using Assets.Scripts.Buttons;
using Assets.Scripts.Explore;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.MyThings
{
    public class MyThingsView : MonoBehaviour
    {


        public Toggle StartedBooksToggle;
        public Toggle FinishedBooksToggle;
        public PreviewBook PreviewBook;
        public InputField Searcher;

        public GridLayoutGroup BooksGridLayoutGroup;

        void Start()
        {
            PreviewBook.gameObject.SetActive(false);
            StartedBooksToggle.isOn = true;

        }

        public void AddResult(GameObject aGameObject)
        {
            ViewController.GetController().FitObjectTo(aGameObject, BooksGridLayoutGroup.gameObject);
        }
    }
}
