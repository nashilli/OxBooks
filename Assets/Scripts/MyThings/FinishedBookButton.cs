﻿using System;
using System.Linq;
using Assets.Scripts.App;
using Assets.Scripts.Metrics;
using Assets.Scripts.Sound;
using Assets.Scripts._Book;
using Assets.Scripts._Book.Page;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.MyThings
{
    public class FinishedBookButton : MonoBehaviour
    {
        public Button Button;
        public Image BookImage;
        public Text TitleText;
        public Image[] Stars;

        private BookMetrics _firstMetrics;
        private BookMetrics _averageMetrics;


        public void SetBook(StartedBook[] startedBooks)
        {

            Book book = AppController.GetController().GetBook(startedBooks[0].IdBook);
            _firstMetrics = startedBooks[0].Metrics;
            _averageMetrics = new BookMetrics();
            TitleText.text = book.Title;

            foreach (StartedBook startedBook in startedBooks)
            {
                if (startedBook.State == StartedBookState.Finished)
                {
                    _averageMetrics.Add(startedBook.Metrics);
                }
            }

            _averageMetrics.CalculateAverage(startedBooks.Count(e => e.State == StartedBookState.Finished));


            string imagePath = ((CoverPage)book.Pages[0]).ImagePath;
            BookImage.sprite = Resources.Load<Sprite>(imagePath.Substring(0, imagePath.Length - 2) + "Icon");

            for (int i = Stars.Length - 1; i >= 0; i--)
            {
                Stars[i].enabled = i < _averageMetrics.Stars ;
            }

            Button.onClick.AddListener(delegate
            {   
                SoundController.GetController().PlayClickSound();
                MyThingsController.GetController().ShowPreviewFinishedBook(_averageMetrics, book);
            });

        }
    }
}