﻿using System.Collections.Generic;
using System.Globalization;
using Assets.Scripts.App;
using Assets.Scripts.Buttons;
using Assets.Scripts.Metrics;
using Assets.Scripts.Sound;
using Assets.Scripts._Book;
using UnityEngine;

namespace Assets.Scripts.MyThings
{
    public class MyThingsController : MonoBehaviour
    {

        private static MyThingsController _myThingsController;

        public GameObject FinishedBookPrefab;
        public GameObject StartedBookPrefab;

        public MyThingsView MyThingsView;

        private List<long> _finishedBookList = new List<long>();



        void Awake()
        {
            if (_myThingsController == null) _myThingsController = this;
            else if (_myThingsController != this) Destroy(gameObject);
        }

        // Use this for initialization
        void Start ()
        {
            _finishedBookList = new List<long>();
            AddListeners();
        }

        private void AddListeners()
        {
            MyThingsView.StartedBooksToggle.onValueChanged.AddListener(delegate
            {
                MyThingsView.PreviewBook.gameObject.SetActive(false);
                if (MyThingsView.StartedBooksToggle.isOn)
                {
                    SoundController.GetController().PlayClickSound();
                    ShowStartedBooks();

                }
                    
                else HideStartedBooks();
            });

            MyThingsView.FinishedBooksToggle.onValueChanged.AddListener(delegate
            {
                MyThingsView.PreviewBook.gameObject.SetActive(false);
                if (MyThingsView.FinishedBooksToggle.isOn)
                {
                    SoundController.GetController().PlayClickSound();
                    ShowFinishedBooks();
                }
                else HideFinishedBooks();
            });

            MyThingsView.Searcher.onValueChanged.AddListener(delegate
            {
                SoundController.GetController().PlayKeyPressedSound();
                if (MyThingsView.StartedBooksToggle.isOn)
                {
                    foreach (StartedBookButton startedBookButton in MyThingsView.BooksGridLayoutGroup.GetComponentsInChildren<StartedBookButton>(true))
                    {
                        startedBookButton.gameObject.SetActive(CultureInfo.InvariantCulture.CompareInfo.IndexOf(
                                startedBookButton.TitleText.text, MyThingsView.Searcher.text,
                                CompareOptions.IgnoreCase) >= 0);

                    }
                }
                else if (MyThingsView.FinishedBooksToggle.isOn)
                {
                    foreach (FinishedBookButton finishedBookButton in MyThingsView.BooksGridLayoutGroup.GetComponentsInChildren<FinishedBookButton>(true))
                    {
                        finishedBookButton.gameObject.SetActive(CultureInfo.InvariantCulture.CompareInfo.IndexOf(
                                finishedBookButton.TitleText.text, MyThingsView.Searcher.text,
                                CompareOptions.IgnoreCase) >= 0);

                    }
                }
                
            });
        
        }

        private void HideFinishedBooks()
        {
            FinishedBookButton[] books = MyThingsView.BooksGridLayoutGroup.GetComponentsInChildren<FinishedBookButton>();
            foreach (FinishedBookButton book in books)
            {
                book.gameObject.SetActive(false);
            }
        }

        private void ShowFinishedBooks()
        {
            MyThingsView.BooksGridLayoutGroup.cellSize = new Vector2(320, 505);
            FinishedBookButton[] books = MyThingsView.BooksGridLayoutGroup.GetComponentsInChildren<FinishedBookButton>(true);
            if (books.Length > 0)
            {
                foreach (FinishedBookButton book in books)
                {
                    book.gameObject.SetActive(true);
                }
                return;
            }

            for (int i = 0; i < AppController.GetController().GetCurrentUser().StartedBooks.Count; i++)
            {
                StartedBook startedBook = AppController.GetController().GetCurrentUser().StartedBooks[i];
                if (startedBook.State == StartedBookState.InProgress || _finishedBookList.Contains(startedBook.IdBook)) continue;
                FinishedBookButton bookButton = (Lean.LeanPool.Spawn(FinishedBookPrefab)).GetComponentInChildren<FinishedBookButton>();
                List<StartedBook> startedBooks = new List<StartedBook> {startedBook};
                for (int j = AppController.GetController().GetCurrentUser().StartedBooks.Count - 1; j > i; j--)
                {
                    if(AppController.GetController().GetCurrentUser().StartedBooks[j].IdBook != startedBook.IdBook && AppController.GetController().GetCurrentUser().StartedBooks[j].State == StartedBookState.InProgress) continue;
                    startedBooks.Add(AppController.GetController().GetCurrentUser().StartedBooks[j]);

                }

                bookButton.SetBook(startedBooks.ToArray());
                MyThingsView.AddResult(bookButton.gameObject);
                _finishedBookList.Add(startedBook.IdBook);
            }
        }

        private void HideStartedBooks()
        {
            StartedBookButton[] books = MyThingsView.BooksGridLayoutGroup.GetComponentsInChildren<StartedBookButton>();
            foreach (StartedBookButton book in books)
            {
                book.gameObject.SetActive(false);
            }
        }

        private void ShowStartedBooks()
        {
            MyThingsView.BooksGridLayoutGroup.cellSize = new Vector2(1343, 419);
            StartedBookButton[] books = MyThingsView.BooksGridLayoutGroup.GetComponentsInChildren<StartedBookButton>(true);
            if (books.Length > 0)
            {
                foreach (StartedBookButton book in books)
                {
                    book.gameObject.SetActive(true);
                }
                return;
            } 

            foreach (StartedBook startedBook in AppController.GetController().GetCurrentUser().StartedBooks)
            {
                if(startedBook.State == StartedBookState.Finished) continue;
                StartedBookButton bookButton = (Lean.LeanPool.Spawn(StartedBookPrefab)).GetComponentInChildren<StartedBookButton>();
                bookButton.SetStartedBook(startedBook);
                MyThingsView.AddResult(bookButton.gameObject);
            }
        }

        public static MyThingsController GetController()
        {
            return _myThingsController;
        }

        public void ShowPreviewFinishedBook(BookMetrics averageMetrics, Book book)
        {
            bool inProgress = false;
            foreach (StartedBook startedBook in AppController.GetController().GetCurrentUser().StartedBooks)
            {
                if (startedBook.IdBook == book.Id && startedBook.State == StartedBookState.InProgress)
                {
                    inProgress = true;
                    break;               
                }
            }
            MyThingsView.PreviewBook.gameObject.SetActive(true);
            MyThingsView.PreviewBook.SetData(book, averageMetrics, inProgress);
        }

        public void ShowPreviewStartedBook(Book book)
        {
            SoundController.GetController().PlayClickSound();
            MyThingsView.PreviewBook.SetData(book, null, true);

            MyThingsView.PreviewBook.gameObject.SetActive(true);
        }
    }
}
