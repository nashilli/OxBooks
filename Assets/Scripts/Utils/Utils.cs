﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Utils
{
    public static class Utils {

        public static T[] ShuffleArray<T>(T[] arr)
        {
            List<T> list = arr.ToList();
            List<T> shuffled = new List<T>(arr.Length);
            for (int i = arr.Length - 1; i >= 0; i--)
            {
                int index = Random.Range(0, list.Count);
                shuffled.Add(list[index]);
                list.RemoveAt(index);
            }

            /*
                        for (int i = arr.Length - 1; i >= 0; i--)
                        {
                            shuffled[i] = arr[i];
                        }
                        for (int i = shuffled.Length - 1; i > 0; i--)
                        {
                            int r = UnityEngine.Random.Range(0, i + 1);
                            T tmp = shuffled[i];
                            shuffled[i] = shuffled[r];
                            shuffled[r] = tmp;
                        }*/
            return shuffled.ToArray();
        }

        public static bool AnyToggleOnIn(GameObject gameObject)
        {
            foreach (Toggle toggle in gameObject.GetComponentsInChildren<Toggle>())
            {
                if (toggle.isOn) return true;
            }
            return false;
        }



        public static void UpdateTryButton(Toggle[] toggles, Button tryButton1, Button tryButton2 = null)
        {
            bool actualStatus = false;
            foreach (Toggle toggle in toggles)
            {
                actualStatus = toggle.isActiveAndEnabled && toggle.isOn;
                if (actualStatus) break;
            }

            UpdateTryButton(tryButton1, actualStatus);
            if(tryButton2) UpdateTryButton(tryButton2, actualStatus);

            
        }

        private static void UpdateTryButton(Button tryButton, bool actualStatus)
        {
            bool prevStatus = tryButton.enabled;
            if (prevStatus != actualStatus)
            {
                tryButton.enabled = actualStatus;
            }
        }

        public static T GetRandomEnumValue<T>()
        {
            Array values = Enum.GetValues(typeof(T));
            System.Random random = new System.Random();
            return (T) values.GetValue(random.Next(values.Length));
        }

    }
}
