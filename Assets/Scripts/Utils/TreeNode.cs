﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts._Book;

[Serializable]
public class TreeNode<Serializable>
{
    private readonly Serializable _value;
    private readonly List<TreeNode<Serializable>> _children = new List<TreeNode<Serializable>>();

    public TreeNode(Serializable value)
    {
        _value = value;
    }

    public TreeNode<Serializable> this[int i]
    {
        get { return _children[i]; }
    }

    public TreeNode<Serializable> Parent { get; private set; }

    public Serializable Value { get { return _value; } }

    public System.Collections.ObjectModel.ReadOnlyCollection<TreeNode<Serializable>> Children
    {
        get { return _children.AsReadOnly(); }
    }

    public TreeNode<Serializable> AddChild(Serializable value)
    {
        var node = new TreeNode<Serializable>(value) { Parent = this };
        _children.Add(node);
        return node;
    }

    public TreeNode<Serializable>[] AddChildren(params Serializable[] values)
    {
        return values.Select(e => AddChild(e)).ToArray();
    }

    public bool RemoveChild(TreeNode<Serializable> node)
    {
        return _children.Remove(node);
    }

    public void Traverse(Action<Serializable> action)
    {
        action(Value);
        foreach (var child in _children)
            child.Traverse(action);
    }

    public IEnumerable<Serializable> Flatten()
    {
        return new[] { Value }.Union(_children.SelectMany(x => x.Flatten()));
    }

    public List<TreeNode<Serializable>> PreviousNodes()
    {
        List<TreeNode<Serializable>> previousNodes = new List<TreeNode<Serializable>>();
        TreeNode<Serializable> currenTreeNode = this;
        while (currenTreeNode.Parent != null)
        {
            previousNodes.Add(currenTreeNode);
            currenTreeNode = currenTreeNode.Parent;
        }
        return previousNodes;
    }
}