﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.Scripts.Utils
{
    public class SpecialCursorOnEnter : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {
    
        public Texture2D CursorTexture;
        public CursorMode cursorMode = CursorMode.Auto;
        public Vector2 hotSpot = Vector2.zero;

        public void OnPointerEnter(PointerEventData eventData)
        {
            //Cursor.SetCursor(CursorTexture, hotSpot, cursorMode);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            //Cursor.SetCursor(null, Vector2.zero, cursorMode);
        }
    }
}
