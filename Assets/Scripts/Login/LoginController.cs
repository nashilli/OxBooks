﻿using UnityEngine;
using Assets.Scripts.Settings;
using Assets.Scripts.App;
using Assets.Scripts.Common;
using Avatar = Assets.Scripts.App.Avatar;

namespace Assets.Scripts.Login
{

    public class LoginController : MonoBehaviour
    {
        private static LoginController loginController;
        public LoginView loginView;

        void Awake(){
            if (loginController == null){
                loginController = this;
            }else if (loginController != this){
                Destroy(gameObject);
            }
        }

        public void SaveUsername(string username){
            if(username != "" && username.Length > 2)
            {
                AppController.GetController().LoadUser(username);
                if (AppController.GetController().GetCurrentUser().Avatar == Avatar.None)
                {
                    ViewController.GetController().LoadSelectAvatar();
                    Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);

                }
                else
                {
                    ViewController.GetController().LoadExplore();
                    ViewController.GetController().LoadTopbar();
                    Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);

                }
            } else{
                loginView.ShowIncorrectInputAnimation();
            }

        }

        internal void GoBack() {
            ViewController.GetController().LoadCover();
        }

        public static LoginController GetController(){
            return loginController;
        }
    }
}
