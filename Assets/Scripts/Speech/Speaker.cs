﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Common.Action;
using Assets.Scripts._Book;

public class Speaker : MonoBehaviour
{

    private static Speaker _instanceSpeaker;
    public float Volume { get; set; }
    public float SpeechRate { get; set; }
    public float Pitch { get; set; }

    void Awake()
    {
        if (_instanceSpeaker == null) _instanceSpeaker = this;
        else if(_instanceSpeaker != this) Destroy(this.gameObject);
    }

	// Use this for initialization
	void Start () {
        TTSManager.Initialize(transform.name, "OnTTSInit");
        Volume = 1;
        SpeechRate = 0.4f;
        Pitch = 1;
    }

    void OnTTSInit(string message)
    {
        int response = int.Parse(message);

        switch (response)
        {
            case TTSManager.SUCCESS:
                List<TTSManager.Locale> l = TTSManager.GetAvailableLanguages();
                if (TTSManager.IsLanguageAvailable(new TTSManager.Locale("es", "es")))
                {
                    TTSManager.SetLanguage(new TTSManager.Locale("es", "es"));
                }
                //TTSManager.Speak("Prueba de texto. Prueba- Prueba de texto. Prueba-Prueba de texto. Prueba-", false, TTSManager.STREAM.Music, 1f, 0f, transform.name, "OnSpeechCompleted", "speech_" + (1));

                break;
            case TTSManager.ERROR:
                Debug.Log("Error al iniciar");
                break;
        }
    }

    public static Speaker GetSpeaker()
    {
        return _instanceSpeaker;
    }

    public void Speak(string txt)
    {
        TTSManager.SetPitch(Pitch);
        TTSManager.SetSpeechRate(SpeechRate);
        TTSManager.Speak(txt, false, TTSManager.STREAM.Music, Volume, 0f, transform.name, "OnSpeechCompleted", "speech_" + (0));
    }

    public void OnSpeechCompleted()
    {
        BookViewController.GetController().GetPlayTextAction().NextWord();
    }


    public void Silence()
    {
        TTSManager.Stop();
    }
}
