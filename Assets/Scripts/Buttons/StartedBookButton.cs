﻿using System.Linq;
using Assets.Scripts.App;
using Assets.Scripts.Common;
using Assets.Scripts.Common.Action;
using Assets.Scripts.MyThings;
using Assets.Scripts.Sound;
using Assets.Scripts._Book;
using Assets.Scripts._Book.Page;
using ProgressBar;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Buttons
{
    public class StartedBookButton : MonoBehaviour
    {

        public Button ContinueBookButton;

        public Image BookImage;
        public Image BackImage;
        public Text GenreText;
        public Text AuthorText;
        public Text TitleText;
        public Text PagesText;
        public Text ExercisesText;
        public Text PercentageText;

        public ProgressBarBehaviour PagesProgressBarBehaviour;
        public ProgressBarBehaviour ExercisesProgressBarBehaviour;


    

           

        public void SetStartedBook(StartedBook startedBook)
        {
            Book book = AppController.GetController().GetBook(startedBook.IdBook);

            GenreText.text = string.Join(" - ", book.Genres);
            AuthorText.text = book.Author;
            TitleText.text = book.Title;

            float readPages = startedBook.GetRootNode().Flatten().ToList().Count - 1;
            float totalPages = book.Pages.Count;
            PagesText.text = readPages + "/" + totalPages + " páginas leídas.";
            int totalPercentage = Mathf.RoundToInt((readPages / totalPages) * 100);
            PagesProgressBarBehaviour.Value = totalPercentage;
            float totalExercies = book.GetExercisesCount();
            float resolvedExercises = startedBook.GetResolvedExercises();
            ExercisesText.text = resolvedExercises + "/" + totalExercies + " ejercicios resueltos.";
            ExercisesProgressBarBehaviour.Value = Mathf.RoundToInt((resolvedExercises / totalExercies) * 100);
            PercentageText.text = totalPercentage + " %";

            ContinueBookButton.onClick.AddListener(delegate
            {
                SoundController.GetController().PlayClickSound();
                MyThingsController.GetController().ShowPreviewStartedBook(book);

            });
/*
            ContinueBookButton.MyAction = new ReadBookAction(book);
*/

            string imagePath = ((CoverPage)book.Pages[0]).ImagePath;
            Sprite sprite = Resources.Load<Sprite>(imagePath.Substring(0, imagePath.Length - 2) + "Icon");
            BackImage.sprite = sprite;
            BookImage.sprite = sprite;
            BookImage.type = Image.Type.Filled;
            BookImage.fillMethod = Image.FillMethod.Vertical;
            BookImage.fillAmount = (totalPercentage + 0f) / 100;
        }

    }
}
