﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Buttons
{
    public class BookButton : MonoBehaviour
    {
        public Button ShowPreviewBookButton;
        public Image BookImage;
        public Text BookTitleText;
        public long IdBook { get; set; }

        private int _level;

        void Awake()
        {
            _level = -1;
            IdBook = -1;
        }

        public int GetLevel()
        {
            return _level; 
        }

        public void SetLevel(int value)
        {
            _level = value;
        }
    }
}