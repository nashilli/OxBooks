﻿using System;
using Assets.Scripts.Common;
using UnityEngine;
using Assets.Scripts.Settings;
using Assets.Scripts.Sound;
using Assets.Scripts._Book;
using Assets.Scripts._Book.PageView;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts.App
{
    public class ViewController : MonoBehaviour
    {
        private static ViewController viewController;


        public GameObject viewPanel;

        private GameObject currentGameObject;
        //private GameObject CurrentPageView;

        private GameObject inGameMenuScreen;
        private GameObject instructionsScreen;
        private GameObject _descriptionPopUp;
        private GameObject topBar;
        private GameObject InfoPopUp;
        public GameObject BlockScreenPanel;
        

        public Image LoadingImage;

        private Action _onBookViewControllerLoadedAction;


        private DateTime _time;

        void Awake()
        {
            if (viewController == null) viewController = this;
            else if (viewController != this) Destroy(gameObject);
            DontDestroyOnLoad(this);
        }

        void Start()
        {
            LoadCover();
        }

        internal void LoadProfile()
        {
            ChangeCurrentObject(LoadPrefab("Profile"));
            currentGameObject.transform.SetAsFirstSibling();
            if (!SettingsController.GetController().GetMusic()) SoundController.GetController().StopMusic();
            else SoundController.GetController().PlayMusic();
        }

        private GameObject LoadPrefab(string name)
        {
            return Resources.Load<GameObject>("Prefabs/" + name);
        }

        internal void LoadCover()
        {
            ChangeCurrentObject(LoadPrefab("Cover"));
        }

        internal void LoadMetrics()
        {
            ChangeCurrentObject(LoadPrefab("Metrics"));
            this.gameObject.GetComponent<CanvasScaler>().matchWidthOrHeight = 1f;
        }

        private void ChangeCurrentObject(GameObject newObject)
        {
            _time = DateTime.Now;
            this.gameObject.GetComponent<CanvasScaler>().matchWidthOrHeight = 1f;
            GameObject child = Instantiate(newObject);
            FitObjectTo(child, viewPanel);
            Destroy(currentGameObject);
            currentGameObject = child;
            

        }

        public void HideLoading()
        {
            LoadingImage.GetComponentsInChildren<Image>()[1].fillAmount = 1;
            Invoke("HideLoadingDelayed", 0.2f);
        }

        private void HideLoadingDelayed()
        {
            LoadingImage.gameObject.SetActive(false);

        }

        public void ShowLoading()
        {
            LoadingImage.gameObject.SetActive(true);
        }

        internal void ShowInGameMenu()
        {
            if (inGameMenuScreen == null)
            {
                HideInstructions();
                inGameMenuScreen = Instantiate(LoadPrefab("IngameMenu"));
                FitObjectTo(inGameMenuScreen, viewPanel);
            }

        }

        internal void FitObjectTo(GameObject child, GameObject parent)
        {
            child.transform.SetParent(parent.transform, true);
            child.transform.localPosition = Vector3.zero;
            child.GetComponent<RectTransform>().offsetMax = Vector2.zero;
            child.GetComponent<RectTransform>().offsetMin = Vector2.zero;
            child.transform.localScale = Vector3.one;
        }

        internal void FitObjectToInLocalPosition(GameObject child, GameObject parent)
        {
            Vector3 localPosition = child.transform.localPosition;
            child.transform.SetParent(parent.transform, true);
            child.transform.localScale = Vector3.one;
            child.transform.localPosition = localPosition;
        }

        /*   internal void RestartCurrentGame()
           {
               currentGameObject.GetComponent<BookViewController>().RestartGame();
           }*/

        internal void LoadSettings()
        {
            ChangeCurrentObject(LoadPrefab("Settings"));
        }

        internal void LoadLogin()
        {
            ChangeCurrentObject(LoadPrefab("Login"));
        }

        /*  internal void StartGame(Game game)
          {
              Debug.Log("Prefab: " + game.GetPrefabName());
              ChangeCurrentObject(LoadPrefab("Games/" + game.GetPrefabName()));
              if(game.GetId() == 25 || game.GetId() == 4 || game.GetId() == 29 || game.GetId() == 1 || game.GetId() == 3 || game.GetId() == 5 || game.GetId() == 6 || game.GetId() == 7 || game.GetId() == 19 || game.GetId() == 14 || game.GetId() == 16 || (game.GetId() >= 20 && game.GetId() <= 24)) this.gameObject.GetComponent<CanvasScaler>().matchWidthOrHeight = 1f;     
          }*/

        internal void ShowInstructions()
        {
            instructionsScreen = Instantiate(LoadPrefab("Instructions"));
            FitObjectTo(instructionsScreen, viewPanel);
        }

        internal void HideInGameMenu()
        {
            Destroy(inGameMenuScreen);
        }

        internal void HideInstructions()
        {
            Destroy(instructionsScreen);
        }

        internal void LoadLevelCompleted()
        {
            ChangeCurrentObject(LoadPrefab("LevelCompleted"));
        }


        public static ViewController GetController()
        {
            return viewController;
        }

        public void LoadMyThings()
        {
            ChangeCurrentObject(LoadPrefab("MyThings"));
            currentGameObject.transform.SetAsFirstSibling();

        }

        public void LoadExplore()
        {
            ChangeCurrentObject(LoadPrefab("Explore"));
            currentGameObject.transform.SetAsFirstSibling();

        }

        public void LoadTopbar()
        {
            GameObject prefab = LoadPrefab("Topbar");
            topBar = Instantiate(prefab);
            FitObjectTo(topBar, viewPanel);
            /*  RectTransform rectTransform = topBar.GetComponent<RectTransform>();
              rectTransform.sizeDelta = new Vector2(topBar.transform.parent.GetComponent<RectTransform>().sizeDelta.x, 200);
              rectTransform.localPosition = new Vector3(rectTransform.localPosition.x, 620, 0);
              //FitObjectToInLocalPosition(topBar, viewPanel);*/
        }

        /*   public GameObject LoadPagePrefab(string pagePrefabName)
           {
               Destroy(CurrentPageView);
               CurrentPageView = Instantiate(LoadPrefab("Pages/" + pagePrefabName));
               FitObjectTo(CurrentPageView, currentGameObject);
               CurrentPageView.transform.SetAsFirstSibling();
               return CurrentPageView;
           }*/

        public void LoadBookView(Action action)
        {
            _onBookViewControllerLoadedAction = action;
            Invoke("LoadBookViewDelayed", 0.1f);

            
        
       /* SceneManager.LoadScene(1);
            HideLoading();*/
        }

        private void LoadBookViewDelayed()
        {
            ChangeCurrentObject(LoadPrefab("BookView"));
            UnloadTopbar();
        }

        private void UnloadTopbar()
        {
            Destroy(topBar);
        }

        public PageView GetCurrentPageView()
        {
            return GetComponentInChildren<PageView>();
        }

        public GameObject LoadDescriptionPopUp()
        {
            if (_descriptionPopUp != null) return _descriptionPopUp;
            _descriptionPopUp = Instantiate(LoadPrefab("DescriptionPopUp"));
            FitObjectTo(_descriptionPopUp, GetCurrentPageView().gameObject);
            return _descriptionPopUp;
        }

        public GameObject GetTopbar()
        {
            return topBar;
        }

        public GameObject GetCurrentGameObject()
        {
            return currentGameObject;
        }

        public void LoadSelectAvatar()
        {
            ChangeCurrentObject(LoadPrefab("SelectAvatar"));
        }

        public GameObject GetDescriptionPopUp()
        {
            return _descriptionPopUp;
        }

        public void HideDescriptionPopUp()
        {
            if (_descriptionPopUp != null) Destroy(_descriptionPopUp);
        }

        public void ShowInfoPopUp(string messagge)
        {
            GameObject child = Instantiate(LoadPrefab("InfoPopUp"));
            FitObjectTo(child, GetCurrentGameObject());
            child.GetComponentInChildren<InfoPopUp>().SetMessage(messagge);
        }

        public void OnBookViewControolerLoaded()
        {
            _onBookViewControllerLoadedAction();
        }

        public void BlockScreen()
        {
            BlockScreenPanel.SetActive(true);
        }

        public void UnBlockScreen()
        {
            BlockScreenPanel.SetActive(false);
        }
    }
}
