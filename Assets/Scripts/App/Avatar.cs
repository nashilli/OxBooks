﻿using System;
using Assets.Scripts.Utils;
using UnityEngine;

namespace Assets.Scripts.App
{
    
    public class AvatarInfoAttribute : Attribute
    {
        internal AvatarInfoAttribute(string path)
        {
            this.Path = path;
        }
        public string Path { get; private set; }
    }

    [Serializable]
    public enum Avatar
    {
        [AvatarInfo("")]
        None,
        [AvatarInfo("")]
        A,
        [AvatarInfo("")]
        B,
        [AvatarInfo("")]
        C,
        [AvatarInfo("")]
        D,
        [AvatarInfo("")]
        E,
        [AvatarInfo("")]
        F,
        [AvatarInfo("")]
        G,
        [AvatarInfo("")]
        H,
        [AvatarInfo("")]
        I,
        [AvatarInfo("")]
        J
    }

    public static class AvatarExtensions
    {
        public static Sprite LoadBigPicture(this Avatar p)
        {
            var attr = p.GetAttribute<AvatarInfoAttribute>();
            return Resources.Load<Sprite>(attr.Path + "Avatar/Big/bigPicture_" + (int)p);
        }

        public static Material LoadProfilePicture(this Avatar p)
        {
            var attr = p.GetAttribute<AvatarInfoAttribute>();
            return Resources.Load<Material>(attr.Path + "Avatar/Profile/profilePicture_" + (int) p);
        }

    }

    

}