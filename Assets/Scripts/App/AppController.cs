﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using AssetBundles;
using Assets.Scripts.Common;
using Assets.Scripts._Book;
using Assets.Scripts._Book.Page;
using SimpleJSON;
using UnityEngine;

namespace Assets.Scripts.App
{
    public class AppController : MonoBehaviour
    {
        private Tuple<int, string>[] _genresTuple;

        private static AppController appController;
        private User currentUser;
        private AppModel appModel;
        [SerializeField]
        private List<Book> books;


        // variable for books without pageid
        private int _lastId;

        public int LastSelectedLevel;
        public Language LastSelectedLanguage;

        void Awake()
        {
            //LastSelectedFilter = 0;
            if (appController == null) appController = this;
            else if (appController != this) Destroy(gameObject);     
            DontDestroyOnLoad(gameObject);
            appModel = new AppModel();
            LoadGenres();
            books = new List<Book>();
            LoadBookFromResources();
            LoadBookFromDocuments();


            List<Tuple<long, string>> idList = new List<Tuple<long, string>>(books.Count);
            foreach (Book book in books)
            {
                for (int i = idList.Count - 1; i >= 0; i--)
                {
                    if (idList[i].First == book.Id || book.Id == 55)
                    {
                        Debug.Log("Repeated id (" + book.Id +")" + ". Books in conflict: " + book.Title + " and " + idList[i].Second);
                    }
                }
               idList.Add(new Tuple<long, string>(book.Id, book.Title));
            }
   
        }

        private void LoadBookFromDocuments()
        {
            if(!Directory.Exists(Application.dataPath + "/Books/")) return;
            string[] paths = Directory.GetFiles(Application.dataPath + "/Books/", "*.json");
            foreach (string path in paths)
            {
                if(path != "") books.Add(LoadBookFromJson(JSONNode.Parse(File.ReadAllText(path))));
            }
        }

        private void LoadBookFromResources()
        {
            TextAsset[] textAssets = Resources.LoadAll<TextAsset>("Books/");
            foreach (TextAsset textAsset in textAssets)
            {
               
                books.Add(LoadBookFromJson(JSON.Parse(textAsset.text)));
            }

        }

        private void LoadGenres()
        {

            //string txt = File.ReadAllText(Application.persistentDataPath + "/Jsons/" + "genres.json");
            TextAsset textAsset = Resources.Load<TextAsset>("genres");
            JSONNode node = JSON.Parse(textAsset.text);
            JSONNode jsonNode = node["genres"];
            
            _genresTuple = new Tuple<int, string>[jsonNode.Count];
            for (int i = jsonNode.Count - 1; i >= 0; i--)
            {
                string second = jsonNode[i]["genre"];
                _genresTuple[i] = new Tuple<int, string>(int.Parse(jsonNode[i]["id"]), second);
            }
        }

        private Book LoadBookFromJson(JSONNode book)
        {
/*
            JSONNode book = JSON.Parse(jsoNstring.text);
*/
            string title = book["title"];
            
            string folder = "Ilustrations/" + (book["resourcesFolder"] == null ? title.ToLower() : book["resourcesFolder"].Value);
            JSONNode pages = book["pages"];
            if (book["level"] == null) throw new Exception(title + " no tiene level asignado");
            Language language = book["language"] != null ? (Language) int.Parse(book["language"]) : Language.Spanish; 
            Book newBook = new Book(book["id"] != null ? long.Parse(book["id"]) : 55, book["author"], book["description"], title, ObtainGenres(book["genres"]), int.Parse(book["level"]), language, book["assetBundle"].Value);

            SetIllustrator(newBook, book["illustrator"]);
            newBook.EstimatedReadTimeInMinutes = book["estimatedTime"].AsInt;
            _lastId = 0;
            for (int i = 0; i < pages.Count; i++)
            {
                Page page = ObtainPageFromJson(pages[i], folder);
                if (page == null) throw new Exception(title + " page " + i + " no tiene tipo");
                newBook.AddPage(page);
               /* if (( page as MultipleChoicePage ) != null)
                {
                    if (!(page is EtpaPage) && !(((MultipleChoicePage)page).Question.Contains("¿")))
                    {
                        Debug.Log("No tiene signo " + title + " page " + i);
                    }
                }*/
                _lastId++;
            }
            ((CoverPage) newBook.Pages[0]).Text = title;
            return newBook;
        }

        private void SetIllustrator(Book book, JSONNode jsonNode)
        {
            Illustrator bookIllustrator = new Illustrator();
            bookIllustrator.FirstName = jsonNode["firstname"];
            bookIllustrator.LastName = jsonNode["lastname"];
            bookIllustrator.ContactForms = new List<string>(jsonNode["lastname"].Count);
            foreach (string contactForm in jsonNode["contactForms"].AsArray)
            {
                bookIllustrator.ContactForms[contactForm.Length] = contactForm;
            }
            bookIllustrator.ContactForms = new List<string>(jsonNode["contactForms"].Count);
            book.Illustrator = bookIllustrator;
        }

        private string[] ObtainGenres(JSONNode jsonNode)
        {
            
            string[] genres = new string[jsonNode.Count];
            for (int i = jsonNode.Count - 1; i >= 0; i--)
            {
                genres[i] = _genresTuple[int.Parse(jsonNode[i].Value) - 1].Second;
            }
            return genres;
        }

        private Page ObtainPageFromJson(JSONNode page, string folderPath)
        {
            // for test purposes
            if (page["pageType"] == null) return null;
            switch ((PageType) int.Parse(page["pageType"]))
            {
                case PageType.Cover:
                    CoverPage coverPage = new CoverPage(ObtainPageId(page))
                    {
                        ImagePath = ObtainImagePath(page, folderPath),
                        NextPageId = ObtainNextPageId(page),
                        AudioPath = ObtainAudioPath(page, folderPath)

                    };
                    return coverPage;
                case PageType.Text:
                    TextPage textPage = new TextPage(ObtainPageId(page))
                    {
                        Text = ObtainText(page),
                        SpecialWords = ObtainSpecialWords(page),
                        AudioPath = ObtainAudioPath(page, folderPath),
                        NextPageId = ObtainNextPageId(page)

                    };
                    return textPage;
                case PageType.Image:
                    ImagePage imagePage = new ImagePage(ObtainPageId(page))
                    {
                        ImagePath = ObtainImagePath(page, folderPath),
                        AudioPath = ObtainAudioPath(page, folderPath),
                        NextPageId = ObtainNextPageId(page)

                    };
                    return imagePage;
                case PageType.TextAndImage:
                    TextAndImagePage textAndImagePage = new TextAndImagePage(ObtainPageId(page))
                    {
                        ImagePath = ObtainImagePath(page, folderPath),
                        Text = ObtainText(page),
                        SpecialWords = ObtainSpecialWords(page),
                        AudioPath = ObtainAudioPath(page, folderPath),
                        NextPageId = ObtainNextPageId(page)


                    };
                    return textAndImagePage;
                case PageType.MultipleChoice:

                    MultipleChoicePage choicePage = new MultipleChoicePage(ObtainPageId(page), ObtainText(page) != null ? ChoiceType.Text : ChoiceType.Image)
                    {
                        Text = ObtainText(page),
                        ImagePath = ObtainImagePath(page, folderPath),
                        Question = ObtainQuestion(page),
                        Options = ObtainOptions(page, folderPath),
                        HintTexts =  ObtainHintTexts(page),
                        SpecialWords = ObtainSpecialWords(page),
                        AudioPath = ObtainAudioPath(page, folderPath),
                        NextPageId = ObtainNextPageId(page)

                    };

                    return choicePage;
                case PageType.LetterSoup:
                    LetterSoup letterSoup = new LetterSoup(ObtainPageId(page));
                    JSONNode objectiveWords = page["objectiveWords"];
                    for (int i = 0; i < objectiveWords.Count; i++)
                    {
                        ObjectiveWord objectiveWord = new ObjectiveWord(objectiveWords[i]["question"], objectiveWords[i]["word"], ((string)objectiveWords[i]["orientation"]).ToLower() == "horizontal" ? WordOrientation.Horizontal : WordOrientation.Vertical, new Point(int.Parse(objectiveWords[i]["initialPoint"]["x"]), int.Parse(objectiveWords[i]["initialPoint"]["y"])));
                        letterSoup.AddObjectiveWord(objectiveWord);


                    }
                    letterSoup.LocateObjectiveWords();
                    letterSoup.FillBlanks();
                    letterSoup.NextPageId = ObtainNextPageId(page);

                    return letterSoup;
                case PageType.ReadingFluency:
                    ReadingFluencyPage readingFluency = new ReadingFluencyPage(ObtainPageId(page))
                    {
                        Text = ObtainText(page),
                        FluencySpeed = 1,
                        Options = ObtainOptions(page, folderPath),
                        Question = ObtainQuestion(page),
                        NextPageId = ObtainNextPageId(page)

                    };
                    return readingFluency;
                case PageType.Etpa:
                    EtpaPage etpa = new EtpaPage(ObtainPageId(page),
                        ObtainText(page) != null ? ChoiceType.Text : ChoiceType.Image)
                    {
                        Text = ObtainText(page),
                        ImagePath = ObtainImagePath(page, folderPath),
                        Question = ObtainQuestion(page),
                        Options = ObtainOptions(page, folderPath),
                        HintTexts = ObtainHintTexts(page),
                        SpecialWords = ObtainSpecialWords(page),
                        AudioPath = ObtainAudioPath(page, folderPath)
                    };
                    return etpa;

            }
            Debug.Log("Error while we are obtaining page from json. Please check it.");
            throw new Exception("Error while we are obtaining page from json. Please check it.");
        }

        private int ObtainNextPageId(JSONNode page)
        {
            int nextPageId = page["nextIdPage"] == null ? _lastId + 1 : int.Parse(page["nextIdPage"].Value);
            return nextPageId;
        }

        private int ObtainPageId(JSONNode page)
        {
            return !string.IsNullOrEmpty(page["pageId"].Value) ? int.Parse(page["pageId"].Value) : _lastId;
        }

        private string ObtainAudioPath(JSONNode page, string folderPath)
        {
            return (folderPath + "/" + page["audio"]).ToLower().Replace("ñ", "n").Replace("ü", "u").Replace("á", "a").Replace("é", "e").Replace("í", "i").Replace("ó", "o").Replace("ú", "u");
        }

        private string ObtainQuestion(JSONNode page)
        {
            
            return page["question"];
        }

        private Option[] ObtainOptions(JSONNode page, string folderPath)
        {
            Option[] options = new Option[page["options"].Count];
            for (int i = 0; i < page["options"].Count; i++)
            {
                string text = page["options"][i]["text"];
                int nextIdPage = page["options"][i]["nextIdPage"].AsInt;
                JSONNode node = page["options"][i]["isCorrect"];
                options[i] = new Option
                {
                    Text = text,
                    ImagePath = ObtainImagePath(page["options"][i], folderPath),
                    IsTrue = node.AsBool,
                    NextPageId = nextIdPage
                };
            }
            return options;
        }

        private SpecialWord[] ObtainSpecialWords(JSONNode page)
        {
            if(page["specialWords"] == null) return new SpecialWord[0];
            SpecialWord[] specialWords = new SpecialWord[page["specialWords"].Count];

            for (int i = page["specialWords"].Count - 1; i >= 0; i--)
            {
                specialWords[i] = new SpecialWord
                {
                    WordInText = page["specialWords"][i]["wordInText"],
                    GenericWord = page["specialWords"][i]["genericWord"],
                    Description = page["specialWords"][i]["description"]
                };
            }

            return specialWords;
        }

        private string[] ObtainHintTexts(JSONNode page)
        {
            string[] hintTexts = new string[page["hint"].Count];
            for (int i = page["hint"].Count - 1; i >= 0; i--)
            {
                hintTexts[i] = page["hint"][i];
            }
            return hintTexts;
        }

        private string ObtainText(JSONNode page)
        {
            return page["text"];
        }

        private string ObtainImagePath(JSONNode page, string folderPath)
        {
            return string.IsNullOrEmpty(page["image"]) ? "" : (folderPath + "/" + page["image"]).ToLower().Replace("ñ","n").Replace("ü", "u").Replace("á","a").Replace("é","e").Replace("í", "i").Replace("ó", "o").Replace("ú", "u");
        }

        private void AddOneBook()
        {
            Book aBook = new Book(999, "Nash", "Este es mi primer libro.", "Title", new []{"genre"}, 0, Language.Spanish, "none");


            // Cover
            CoverPage aCoverPage = new CoverPage(0)
            {
                ImagePath = "Avatar/avatar_0",
                Text = "My title"
            };

            TextPage aTextPage = new TextPage(2);
            aTextPage.Text = "Prueba de texto. Nash está probando. ¿Qué tal? !Hola! K-Po. Prueba de texto. Nash está probando. ¿Qué tal? !Hola! K-Po ";
            SpecialWord[] specialWords = new SpecialWord[3];
            specialWords[0] = new SpecialWord();
            specialWords[0].WordInText = "prueba";
            specialWords[0].Description = "Esta paalabra es prueba";

            specialWords[1] = new SpecialWord();
            specialWords[1].WordInText = "Nash";
            specialWords[1].Description = "Esta paalabra es nash";

            specialWords[2] = new SpecialWord();
            specialWords[2].WordInText = "¡HOLA!";
            specialWords[2].Description = "Esta paalabra es ¿hola!";
            aTextPage.SpecialWords = specialWords;

            aTextPage.SpecialWords = specialWords;
            

            LetterSoup letterSoup = new LetterSoup(1);
            letterSoup.GenereteRandom();
            

            TextAndImagePage aTextAndImagePage = new TextAndImagePage(2)
            {
                ImagePath = "Avatar/avatar_4",
                Text = "Prueba de texto. Nash está probando. ¿Qué tal? !Hola! K-Po"
            };

            Option[] options = new Option[4];
            for (int i = options.Length - 1; i >= 0; i--)
            {
                Option aOption = new Option
                {
                    IsTrue = i > 0,
                    Text = "Option " + LetterSoup.GetLetter() + " - " + (i > 0)
                };
                options[i] = aOption;
            }

            ReadingFluencyPage readingFluency = new ReadingFluencyPage(1)
            {
                Text = "Nash está probando fluidez de lectura. ¿Qué pasará?",
                Question = "Lees fluido?",
                Options = options
                
            };

            MultipleChoicePage aMultipleChoicePage = new MultipleChoicePage(3, ChoiceType.Text)
            {
                Text = "Prueba de texto. Qué tal queda.",
                Question = "This is my question",
                Options = options
            };


            Option[] options2 = new Option[3];
            for (int i = options2.Length - 1; i >= 0; i--)
            {
                Option aOption = new Option
                {
                    IsTrue = i > 0,
                    Text = "Option " + LetterSoup.GetLetter() + " - " + (i > 0)
                };
                options2[i] = aOption;
            }

            MultipleChoicePage aMultipleChoicePage2 = new MultipleChoicePage(3, ChoiceType.Text)
            {
                Text = "Prueba de texto. Qué tal queda.",
                Question = "This is my question",
                Options = options2
            };

            Option[] options3 = new Option[4];
            for (int i = options3.Length - 1; i >= 0; i--)
            {
                Option aOption = new Option
                {
                    IsTrue = (i == 0),
                    Text = "Option " + LetterSoup.GetLetter() + " - " + (i == 0)
                };
                options3[i] = aOption;
            }

            MultipleChoicePage aMultipleChoicePag3e = new MultipleChoicePage(3, ChoiceType.Text)
            {
                Text = "Prueba de texto. Qué tal queda.",
                Question = "This is my question",
                Options = options3
            };

            Option[] options4 = new Option[3];
            for (int i = options4.Length - 1; i >= 0; i--)
            {
                Option aOption = new Option
                {
                    IsTrue = (i == 0),
                    Text = "Option " + LetterSoup.GetLetter() + " - " + (i == 0)  
                };
                options4[i] = aOption;
            }

            MultipleChoicePage aMultipleChoicePag4e = new MultipleChoicePage(3, ChoiceType.Text)
            {
                Text = "Prueba de texto. Qué tal queda.",
                Question = "This is my question",
                Options = options4
            };



            aBook.AddPage(aCoverPage);
            aBook.AddPage(aTextPage);
            aBook.AddPage(readingFluency);
            aBook.AddPage(letterSoup);
            aBook.AddPage(aTextAndImagePage);
            aBook.AddPage(aMultipleChoicePage);
            aBook.AddPage(aMultipleChoicePage2);
            aBook.AddPage(aMultipleChoicePag3e);
            aBook.AddPage(aMultipleChoicePag4e);

            books.Add(aBook);

        }


        internal void BackToGame()
        {
            // todo ver si meter timer
            //Timer.GetTimer().Resume();
        }
      
        public int GetCurrentLevel()
        {
            return appModel.GetCurrentLevel();
        }

        internal void ShowInGameMenu(){
            // todo ver si meter timer
            //Timer.GetTimer().Pause();
            ViewController.GetController().ShowInGameMenu();
        }
 

        public static AppController GetController()
        {
            return appController;
        }

        internal int GetCurrentArea()
        {
            return appModel.GetCurrentArea();
        }

        public void LoadUser(string username)
        {
            string path = Application.persistentDataPath + "/" + username.ToLower() + "_user.OxBooks";
            if (File.Exists(path))
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Open(path, FileMode.Open);
                currentUser = (User) bf.Deserialize(file);
                // todo only for books with pages havent got id
                currentUser.UpdateStartedIdPages();
                currentUser.CurrentBook = null;
                file.Close();
            }
            else
            {
                currentUser = new User(username);
            }
        }

        public User GetCurrentUser()
        {
            return currentUser;
        }

        public Book GetABook()
        {
            return books[0];
        }

        public void SaveAdvance()
        {
            //currentUser.CurrentBook.CurrentPage = BookViewController.GetController().GetCurrentPageId();
            if (currentUser.CurrentBook.Mode == Mode.View) return;
            currentUser.SaveCurrentBook();
            currentUser.SaveToDisk();
        }

        public Book[] GetBooks(int level)
        {
           return books.FindAll(s => s.Level == level).ToArray();
        }

        public Book GetBook(long idBook)
        {
            foreach (Book book in books)
            {
                if (book.Id == idBook) return book;
            }
            throw new Exception("Libro no encontrado");
        }

        public int GetLevels()
        {
            return 6;
        }

        public string[] GetDistinctGenresFrom(int level, Language language)
        {
            List<string> genres = new List<string>();
            
            foreach (Book book in books)
            {
                if (book.Level == level && book.Language == language)
                {
                    foreach (string genre in book.Genres)
                    {
                        if(!genres.Contains(genre)) genres.Add(genre);
                    }
                }    
            }

            return genres.ToArray();
        }

        public IEnumerable<Book> GetAllBooks()
        {
            return books;
        }
    }
}
