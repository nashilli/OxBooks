﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.App
{
    internal class AppModel
    {
        private int currentArea;
        private int currentGame;
        private int currentLevel;


        internal int GetCurrentGame(){
            return currentGame;
        }

        internal void SetCurrentGame(int currentGame){
            this.currentGame = currentGame;
        }

        internal int GetCurrentLevel()
        {
            return currentLevel;
        }

        internal void SetCurrentLevel(int level)
        {
            currentLevel = level;
        }

        internal int GetCurrentArea()
        {
            return currentArea;
        }

        internal void SetCurrentArea(int area)
        {
            this.currentArea = area;
        }

    }

    public enum Language
    {
        Spanish, English
    }
}