﻿using System;
using UnityEngine;

namespace Assets.Scripts.App
{
    public class Coroutiner : MonoBehaviour
    {
        private static Coroutiner _coroutiner;

        public Action Action;
        public float Seconds;

        public void ExecuteIn(Action action, float seconds)
        {
            Action = action;
            Seconds = seconds;
        }

        void Awake()
        {
            if (_coroutiner == null) _coroutiner = this;
            else if(_coroutiner != this) Destroy(this.gameObject);
        }

        void Update()
        {
            if (Action != null)
            {
                Seconds -= Time.deltaTime;
                if (Seconds <= 0)
                {
                    Action aux = (Action) Action.Clone();
                    Action = null;
                    aux();
                }
                    
            }
        }

        public void Stop()
        {
            Action = null;
        }

        public static Coroutiner GetCoroutiner()
        {
            return _coroutiner;
        }

        
    }
}