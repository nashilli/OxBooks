﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using Assets.Scripts.Settings;
using Assets.Scripts._Book;
using UnityEngine;

namespace Assets.Scripts.App
{
    [Serializable]
    public class User
    {
        public Avatar Avatar { get; set; }
        public string Name { get; private set; }
        //private string lastname;
        //private DateTime borndDate;
        public UserLevel Level { get; private set; }
        public List<Trophy> Trophies { get; private set; }
        public List<StartedBook> StartedBooks { get; private set; }
        public StartedBook CurrentBook { get; set; }

        //public List<int> CreatedBooks { get; private set; }

        public User(string name)
        {
            this.Name = name;

            Avatar = Avatar.None;
            SetLevel();

            Trophies = new List<Trophy>();
            StartedBooks = new List<StartedBook>();
            //CreatedBooks = new List<int>();
        }

        private void SetLevel()
        {           
            Level = Utils.Utils.GetRandomEnumValue<UserLevel>();
        }

       

        public StartedBook GetStartedBookNotFinished(long id)
        {
            foreach (StartedBook startedBook in StartedBooks)
            {
                if (startedBook.IdBook == id && startedBook.State == StartedBookState.InProgress) return startedBook;
            }
            return null;
        }

        public void AddNewStartedBook(long id, int pagesLenght)
        { 
            StartedBooks.Add(new StartedBook(id, pagesLenght, Mode.Play));
            CurrentBook = StartedBooks[StartedBooks.Count - 1];
        }

        public void SaveToDisk()
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Create(Application.persistentDataPath + "/" + AppController.GetController().GetCurrentUser().Name.ToLower() + "_user.OxBooks");
            bf.Serialize(file, this);
            file.Close();
        }

        public void SaveCurrentBook()
        {
            for (int i = StartedBooks.Count - 1; i >= 0; i--)
            {
                if (StartedBooks[i].IdBook == CurrentBook.IdBook)
                {
                    StartedBooks[i] = CurrentBook;
                    return;
                }
            }
            StartedBooks.Add(CurrentBook);
        }


        public void UpdateStartedIdPages()
        {
            for (int i = StartedBooks.Count - 1; i >= 0; i--)
            {
                if(StartedBooks[i].IdBook >= 40) continue;
                
                for (int j = StartedBooks[i].GetRootNode().Flatten().ToList().Count - 1; j >= 0; j--)
                {
                    StartedBooks[i].GetRootNode().Flatten().ToList()[j].PageId = j;
                }
            }
        }
    }
}