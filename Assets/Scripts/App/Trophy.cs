﻿using System;
using Assets.Scripts.Utils;
using UnityEngine;

namespace Assets.Scripts.App
{
    public class TrophyInfoAttribute : Attribute
    {
        internal TrophyInfoAttribute(string path, string name)
        {
            this.Path = path;
            this.Name = name;
        }

        public string Path { get; private set; }
        public string Name { get; private set; }
    }

    [Serializable]
    public enum Trophy
    {
        [UserLevelInfo("trophy_0", 1)]
        TERROR,
        [UserLevelInfo("trophy_0", 2)]
        MARAVILLOSO,
        [UserLevelInfo("trophy_0", 3)]
        TEATRO,
        [UserLevelInfo("trophy_0", 4)]
        ESCRITOR,
        [UserLevelInfo("trophy_0", 5)]
        RANDOM
    }

    public static class TrophyExtensions
    {
        public static Sprite LoadSprite(this Trophy u)
        {
            var attr = u.GetAttribute<TrophyInfoAttribute>();
            return Resources.Load<Sprite>("Trophy/" + attr.Path);
        }

        public static string GetName(this Trophy u)
        {
            var attr = u.GetAttribute<TrophyInfoAttribute>();
            return attr.Name;
        }

    }
}