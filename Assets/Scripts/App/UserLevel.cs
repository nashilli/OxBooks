﻿using System;
using UnityEngine;
using System.Collections;
using Assets.Scripts.Utils;

namespace Assets.Scripts.App
{

    public class UserLevelInfoAttribute : Attribute
    {
        internal UserLevelInfoAttribute(string path, int numericLevel)
        {
            this.Path = path;
            this.NumericLevel = numericLevel;
        }

        public string Path { get; private set; }
        public int NumericLevel { get; private set; }
    }

    [Serializable]
    public enum UserLevel
    {
        [UserLevelInfo("userLevel_0", 1)] PEZ,
        [UserLevelInfo("userLevel_1", 2)] GATO,
        [UserLevelInfo("userLevel_2", 3)] PERRO,
        [UserLevelInfo("userLevel_3", 4)] DELFIN,
        [UserLevelInfo("userLevel_4", 5)] LEON
    }

    public static class UserLevelExtensions
    {
        public static Sprite LoadSprite(this UserLevel u)
        {
            var attr = u.GetAttribute<UserLevelInfoAttribute>();
            return Resources.Load<Sprite>("UserLevel/" + attr.Path);
        }

        public static int GetNumericLevel(this UserLevel u)
        {
            var attr = u.GetAttribute<UserLevelInfoAttribute>();
            return attr.NumericLevel;
        }

    }

}
