﻿using Assets.Scripts.App;
using Assets.Scripts.Sound;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Settings
{

    public class SettingsView : MonoBehaviour
    {
        public SwitchPlayerView switchPlayerView;
        public GeneralSettingsView generalSettingsView;

        public Toggle MusicToggle;
        public Toggle SoundToggle;

        public Button CloseButton;

        void Start()
        {

            MusicToggle.isOn = !SettingsController.GetController().GetMusic();
            SoundToggle.isOn = !SettingsController.GetController().SfxOn();

            CloseButton.onClick.AddListener(CloseSettings);
            MusicToggle.onValueChanged.AddListener(delegate
            {
                SettingsController.GetController().ToggleMusic();
                SoundController.GetController().PlayClickSound();
            });

            SoundToggle.onValueChanged.AddListener(delegate
            {
                SettingsController.GetController().ToggleSFX();
                SoundController.GetController().PlayClickSound();
            });
        }

        internal void ShowSwitchPlayer()
        {
            SoundController.GetController().PlayClickSound();
            switchPlayerView.gameObject.SetActive(true);
            switchPlayerView.inputName.Select();
        }

        internal void CloseSettings()
        {
            SoundController.GetController().PlayClickSound();
            ViewController.GetController().LoadMyThings();
        }

        internal void ShowGeneralSettings()
        {
            generalSettingsView.gameObject.SetActive(true);
        }
    }
}
