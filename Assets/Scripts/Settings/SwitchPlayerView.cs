﻿using Assets.Scripts.App;
using Assets.Scripts.Sound;
using System;
using Assets.Scripts.SelectAvatar;
using UnityEngine;
using UnityEngine.UI;
using Avatar = Assets.Scripts.App.Avatar;

namespace Assets.Scripts.Settings
{

    public class SwitchPlayerView : MonoBehaviour
    {

        public SettingsView settingsView;

        public Text title;
        public Text incorrectInput;
        public InputField inputName;
        public Button ticBtn;

        

        void OnEnable()
        {
            incorrectInput.gameObject.SetActive(false);
            UpdateTexts();
            string username = AppController.GetController().GetCurrentUser().Name;
            inputName.text = username.Replace(username[0], (""+username[0]).ToUpper()[0]);

        }

        private void UpdateTexts()
        {
            switch (SettingsController.GetController().GetLanguage())
            {
                case 0:
                    title.text = "CAMBIAR USUARIO";
                    inputName.placeholder.GetComponent<Text>().text = "Ingresa tu nombre";
                    incorrectInput.text = "Por favor, ingresa tu nombre";

                    break;
                default:
                    title.text = "SWITCH USER";
                    inputName.placeholder.GetComponent<Text>().text = "Insert your name";
                    incorrectInput.text = "Please, insert your name";
                    break;
            }
        }

        public void OnClickTicBtn()
        {
            PlayClickSound();
            CheckEnteredUsername();
        }

       
        void CheckEnteredUsername()
        {
            inputName.text = inputName.text.Trim();
            if (SettingsController.GetController().SetUsername(inputName.text.ToLower())) {
                if (AppController.GetController().GetCurrentUser().Avatar != Avatar.None)
                {
                    settingsView.ShowGeneralSettings();
                    gameObject.SetActive(false);
                }
                else
                {
                    ViewController.GetController().LoadSelectAvatar();
                }
                
            } else ShowIncorrectInputAnimation();           
        }

        internal void ShowIncorrectInputAnimation()
        {
            ticBtn.interactable = false;
            ticBtn.enabled = false;
            incorrectInput.GetComponent<IncorrectUserAnimation>().ShowIncorrecrUserAnimation();
        }

        public void OnIncorrectInputAnimationEnd()
        {
            ticBtn.interactable = true;
            ticBtn.enabled = true;
        }

        public void OnClickCrossBtn()
        {
            PlayClickSound();
            settingsView.ShowGeneralSettings();
            gameObject.SetActive(false);
        }

        private void PlayClickSound()
        {
            SoundController.GetController().PlayClickSound();
        }

    }
}
