﻿using System.Collections.Generic;
using Assets.Scripts.Settings;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.Sound;
using Assets.Scripts.App;

namespace Assets.Scripts.Rules
{
    public class GameRules : MonoBehaviour
    {
        [SerializeField]
        private Sprite[] areaSprites;
        private List<Color> areaColors;

        [SerializeField]
        private Text rulesLabel;
        [SerializeField]
        private Text backToGameLabel;
        [SerializeField]
        private Text gameDescription;
        [SerializeField]
        private List<Image> areaColorImages;
        [SerializeField]
        private Image icon;
        

        void Start(){
            areaColors = new List<Color>{ new Color32(237, 164, 60, 255), new Color32(209, 83, 51, 255), new Color32(26, 171, 209, 255), new Color32(11, 34, 86, 255) };
            UpdateTexts();
/*
            Game currentGame = AppController.GetController().GetCurrentGame();
*/
            /*gameDescription.text = currentGame.GetDescriptions()[SettingsController.GetController().GetLanguage()];
            SetArea(currentGame.GetArea());
            icon.sprite = currentGame.GetIcon();*/
        }

        private void UpdateTexts()
        {
           /* switch (SettingsController.GetController().GetLanguage())
            {
                case 0:
                    rulesLabel.text = AppController.GetController().GetCurrentGame().GetNames()[0];
                    backToGameLabel.text = "Volver al juego";
                    break;
                default:
                    rulesLabel.text = AppController.GetController().GetCurrentGame().GetNames()[1];
                    backToGameLabel.text = "Back to game";
                    break;
            }*/
        }

        internal void SetGameDescription(string description)
        {
            this.gameDescription.text = description;
        }

        internal void SetArea(int area)
        {
            UpdateAreaImages(areaColors[area]);
        }

        private void UpdateAreaImages(Color color)
        {
            for (int i = 0; i < areaColorImages.Count; i++)
            {
                areaColorImages[i].color = color;
            }
        }

        public void OnClickBackToGame()
        {
            ClickSound();
            ViewController.GetController().HideInstructions();
        }

        private void ClickSound()
        {
            SoundController.GetController().PlayClickSound();
        }
    }
}