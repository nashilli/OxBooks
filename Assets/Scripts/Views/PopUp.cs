﻿using Assets.Scripts.App;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Views
{
    public abstract class PopUp : MonoBehaviour
    {

        public Button CloseButton;
            
        // Use this for initialization
        protected void Start () {
		    CloseButton.onClick.AddListener(() =>
		    {
		        ViewController.GetController().GetTopbar().SetActive(true);
		        gameObject.SetActive(false);


            });
        }
	
        // Update is called once per frame
        void Update () {
		
        }
    }
}
