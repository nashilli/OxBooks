﻿using UnityEngine.UI;

namespace Assets.Scripts.Views
{
    public class IllustratorPopUp : PopUp
    {

        public Text BookTitleText;
        public Text ThisBooksWasIlustratedByLabel;
        public Text IllustratorNameAndLastNameText;
        public Text IllustratorContactText;
        public Text IllustratorProgramText;
        public Text IllustratorProgramParticipateText;
        public Text MoreAboutText;
        public Image BookImage;



    
	
        // Update is called once per frame
        void Update () {
		
        }
    }
}
