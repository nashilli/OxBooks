﻿using Assets.Scripts.Settings;
using UnityEngine.UI;

namespace Assets.Scripts.Views
{
    public class ProblemPopUp : PopUp
    {

        public Text ProblemText;


        // Use this for initialization
        void OnEnable ()
        {
            ProblemText.text = SettingsController.GetController().GetLanguage() == 0
                ? "¡Upps! ¡Parece que hubo un problema! Verifica tu conexión a internet."
                : "Ooops! We have a problem. Check your internet connection and restart OxBooks.";
        }
	
    }
}
