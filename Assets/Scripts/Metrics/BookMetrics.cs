﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Common;
using Assets.Scripts._Book.Page;
using UnityEngine;

namespace Assets.Scripts.Metrics
{
    [Serializable]
    public class BookMetrics
    {
        private static float MAX_SCORE = 10000;

        public BookMetrics()
        {
            InitialDateTime = DateTime.Now;
            WrongAnswersList = new List<Tuple<PageType, float>>();
        }

        public void AddWrongAnswer(PageType pageType, float errors = 1)
        {
            for (int i = WrongAnswersList.Count - 1; i >= 0; i--)
            {
                if (WrongAnswersList[i].First == pageType)
                {
                    WrongAnswersList[i].Second++;
                    return;
                }
            }
            WrongAnswersList.Add(new Tuple<PageType, float>(pageType, errors));
        }

        public List<Tuple<PageType, float>> WrongAnswersList { get; private set; }
        public int CorrectAnswers { get; private set; }
        public int Hints { get; private set; }
        public float Score { get; set; }
        public int Stars { get; set; }
        public DateTime InitialDateTime { get; private set; }
        public DateTime FinishedDateTime { get; private set; }
        public float Duration { get; private set; }


        public int GetTotalWrongAnswers()
        {
            float totalWrongAnswers = 0;
            foreach (Tuple<PageType, float> tuple in WrongAnswersList)
            {
                totalWrongAnswers += tuple.Second;
            }
            return Mathf.RoundToInt(totalWrongAnswers);
        }

        public void AddCorrectAnswer()
        {
            CorrectAnswers++;
        }

        public void Add(BookMetrics other)
        {
            CorrectAnswers += other.CorrectAnswers;
            Hints += other.Hints;
            Duration += other.Duration;
            Stars += other.Stars;
            Score += other.Score;

            for (int i = other.WrongAnswersList.Count - 1; i >= 0; i--)
            {
                AddWrongAnswer(other.WrongAnswersList[i].First, other.WrongAnswersList[i].Second);
    
            }
        }

        public void CalculateScore()
        {
            float pointsPerError = Mathf.RoundToInt((9500 +0f) / CorrectAnswers);
            Score = 10000 - GetTotalWrongAnswers()*pointsPerError - (Hints + 0f) * pointsPerError / 5f;
            if (Score < 500) Score = 500;
            Score = Mathf.RoundToInt(Score);
        }

        public void CalculateStars()
        {
            float ratio = Score/MAX_SCORE;
            if (ratio > 0.89) Stars = 3;
            else if (ratio > 0.6) Stars = 2;
            else Stars = 1;
        }

        public void AddHint()
        {
            Hints++;
        }

        public void Restart()
        {
            WrongAnswersList.Clear();
            CorrectAnswers = 0;
            Hints = 0;
            InitialDateTime = DateTime.Now;
        }

        public void CalculateAverage(int length)
        {
            Score = Mathf.RoundToInt((Score + 0f) / (length + 0f));
            Stars = Mathf.RoundToInt((Stars + 0f) / (length + 0f));

            CorrectAnswers = Mathf.RoundToInt((CorrectAnswers + 0f) / (length + 0f));
            Hints = Mathf.RoundToInt((Hints + 0f) / (length + 0f));
            Duration = Mathf.RoundToInt((Duration + 0f) / (length + 0f));

            for (int i = WrongAnswersList.Count - 1; i >= 0; i--)
            {
                WrongAnswersList[i].Second /= (length + 0f);

            }
        }
    }
}