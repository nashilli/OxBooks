﻿using Assets.Scripts.Settings;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Cover
{

    public class AboutScreen : MonoBehaviour
    {
        public CoverView coverView;


        public Text nameLabel;
        public Text nameText;
        public Text ageRangeLabel;
        public Text ageRangeText;
        public Text areaLabel;
        public Text areaText;
        public Text contentsLabel;
        public Text contentsText;
        public Text creditsLabel;
        public Text creditsText;

        void OnEnable()
        {
            UpdateTexts();
        }

        private void UpdateTexts()
        {
            switch (SettingsController.GetController().GetLanguage())
            {
                case 0:
                    nameLabel.text = "MiniLesson";
                    nameText.text = "OXBOOKS";
                    ageRangeLabel.text = "EDAD";
                    ageRangeText.text = "De 6 a 13 años";
                    areaLabel.text = "ÁREA";
                    areaText.text = "Lengua";
                    contentsLabel.text = "CONTENIDOS QUE ABARCA";
                    contentsText.text = "Comprensión lectora\nAnálisis de textos";
                    creditsLabel.text = "CRÉDITOS";
                    creditsText.text = "Música: Cosmic Shell Suit de ScottHolmes";
                    break;
                default:
                    nameLabel.text = "NAME";
                    ageRangeLabel.text = "AGE";
                    areaLabel.text = "AREA";
                    areaText.text = "Language";
                    contentsLabel.text = "CONTENTS";
                    contentsText.text = "\n------";
                    creditsLabel.text = "CREDITS";
                    break;
            }
        }

        public void OnClickAboutScreen(){
            coverView.ClickSound();
            coverView.ShowCoverScreen();
            gameObject.SetActive(false);
        }

        
    }
}