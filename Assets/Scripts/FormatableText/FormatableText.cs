﻿using System.Linq;
using UnityEngine;

namespace Assets.Scripts.FormatableText
{
    public class FormatableText : MonoBehaviour
    {

        [SerializeField]
        private GameObject linePrefab;


        public int fontSize;
        public Color DefaulFontColor;

        public void ShowText(string text, int charWidth, char charToSplit)
        {
            DestroyPrevious();

            string[] splittedText = text.Split(charToSplit);
            int i = 0;
            for (; i < splittedText.Length; i++)
            {
                if (FormatableTextController.GetController().FormatableWords.Count - 1 < i)
                {
                    Debug.Log("word spawned");
                    FormatableTextController.GetController().SpawnFormatableWord();
                }
                FormatableWord formatableWord = FormatableTextController.GetController().FormatableWords[i];
                formatableWord.RemoveSpecialWord();
                formatableWord.Restart(DefaulFontColor);
                formatableWord.gameObject.SetActive(true);
                AddWord(splittedText[i], formatableWord, charWidth);

            }
            for (int j = i; j < FormatableTextController.GetController().FormatableWords.Count; j++)
            {
                FormatableTextController.GetController().FormatableWords[j].gameObject.SetActive(false);
            }
        }

        private void DestroyPrevious()
        {
            foreach (FormatableLine line in GetComponentsInChildren<FormatableLine>())
            {
                Lean.LeanPool.Despawn(line.gameObject);
            }
        }


        public void AddWord(string word, FormatableWord formatableWord, int charWidth)
        {
            // Debug.Log("char size " + charWidth);
            FormatableLine[] lines = GetComponentsInChildren<FormatableLine>();
            if (lines.Length == 0)
            {
                AddLine();
                lines = GetComponentsInChildren<FormatableLine>();
            }
            /*  else
              {
                  int currentLineIndex = 0;
                  for (int i = lines.Length - 1; i >= 0; i--)
                  {

                  }
              }
              */

            FormatableLine currentLine = lines[lines.Length - 1];
            {
                if (word == "\n")
                {
                    AddLine();
                    return;
                }
                formatableWord.SetText(word);
                formatableWord.SetFontSize(fontSize);
                formatableWord.SetFontColor(DefaulFontColor);
                float width = FormatableTextController.GetController().CalculateWidthUnits(word) * charWidth;
                if (currentLine.CanAdd(width))
                {
                    //currentLine.AddWord(word, fontSize, DefaulFontColor);
                    currentLine.AddFormatableWord(formatableWord, width);

                }
                else
                {
                    AddLine();
                    lines = GetComponentsInChildren<FormatableLine>();

                    lines[lines.Length - 1].AddFormatableWord(formatableWord, width);
                }
            }
        }

        internal void AddLine()
        {
            GameObject newLine = Lean.LeanPool.Spawn(linePrefab);
            newLine.transform.SetParent(gameObject.transform, true);
            newLine.transform.localPosition = Vector3.zero;
            newLine.GetComponent<RectTransform>().offsetMax = Vector2.zero;
            newLine.GetComponent<RectTransform>().offsetMin = Vector2.zero;
            newLine.transform.localScale = Vector3.one;
            newLine.GetComponent<FormatableLine>().SetWidths(gameObject.GetComponent<RectTransform>().rect.width);
        }

        /* public FormatableWord AddWord(FormatableWord formatableWord)
         {
             FormatableLine[] lines = GetComponentsInChildren<FormatableLine>();
             if (lines.Length == 0)
             {
                 AddLine();
                 lines = GetComponentsInChildren<FormatableLine>();
             }
             FormatableLine currentLine = lines[lines.Length - 1];

             {
                 if (currentLine.CanAdd(formatableWord.GetText().Length * charWidth))
                 {
                     return currentLine.AddWord(formatableWord, fontSize);

                 }
                 else
                 {
                     AddLine();
                     lines = GetComponentsInChildren<FormatableLine>();
                     return lines[lines.Length - 1].AddWord(formatableWord, fontSize);
                 }
             }
         }
         */
        public void UpdateWord(int wordIndex, string word)
        {
            GetComponentsInChildren<FormatableWord>()[wordIndex].SetText(word);
        }
    }
}
