﻿namespace Assets.Scripts.FormatableText
{
    public enum FormatType  {

        Highlight, Clear, Underline, Paint, NoOne
        
    }
}
