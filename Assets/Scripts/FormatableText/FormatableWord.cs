﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Sound;
using Assets.Scripts._Book;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.Scripts.FormatableText
{
    public class FormatableWord : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerDownHandler {

        [SerializeField]
        private Text visibleText;

        [SerializeField] private Image _underlineImage;

        private List<Action> _actions;

        void Awake()
        {
            _actions = new List<Action>();
            _underlineImage.gameObject.SetActive(false);
            AddOnClickAction(ApplyFormat);
        }

        private void AddOnClickAction(Action action)
        {
            _actions.Add(action);   
        }

        public void MakeInvisible()
        {
            SetFontColor(new Color(GetFontColor().r, GetFontColor().g, GetFontColor().b, 0));
            _actions.Clear();
        }

        

        public void ApplyFormat()
        {
            if (FormatableTextController.GetController() == null || !FormatableTextController.GetController().IsFormatting()) return;
            switch (FormatableTextController.GetController().GetCurrentFormat())
            {
                case FormatType.Paint:
                    //SoundController.GetController().PlayClickSound();
                    Text myText = GetComponentsInChildren<Text>()[1];
                    myText.color = FormatableTextController.GetController().GetLetterColorSelected();
                    break;
                case FormatType.Highlight:
                    //SoundController.GetController().PlayClickSound();
                    GetComponentsInChildren<Image>()[0].color = FormatableTextController.GetController().GetHighlightColorSelected();
                    break;
                case FormatType.Underline:
                    //SoundController.GetController().PlayClickSound();
                    Underline(Color.black);
                    break;
                case FormatType.Clear:
                    //SoundController.GetController().PlayClickSound();

                    GetComponentsInChildren<Image>()[0].color = new Color32(0, 0, 0, 0);
                    /*_underlineImage.gameObject.SetActive(false);
                    GetComponentsInChildren<Text>()[1].color = Color.black;*/
                    break;
            }
        }

        public void Underline(Color color)
        {
            _underlineImage.color = color;
            _underlineImage.gameObject.SetActive(true);

        }

        internal void SetText(string word)
        {
            gameObject.GetComponent<Text>().text = word;
            visibleText.text = word;
    
        }

        public void SetFontColor(Color color)
        {
            Text myText = GetComponentsInChildren<Text>()[1];
            myText.color = color;
        }

        internal void SetHighlightColor(Color color)
        {
            GetComponentsInChildren<Image>()[0].color = color;

        }

        internal string GetText()
        {
            return gameObject.GetComponent<Text>().text;
        }

        public Color GetFontColor()
        {
            return GetComponentsInChildren<Text>()[1].color;
        }

        public Color GetHighlightColor()
        {
            return GetComponentsInChildren<Image>()[0].color;
        }

        public void SetFontSize(int fontSize)
        {
            gameObject.GetComponent<Text>().fontSize = fontSize;
            gameObject.GetComponent<Text>().resizeTextMaxSize = fontSize;
            visibleText.fontSize = fontSize;
            visibleText.resizeTextMaxSize = fontSize;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            for (int i = _actions.Count - 1; i >= 0; i--)
            {
                _actions[i]();
            }
        }

        public void SetBoldAndtalic()
        {
            gameObject.GetComponent<Text>().fontStyle = FontStyle.BoldAndItalic;
            visibleText.fontStyle = FontStyle.BoldAndItalic;
        }

        public void SetAsSpecialWord(string word, string description)
        {
            Underline(new Color32(21,117,187,255));
            AddOnClickAction(delegate
            {
                if (FormatableTextController.GetController() != null && FormatableTextController.GetController().IsFormatting()) return;
                SoundController.GetController().PlayClickSound();
                ShowDescriptionPopUp(word, description);
            });
        }

        private void ShowDescriptionPopUp(string word, string description)
        {
            BookViewController.GetController().ShowDescriptionPopUp(word, description);
        }

        public bool IsUnderlined()
        {
            return _underlineImage.color.a > 0;
        }

        public Color UnderlineColor()
        {
            return _underlineImage.color;
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (Input.GetMouseButton(0))
            {
                ApplyFormat();
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            ApplyFormat();
        }

        public void RemoveSpecialWord()
        {
            if (_actions.Count > 1)
            {
                _underlineImage.gameObject.SetActive(false);
                _actions.RemoveAt(1);
            }
            
        }

        internal void Restart(Color defaulFontColor)
        {
            RemoveSpecialWord();
            SetFontColor(defaulFontColor);
            GetComponentsInChildren<Image>()[0].color = new Color32(0, 0, 0, 0);
            Underline(new Color(0,0,0,0));
        }

    }
}
