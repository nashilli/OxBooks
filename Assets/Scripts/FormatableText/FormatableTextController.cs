﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Sound;
using Assets.Scripts._Book;
using Assets.Scripts._Book.Page;
using Assets.Scripts._Book.PageView;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.FormatableText
{
    public class FormatableTextController : MonoBehaviour
    {


        public FormatableWord WordPrefab;

        private static FormatableTextController formatableTextController;

        [SerializeField]
        private List<Toggle> formatToggles;
        [SerializeField]
        private GameObject letterColourPanel;
        [SerializeField]
        private GameObject highlightColourPanel;

        public List<FormatableWord> FormatableWords;

        void Awake()
        {
            if (formatableTextController == null) formatableTextController = this;
            else if (this != formatableTextController) Destroy(this);
            FormatableWords = GetComponentsInChildren<FormatableWord>(true).ToList();
        }


        internal FormatType GetCurrentFormat()
        {
            for (int i = 0; i < formatToggles.Count; i++)
            {
                if (formatToggles[i].isOn)
                {
                    return (FormatType)i;

                }
            }
            return FormatType.NoOne;
        }

        internal Color GetLetterColorSelected()
        {
            Toggle[] colourToggles = letterColourPanel.GetComponentsInChildren<Toggle>();
            return GetColourSelectedOf(colourToggles);
        }

        internal Color GetHighlightColorSelected()
        {
            return GetColourSelectedOf(highlightColourPanel.GetComponentsInChildren<Toggle>());
        }

        private Color GetColourSelectedOf(Toggle[] colourToggles)
        {
            for (int i = 0; i < colourToggles.Length; i++)
            {
                if (colourToggles[i].isOn)
                {
                    Color color = colourToggles[i].GetComponentsInChildren<Image>()[1].color;
                    return color;
                }
            }

            return Color.black;
        }

        public void ShowLetterColourPanelColourPanel()
        {
            letterColourPanel.SetActive(true);
            highlightColourPanel.SetActive(false);
        }

        public void ShowHighlightColourPanelColourPanel()
        {
            letterColourPanel.SetActive(false);
            highlightColourPanel.SetActive(true);
        }

        public void HideColourPanel()
        {
            letterColourPanel.SetActive(false);
            highlightColourPanel.SetActive(false);
        }

        public static FormatableTextController GetController()
        {
            return formatableTextController;
        }


        public bool IsFormatting()
        {
            return gameObject.activeSelf && formatToggles.Exists(e => e.isOn);
        }

        public void SpawnFormatableWord()
        {
            FormatableWords.Add(Lean.LeanPool.Spawn(FormatableTextController.GetController().WordPrefab.gameObject).GetComponent<FormatableWord>());
        }

        public float CalculateWidthUnits(string word)
        {
            float width = 0;
            foreach (char c in word)
            {
                if (c == '.' || c == ',' || c == ';' || c == ':' || c == '!' || c == '¡' || c == '?' || c == '¿')
                    width += 0.5f;
                else width++;
            }
            return width;
        }

        public void PlayClickSound()
        {
            SoundController.GetController().PlayClickSound();
        }
    }
}
