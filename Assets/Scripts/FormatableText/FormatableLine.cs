﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.FormatableText
{
    public class FormatableLine : MonoBehaviour
    {

        /* [SerializeField]
         private FormatableWord wordPrefab;*/

        private float maxWidth;

        private float currentWidht;
        private int currentChilds;

        internal void SetWidths(float maxWidth)
        {
            this.maxWidth = maxWidth;
        }

        internal bool isEmpty()
        {
            return GetComponentsInChildren<FormatableWord>().Length == 0;
        }

        internal void AddWord(string word, int fontSize, Color fontColor)
        {
            /*  currentWidht += charWidth * word.Length;
              currentChilds++;
              GameObject newWord = Lean.LeanPool.Spawn(wordPrefab.gameObject);
              newWord.GetComponent<FormatableWord>().SetText(word);
              newWord.GetComponent<FormatableWord>().SetFontSize(fontSize);
              newWord.GetComponent<FormatableWord>().SetFontColor(fontColor);
              newWord.transform.SetParent(gameObject.transform, true);
              newWord.transform.localPosition = Vector3.zero;
              newWord.GetComponent<RectTransform>().offsetMax = Vector2.zero;
              newWord.GetComponent<RectTransform>().offsetMin = Vector2.zero;
              newWord.transform.localScale = Vector3.one;*/

        }

        internal bool CanAdd(float width)
        {
            //Debug.Log("Currwnt width  = " + currentWidht);
            return currentWidht + width + gameObject.GetComponent<HorizontalLayoutGroup>().spacing * (currentChilds + 1) < maxWidth;
        }

        internal string RemoveLastWord()
        {
            FormatableWord last = GetComponentsInChildren<FormatableWord>()[GetComponentsInChildren<FormatableWord>().Length - 1];
            string toReturn = last.GetText();
            Lean.LeanPool.Despawn(GetComponentsInChildren<FormatableWord>()[GetComponentsInChildren<FormatableWord>().Length - 1].gameObject);
            return toReturn;
        }

        /*     internal FormatableWord AddWord(FormatableWord formatableWord, int fontSize)
             {
                 currentWidht += charWidth * formatableWord.GetText().Length;
                 currentChilds++;
                 GameObject newWord = Lean.LeanPool.Spawn(wordPrefab.gameObject);
                 newWord.GetComponent<FormatableWord>().SetText(formatableWord.GetText());
                 newWord.GetComponent<FormatableWord>().SetFontSize(fontSize);
                 newWord.GetComponent<FormatableWord>().SetFontColor(formatableWord.GetFontColor());
                 newWord.GetComponent<FormatableWord>().SetHighlightColor(formatableWord.GetHighlightColor());
                 newWord.GetComponent<FormatableWord>().SetText(formatableWord.GetText());
                 newWord.transform.SetParent(gameObject.transform, true);
                 newWord.transform.localPosition = Vector3.zero;
                 newWord.GetComponent<RectTransform>().offsetMax = Vector2.zero;
                 newWord.GetComponent<RectTransform>().offsetMin = Vector2.zero;
                 newWord.transform.localScale = Vector3.one;
                 return newWord.GetComponent<FormatableWord>();
             }
             */

        public void AddFormatableWord(FormatableWord formatableWord, float width)
        {
            currentWidht += width;
            currentChilds++;
            //GameObject newWord = Lean.LeanPool.Spawn(wordPrefab.gameObject);
            formatableWord.transform.SetParent(gameObject.transform, true);
            formatableWord.transform.localPosition = Vector3.zero;
            formatableWord.GetComponent<RectTransform>().offsetMax = Vector2.zero;
            formatableWord.GetComponent<RectTransform>().offsetMin = Vector2.zero;
            formatableWord.transform.localScale = Vector3.one;
        }

        public void Restart()
        {
            currentWidht = 0;
            currentChilds = 0;
            foreach (FormatableWord child in GetComponentsInChildren<FormatableWord>(true))
            {
                child.transform.SetParent(FormatableTextController.GetController().gameObject.transform);
                child.gameObject.SetActive(false);
            }
        }
    }
}
