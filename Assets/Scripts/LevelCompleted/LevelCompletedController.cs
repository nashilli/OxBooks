﻿using System;
using System.IO;
using UnityEngine;
using Assets.Scripts.Sound;
using Assets.Scripts.App;
using Assets.Scripts.Common;
using Assets.Scripts.Common.Action;
using Assets.Scripts.Metrics;
using Assets.Scripts.Settings;
using Assets.Scripts._Book;

namespace Assets.Scripts.LevelCompleted
{
    public class LevelCompletedController : MonoBehaviour
    {
        private static LevelCompletedController _levelCompletedController;
        public LevelCompletedView LevelCompletedView;

        void Awake()
        {
            if (_levelCompletedController == null) _levelCompletedController = this;
            else if (_levelCompletedController != this) Destroy(gameObject);
            SoundController.GetController().PlayLevelCompleteSound();
        }


        void Start()
        {
            UpdateTexts();
            StartedBook currentBook = AppController.GetController().GetCurrentUser().CurrentBook;
            BookMetrics bookMetrics = currentBook.Metrics;
            SetValues(bookMetrics);
            AppController.GetController().SaveAdvance();
/*
            currentBook.Mode = Mode.View;
*/
            AddListeners();
        }

        private void AddListeners()
        {
            LevelCompletedView.ExploreButton.onClick.AddListener(delegate
            {
                PlayClikSound();
               
                ViewController.GetController().LoadTopbar();
                ViewController.GetController().LoadExplore();
                SoundController.GetController().PlayMusic();
                AppController.GetController().GetCurrentUser().CurrentBook = null;
            });

            LevelCompletedView.MyThingsButton.onClick.AddListener(delegate
            {
                PlayClikSound();
                ViewController.GetController().LoadTopbar();
                ViewController.GetController().LoadMyThings();
                SoundController.GetController().PlayMusic();
                AppController.GetController().GetCurrentUser().CurrentBook = null;

            });

            LevelCompletedView.PrintButton.onClick.AddListener(delegate
            {
                PlayClikSound();
                string s = (Application.platform == RuntimePlatform.Android ? Application.persistentDataPath : Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "/OXBOOKS") + "/" + BookViewController.GetController().CurrentBook.Title + "/ ";

                Directory.CreateDirectory(s);
                Printer.ConvertBookToDoc(BookViewController.GetController().CurrentBook,
                    s);
                ViewController.GetController().ShowInfoPopUp("La versión imprimible del cuento se ha guardado con éxito en " + s);
            });

            LevelCompletedView.BackToBookButton.MyAction = new ViewBookAction(BookViewController.GetController().CurrentBook);
            LevelCompletedView.BackToBookButton.onClick.AddListener(PlayClikSound);
        }


        private void UpdateTexts()
        {
            switch (SettingsController.GetController().GetLanguage())
            {
                case 0:  
                    break;
                case 1: 
                    break;
            }
        }

        public void SetValues(BookMetrics bookMetrics)
        {
            for (int i = 0; i < LevelCompletedView.Stars.Count; i++)
            {
                LevelCompletedView.Stars[i].enabled = i < bookMetrics.Stars;
            }
            LevelCompletedView.ScoreText.text = "" + GetCorrectTextFromScore(bookMetrics.Score);
            LevelCompletedView.CorrectAnswers.text = "" + bookMetrics.CorrectAnswers;
            LevelCompletedView.WrongAnswers.text = "" + bookMetrics.GetTotalWrongAnswers();
            LevelCompletedView.HintsText.text = "" + bookMetrics.Hints;
        }

        private string GetCorrectTextFromScore(float value)
        {
            return value > 0 ? "" + value : "-";
        }


        internal void PlayClikSound()
        {
            SoundController.GetController().PlayClickSound();
        }

        internal static LevelCompletedController GetController()
        {
            return _levelCompletedController;
        }
    }
}
