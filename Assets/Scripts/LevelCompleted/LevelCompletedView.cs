﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using Assets.Scripts.Common;

namespace Assets.Scripts.LevelCompleted
{

    public class LevelCompletedView : MonoBehaviour
    {

        public Text CongratsText;
        public Text LabelScoreText;
        public Text ScoreText;
        public List<Image> Stars;
        public Text LabelCorrectAnswers;
    
        public Text CorrectAnswers;
        public Text LabelWrongAnswers;
        public Text WrongAnswers;
        public Text LabelHintsText;
        public Text HintsText;

        public Button MyThingsButton;
        public Button ExploreButton;     
        public Button PrintButton;     
        public ActionButton BackToBookButton;     
        
    }
}
