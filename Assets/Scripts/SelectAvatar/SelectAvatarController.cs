﻿using System;
using Assets.Scripts.App;
using Assets.Scripts.Sound;
using UnityEngine;
using Avatar = Assets.Scripts.App.Avatar;

namespace Assets.Scripts.SelectAvatar
{
    public class SelectAvatarController : MonoBehaviour
    {
        public SelectAvatarView SelectAvatarView;
        private Material[] _avatarSprites;
        private int _currentAvatar;
	
        // Use this for initialization
        void Start ()
        {
            _currentAvatar = 0;
            _avatarSprites = Utils.Utils.ShuffleArray(Resources.LoadAll<Material>("Avatar/Big/"));
            SelectAvatarView.CurrentAvaterImage.material = _avatarSprites[_currentAvatar];
            AddListeners();
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.LeftArrow)) PrevAvatar();
            else if(Input.GetKeyDown(KeyCode.RightArrow)) NextAvatar();
            else if(Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter)) ConfirmAvatar();
            else if(Input.GetKeyDown(KeyCode.Escape)) ViewController.GetController().LoadLogin();
        }

        private void AddListeners()
        {
            SelectAvatarView.BackButton.onClick.AddListener(delegate
            {
                SoundController.GetController().PlayClickSound();
                ViewController.GetController().LoadLogin();
            });

            SelectAvatarView.NextAvatarButton.onClick.AddListener(NextAvatar);

            SelectAvatarView.PrevAvatarButton.onClick.AddListener(PrevAvatar);

            SelectAvatarView.TicButton.onClick.AddListener(ConfirmAvatar);
        }

        private void ConfirmAvatar()
        {
            SoundController.GetController().PlayClickSound();
            int c = int.Parse("" + _avatarSprites[_currentAvatar].name.Substring(_avatarSprites[_currentAvatar].name.IndexOf("_", StringComparison.Ordinal) + 1));
            User currentUser = AppController.GetController().GetCurrentUser();
            currentUser.Avatar = (Avatar) c;
            ViewController.GetController().LoadExplore();
            ViewController.GetController().LoadTopbar();
            currentUser.SaveToDisk();
        }

        private void NextAvatar()
        {
            SoundController.GetController().PlayClickSound();
            _currentAvatar = _currentAvatar + 1 < _avatarSprites.Length ? _currentAvatar + 1 : 0;
            SelectAvatarView.CurrentAvaterImage.material = _avatarSprites[_currentAvatar];
        }

        private void PrevAvatar()
        {
            SoundController.GetController().PlayClickSound();
            _currentAvatar = _currentAvatar - 1 >= 0 ? _currentAvatar - 1 : _avatarSprites.Length - 1;
            SelectAvatarView.CurrentAvaterImage.material = _avatarSprites[_currentAvatar];
        }
    }
}
