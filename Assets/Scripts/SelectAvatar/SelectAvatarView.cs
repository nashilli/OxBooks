﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.SelectAvatar
{
    public class SelectAvatarView : MonoBehaviour
    {

        public Button NextAvatarButton;
        public Button PrevAvatarButton;
        public Image CurrentAvaterImage;
        public Button BackButton;
        public Button TicButton;

    }
}
